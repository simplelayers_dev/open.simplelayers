function initDataEntry()
{
    require
            (
                    ["dojo/dom", 'components/inputs/json_input/OldJSONInput'],
                    function (dom, inputWidget)
                    {
                        var inputWidget = new inputWidget();
                        inputWidget.placeAt(dom.byId('JSONInputField'));
                    }
            );
    baseURL = loc;
}