function initDataEntry()
{
    require
            (
                    ["dojo/dom", 'sl_components_open/inputs/json_input/JSONInput',
                        'dojo/text!' + currentPage + "/JSONInputData.json", 'dojo/text!' + currentPage + "/ExistingData.json"],
                    function (dom, inputWidget, JSONInputData, ExistingData)
                    {
                        var inputWidget = new inputWidget(JSONInputData, ExistingData);
                        inputWidget.placeAt(dom.byId('JSONInputField'));
                    }
            );
    baseURL = loc;
}