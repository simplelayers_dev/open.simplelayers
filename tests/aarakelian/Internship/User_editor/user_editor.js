userlist = [];
currentuser = null;
function SaveButtonHandler() {
    var a = document.getElementById("uname").value;
    var b = document.getElementById("upass").value;
    var c = document.getElementById("umail").value;
    var d = {name: a, pass: b, email: c};

    if (d.name !== null || d.pass !== null || d.email !== null && currentuser !== null)
    {
        document.getElementById("uname").value = d.name;
        document.getElementById("upass").value = d.pass;
        document.getElementById("umail").value = d.email;
        ClearForm();
        userlist[parseInt(currentuser)] = d;
        RefreshList();

    } else {
        $().addClass('Error()');
        RefreshList();
        ClearForm();
    }


    if (ValidUser(d) === true && currentuser === null)
    {
        userlist.push(d);
        var i = userlist.length - 1;
        AddUser(d, i);
    } else
    {

    }
    currentuser = null;

}

function ValidUser(userOb) {
    var isValid = true;
    $("#umail").toggleClass("Error", false);
    if (userOb.email.indexOf("@") === -1 || userOb.email.indexOf(".") === -1 || userOb.email.indexOf("..") !== -1)
    {
        $("#umail").toggleClass("Error", true);
        isValid = false;
    }

    $("#uname").toggleClass("Error", false);
    if (userOb.name.length === 0) {
        $("#uname").toggleClass("Error", true);
        isValid = false;

    }
    $("#upass").toggleClass("Error", false);
    if (userOb.pass.length === 0) {
        $("#upass").toggleClass("Error", true);
        isValid = false;
    }

    return isValid;
}



function ClearForm() {
    $().find('input').removeClass('Error');
    document.getElementById('uname').value = "";
    document.getElementById('upass').value = "";
    document.getElementById('umail').value = "";
}

function ResetValidation() {

}

function AddUser(userOb, id) {

    var a = document.getElementById("userlist");
    var b = "<tr class='useritem'><td><button type='button' data-userindex='" + id + "' onclick='DeleteUser(event)'>X</button></td><td><a href='#' onclick='SetUser(event)' data-userindex='" + id + "' >" + userOb.name + "</a></td><td><a href='mailto:" + userOb.email + "'>" + userOb.email + "</a></td></tr>";
    $("#userlist").append(b);

//    console.log(GetUser('Anthony'));
}

function SetUser(event) {

    var id = $(event.target).attr("data-userindex");
    var d = userlist[parseInt(id)];
    currentuser = id;
    var c = document.getElementById("uname");
    console.log(id);
    console.log(c.value);

    document.getElementById("uname").value = d.name;
    document.getElementById("upass").value = d.pass;
    document.getElementById("umail").value = d.email;

    $("#upass").toggleClass("Error", false);
    $("#uname").toggleClass("Error", false);
    $("#umail").toggleClass("Error", false);
    RefreshList();
}

function DeleteUser(event) {

//   console.log(event);
    var id = $(event.target).attr("data-userindex");
//   console.log(id);
    userlist.splice(id, 1);
//   console.log(userlist.length);
    RefreshList();
}

function ClearList() {

}

function RefreshList() {
    $(".useritem").remove();
    for (var i = 0; i < userlist.length; i++)
    {
        AddUser(userlist[i], i);
    }
    return
}

function GetUser(name) {
    var found = false;
    for (var a = 0; a < userlist.length; a++)
    {
        if (name === userlist[a].name) {
            found = true;
        } else {
            found = false;
        }
    }
    return found;
}

