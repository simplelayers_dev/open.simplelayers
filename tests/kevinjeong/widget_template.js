define(["dojo/_base/declare", //imported js files
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./TEMPLATENAME.tpl.html"
], function (declare, //variables for the imported js files
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template
        ) {
    return declare("LayerItem", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
	//variable names
        templateString: template,
        baseClass: "CLASS_NAME_FOR_WIDGET",
        constructor: function (data) { //called before creation
            //data is a parameter you can do w/e you want
        },
        postCreate: function () { //called after creation
            
        },
        CustomFunction: function (event) {
            //make your own function
        }
    });
});