		var dojoConfig = 
		{
			async : true,
			baseUrl : "../../",
			packages : 
			[ 
				{
					name : "dojo",
					location : "dojo-release-1.10.4/dojo"
				}, 
				{
					name : "dijit",
					location : "dojo-release-1.10.4/dijit"
				}, 
				{
					name : "dojox",
					location : "dojo-release-1.10.4/dojox"
				}, 
				{
					name : "components",
					location : "components"
				}
			]
		};

