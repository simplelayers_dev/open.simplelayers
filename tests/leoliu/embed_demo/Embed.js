define(["dojo/_base/declare", "dojo/on", "dojo/dom-construct",
    "dojo/dom-attr", "dojo/dom-class", "dijit/_WidgetBase",
    "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "dijit/registry",
    "dojo/dom", "sl_modules_open/env", "dojo/text!./Embed.tpl.html"],
        function (declare, on, domCon, domAttr, domClass, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, registry, dom, Env, template) {
            return declare('Embed', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                /**
                 * Part of the Open SimpleLayers project
                 * https://bitbucket.org/simplelayers_dev/open.simplelayers
                 * 
                 **/
                data: null,
                inputs: null,
                disableToggle: false,
                templateString: template,
                baseClass: 'Embed',
                Start: function (JSONObj) {
                    this.data = JSONObj;

                    Env.RequireBootStrap();
                    if(this.data.hasOwnProperty("mApps")) {
                        console.log(this.data);
                    }
                    else {
                        
                    }
                }
            });
        });
