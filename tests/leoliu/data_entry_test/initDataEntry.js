function initDataEntry()
{
    require
            (
                    ["dojo/dom",
                        "dojo/on",
                        "sl_components_open/inputs/json_input/JSONInput",
                        "sl_components_open/data_entry/DataEntry",
                        "sl_components_open/data_entry/inputs/button/Button",
                        "dojo/text!" + currentPage + "/JSONInputData.json",
                        "dojo/text!" + currentPage + "/ExistingData.json"],
                    function (dom, on, inputWidget, formWidget, buttonWidget, JSONInputData, ExistingData)
                    {
                        var DataEntryForm = new inputWidget(JSONInputData, ExistingData);
                        DataEntryForm.placeAt(dom.byId('JSONInputField'));

                        var generateFormButton = new buttonWidget({
                            singlebutton: "Generate Form"
                        });
                        generateFormButton.placeAt(dom.byId('generateForm'));
                        on(generateFormButton, 'click', function () {
                            GenerateFormHandler();
                        });
                        GenerateFormHandler(generateFormButton); // To autopress button on load

                        var setFormDataButton = new buttonWidget({
                            singlebutton: "Set Form Data"
                        });
                        setFormDataButton.placeAt(dom.byId('setFormData'));
                        on(setFormDataButton, 'click', function () {
                            ExistingDataHandler();
                        });
                        ExistingDataHandler(setFormDataButton); // To autopress button on load

                        var clearFormDataButton = new buttonWidget({
                            singlebutton: "Clear Form Data"
                        });
                        clearFormDataButton.placeAt(dom.byId('clearFormData'));
                        on(clearFormDataButton, 'click', function () {
                            ClearFormDataHandler();
                        });

                        var toggleDisableButton = new buttonWidget({
                            singlebutton: "Toggle Disable"
                        });
                        toggleDisableButton.placeAt(dom.byId('toggleDisable'));
                        on(toggleDisableButton, 'click', function () {
                            ToggleDisableHandler();
                        });

                        var toggleReadOnlyButton = new buttonWidget({
                            singlebutton: "Toggle ReadOnly"
                        });
                        toggleReadOnlyButton.placeAt(dom.byId('toggleReadOnly'));
                        on(toggleReadOnlyButton, 'click', function () {
                            ToggleReadOnlyHandler();
                        });

                        var clearFormOutputButton = new buttonWidget({
                            singlebutton: "Clear Form Output"
                        });
                        clearFormOutputButton.placeAt(dom.byId('clearFormOutput'));
                        on(clearFormOutputButton, 'click', function () {
                            ClearFormOutputHandler();
                        });

                        function GenerateFormHandler() {
                            var JSONText = DataEntryForm.JSONInputText.textAreaText();
                            if (DataEntryForm.isJsonString(JSONText))
                            {
                                var wrapper = dom.byId('wrapper');
                                wrapper.innerHTML = "";
                                var obj = JSON.parse(JSONText);
                                DataEntryForm.form = new formWidget();
                                DataEntryForm.form.Start(obj);
                                DataEntryForm.form.placeAt(dom.byId('wrapper'));

                                DataEntryForm.form.on("formsubmit", function (data) {
                                    DataEntryForm.formOutputText.SetValue(data);
                                }.bind(this));
                            } else
                            {
                                alert("Syntax error in JSON input");
                            }
                        }
                        function ExistingDataHandler() {
                            // clears form before setting data
                            DataEntryForm.form.ClearForm();

                            var JSONText = DataEntryForm.existingDataText.textAreaText();
                            if (DataEntryForm.isJsonString(JSONText))
                            {
                                var obj = JSON.parse(JSONText);
                                DataEntryForm.form.SetForm(obj);
                            } else if (JSONText.trim().length !== 0)
                            {
                                alert("Syntax error in Existing Data");
                            }
                        }
                        function ClearFormDataHandler()
                        {
                            DataEntryForm.form.ClearForm();
                        }
                        function ToggleDisableHandler()
                        {
                            DataEntryForm.form.DisableToggle();
                        }
                        function ToggleReadOnlyHandler()
                        {
                            DataEntryForm.form.ReadOnlyToggle();
                        }
                        function ClearFormOutputHandler()
                        {
                            DataEntryForm.formOutputText.SetValue("");
                        }

                        // Sets the height of the columns to match the height of the user window or form length.
                        var firstCol = document.getElementById("firstCol");
                        var secondCol = document.getElementById("secondCol");
                        var thirdCol = document.getElementById("thirdCol");

                        var maxColHeight = window.innerHeight;

                        if (secondCol.offsetHeight > window.innerHeight) {
                            maxColHeight = document.body.offsetHeight;
                        }

                        // The expression (firstCol.offsetHeight * 2) comes from the fact that the JSON input field is generated after
                        // this code runs. A work around is to simply double the height of the initial form to get the height of the final.
                        var firstColHeight = firstCol.offsetHeight * 2;

                        // The first 20 refers to the adjustment needed so that there is no scroll bar when the form is initialized.
                        // The second 20 is the number of pixels each text area row takes, 2 is the number of text areas in the column.
                        var numberOfTextAreaRows = (maxColHeight - firstColHeight - 20) / (20 * 2);
                        DataEntryForm.JSONInputText.textarea.rows = numberOfTextAreaRows;
                        DataEntryForm.existingDataText.textarea.rows = numberOfTextAreaRows;

                        // The length of formOutputText is simply (numberOfTextAreaRows * 2) plus the length of the labels of the first col,
                        // which are both 20px, or 2 rows.
                        DataEntryForm.formOutputText.textarea.rows = (numberOfTextAreaRows * 2) + 2;

                        // Sets the second column height if either column is bigger than the form. This is to expand the form to the length
                        // of the window for aesthetic purposes.
                        var tallestCol = firstColHeight;

                        if (thirdCol.offsetHeight > tallestCol) {
                            tallestCol = thirdCol.offsetHeight;
                        }

                        if (tallestCol > secondCol.offsetHeight) {
                            secondCol.style.height = tallestCol + "px";
                        }
                    }
            );
    baseURL = loc;
}