		function initBroosWidget() 
		{
			require
			(
				[ "dojo/dom", 'components/sample/broosWidget'], 
				function(dom, broosWidget) 
				{
					var broosWidget = new broosWidget();
					broosWidget.placeAt(dom.byId('wrapper'));
				}
			);
		};