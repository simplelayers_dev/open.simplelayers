define([
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "sl_modules_open/sl_URL",
    "dojo/_base/xhr",
], function (dom, domAttr, lang, sl_url, xhr, as) {
    return {
        baseURL: null,

        exec: function (do_cmd, params, handler) {
            this.baseURL = sl_url.getAPIPath();
            console.log(this.baseURL);
            if (!as)
                as = 'json';
            params.format = 'json';
            var doURL = this.baseURL + do_cmd + '/';
            xhr.post({
                url: doURL,
                content: params,
                handleAs: as,
                load: function (result) {
                    handler(result);
                }
            });
            //handleAs:as,
        },
        isset: function (seeSet) {
            if (seeSet === null || typeof seeSet === 'undefined')
                return false;
            else
                return true;
        },
        Has: function (src, what) {
            if (src.hasOwnProperty(what))
                if (this.isset(src[what]))
                    return true;
            return false;
        },
        Get: function (src, what, default_val) {
            if (this.Has(src, what))
                return src[what];
            return default_val;
        },
        ForceOne: function (src, what, defaultVal) {
            if (src.hasOwnProperty(what))
                if (this.isset(src[what]) === false)
                    src[what] = defaultVal;
            return src;
        },
        GetJSON: function (src, what, defaultVal) {
            var content = src[what];
            if (this.isset(content) === false)
                return defaultVal;

            // content = JSON.stringify(src, [what]);
            try {
                content = JSON.parse(src[what]);
            } catch (e) {
                return src[what];
            }
            ;
            return content;
        },
        GetBoolean: function () { // Function accepts an unknown amount of arguments,
            // accessed using the "arguments" variable. The first variable MUST be the src object
            var src = arguments[0];
            if (typeof src !== "object")
                return null;
            var boolVal = {};
            var arg;
            for (var i = 1; i < arguments.length; ++i)
            {
                arg = arguments[i];
                if (this.isset(src[arg]))
                {
                    src[arg] = src[arg].toLowerCase();
                    if (src[arg] === "true" || src[arg] === "t")
                        boolVal[arg] = true;
                    else
                        boolVal[arg] = false;
                } else
                    boolVal[arg] = null;
            }
            return boolVal;
        },
        GetList: function (src, delim, param) {
            if (this.isset(src[param]) === false) {
                return null;
            }
            return src[param].split(delim);

        },
        RequireJSON: function (src, what) {
            var message;
            if (this.Has(src, what))
                if (this.isset(src[what]))
                {
                    try {
                        JSON.parse(src[what]);
                    } catch (e) {
                        console.log("The " + what + " property is not valid JSON.");
                        return src[what];
                    }
                    return JSON.parse(src[what]);
                } else
                    console.log("The " + what + " property is not set.");
            else
                console.log(what + " is not a valid object key.")
            return null;
        },
        Requires: function (src) {
            var message;
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            var arg;
            var wrongArg = [];
            var message;
            var values = [];
            var val;
            var subString;
            for (var i = 1; i < len; ++i)
            {
                arg = arguments[i];
                if (this.Has(src, arg) === false) {
                    wrongArg[wrongArg.length] = arg;
                } else {
                    val = src[arg].trim();
                    subString = val.substr(0, 6);
                    if (subString.indexOf('json'))
                    {
                        try {
                            JSON.parse(src[arg]);
                        } catch (e) {
                            console.log("The " + arg + " property is not valid JSON.");
                        }
                        //val = val['json'];
                    }
                    values[values.length] = val;
                }
            }
            if (wrongArg.length > 0)
            {
                message = "Missing from object: " + wrongArg[0];
                for (var i = 1; i < wrongArg.length - 1; ++i)
                    message = message + ", " + wrongArg[i];
                if (wrongArg.length > 1)
                    message = message + " and " + wrongArg[wrongArg.length - 1];
                console.log(message);
                return null;
            }
            return values;
        },
        RequiresOne: function (src) {
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            var arg;
            for (var i = 1; i < len; ++i)
            {
                arg = arguments[i];
                if (this.Has(src, arg))
                    return src[arg];
            }
            var message = "No matches were found."
            console.log(message);
        },
        Prune: function (src) {
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            var arg;
            for (var i = 1; i < len; ++i)
            {
                arg = arguments[i];
                if (this.Has(src, arg))
                    if (this.isset(src[arg]))
                        delete src[arg];
            }
            return src;
        },
        GetOne: function (src) {
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            for (var i = 1; i < len; ++i)
            {
                if (this.Has(src, arguments[i]))
                    return src[arguments[i]];
            }
            console.log("No matching argument was found.")
            return null;
        },
        GetAllValues: function (src) {
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            var val = [];
            for (var i = 1; i < len; ++i)
            {
                if (this.Has(src, arguments[i]))
                {
                    if (this.isset(src[arguments[i]]))
                        val[val.length] = src[arguments[i]];
                    else
                        val[val.length] = null;
                } else
                    val[val.length] = null;
            }
            return val;
        },
        GetValues: function (src) {
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            var arg = [];
            var values = {};
            var as;
            for (var i = 1; i < len; ++i)
            {
                arg = arguments[i].split(':');
                as = '';
                if (arg.length === 2)
                {
                    as = arg[1];
                    arg = arg[0];
                } else
                    as = arg;
                if (this.Has(src, arg)) {
                    if (this.isset(src, arg))
                        values[as] = src[arg];
                    else
                        values[as] = null;
                } else
                    values[as] = null;
            }
            return values;
        },
        GetRequiredValues: function (src) {
            if (typeof src !== 'object')
                return null;
            var missing = [];
            var values = {};
            var arg = {};
            var as;
            var len = arguments.length;
            for (var i = 1; i < len; ++i)
            {
                arg = arguments[i].split(':');
                as = '';
                if (arg.length === 2)
                {
                    as = arg[1];
                    arg = arg[0];
                } else
                    as = arg;
                if (this.Has(src, arg))
                {
                    if (this.isset(src[arg]))
                        values[as] = src[arg];
                    else
                        missing[missing.length] = arg;
                } else
                    missing[missing.length] = arg;
            }
            if (missing.length !== 0)
            {
                var message = "Missing required parameters: " + missing[0];
                for (var i = 1; i < missing.length - 1; ++i)
                    message = message + ", " + missing[i];
                if (missing.length > 1)
                    message = message + " and " + missing[missing.length - 1];
                console.log(message);
            }
            return values;
        },
        ListValues: function (src) {
            if (typeof src !== 'object')
                return null;
            var len = arguments.length;
            var val = [];
            var arg;
            for (var i = 1; i < len; ++i)
            {
                arg = arguments[i];
                if (this.Has(src, arg))
                {
                    if (this.isset(src[arg]))
                        val[val.length] = src[arg];
                    else
                        val[val.length] = null;
                } else
                    val[val.length] = null;
            }
            return val;
        },
        ParseParams: function (src) {
            /*        var params = '{';
             var keysSrc = [];
             keysSrc = Object.keys(src);
             var split = [];
             var val;
             var key;
             var nVal;
             for(var i = 0; i < keysSrc.length; ++i)
             {
             if(i > 0)
             params += ':';
             if(src[keysSrc[i]].indexOf(':'))
             {
             split = src[keysSrc[i]].split(src[keysSrc[i]]);
             key = split[0];
             val = split[1];
             key = "\"key\"";
             if(val.match('/\D/') !== null)
             val = "\"val\"";
             else
             {
             nVal = 0+val;
             if(val.substr(0, 1) === "0")
             val = "\"val\"";
             else
             val = nVal;
             }
             src[keysSrc[i]] = key + ":" + val;
             params += src[keysSrc[i]];
             }
             else
             {
             if(src[keysSrc[i]].match('/\D/'))
             params += "\"src[keysSrc[i]]\"";
             else
             params += src[keysSrc[i]];
             }
             }
             params = params + "}";
             console.log(params);*/
            params = JSON.stringify(src);
            return params;
        },
        MinMergeLists: function (id, val) {
            // The first argument is an object's property, the second argument
            // is a value, and all other arguments are arrays of objects
            if (arguments.length <= 3)
                return arguments;
            var result = [];
            var argJ;
            var value = null;
            var firstArg = arguments[2];
            if (typeof firstArg !== "object")
                return null;
            for (var j = 2; j < arguments.length; ++j)
            {
                argJ = arguments[j];
                if (typeof argJ !== "object")
                    return null;
                if (this.Has(argJ, id) && this.Has(firstArg, id))
                {
                    if (argJ[id] === firstArg[id])
                    {
                        if (value === null || argJ[val] < value)
                            value = argJ[val];
                    }
                }
            }
            result[firstArg[id]] = value;
            return result;
        },
        GetSubValues: function (params, subId) {
            var subs = [];
            var ids = [];
            ids = subId.split('.');
            var parKeys = [];
            if (typeof params !== "object")
                return null;
            parKeys = Object.keys(params);
            var len = parKeys.length;
            var subItem;
            var item;
            for (var i = 0; i < len; ++i)
            {
                subItem = params[parKeys[i]];
                if (typeof subItem === "object") {
                    for (var j = 0; j < ids.length; ++j)
                    {
                        if (this.isset(subItem))
                            if (this.Has(subItem, ids[j]))
                                subs[subs.length] = subItem[ids[j]];
                        //subItem = subItem[ids[j]];
                    }
                }
                // if(this.isset(subItem))
                //   subs[subs.length] = subItem;
            }
            return subs;
        },
        BoolToTF: function (boolVar) {
            if (this.isset(boolVar) === false)
                boolVar = false;
            if (boolVar === true)
                boolVar = 't';
            else
                boolVar = 'f';
            return boolVar;
        },
        IsUndefined: function (what) {
            return (what === undefined);
        },
        IsNull: function (what) {
            return (what === null);
        },
        IsFalse: function (what) {
            return (what === false);
        },
        IsEmpty: function (what) {
            return (what.trim() == "");
        },
        IsNot: function (what) {
            return (["", null, undefined, false, "null", "undefined", "false"].indexOf(what) > -1);
        },
        Is: function (what) {
            return (["", null, undefined, false, "null", "undefined", "false"].indexOf(what) == -1);
        }
    }
});