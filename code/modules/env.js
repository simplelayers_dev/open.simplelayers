define([], function () {
    return {
        GetMessager:function(handler)  {
            if( handler === undefined) handler = function(){};
            if(typeof OPEN_SL_MESSAGER !== 'undefined') handler(OPEN_SL_MESSAGER);
            require(['sl_components_open/modal_messager/ModalMessager'],function(ModalMessager) {
                OPEN_SL_MESSAGER = new ModalMessager();
                handler(OPEN_SL_MESSAGER);
            });            
        },
        GetWindowSize: function () {
            var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            return (w < 768) ? 'xs' : ((w < 992) ? 'sm' : ((w < 1200) ? 'md' : 'lg'));
        },
        DetectWindowSZ:function() {
            var me =this;
          require(['dojo/on'],function(on) {
              on(window,'resize',me.UpdateWindowSZ.bind(me));
          });
          this.UpdateWindowSZ();
        },
        UpdateWindowSZ: function () {
            var me = this;
            require(['dojo/dom-class'], function (domClass) {

                var sizes = ['xs', 'sm', 'md', 'lg'];
                var winSize = me.GetWindowSize();
                for (var i in sizes) {
                    domClass.toggle(document.body, sizes[i], winSize === sizes[i]);
                }
                var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;;
                domClass.toggle(document.body,'sm-md', (w >= 875) && (w < 1200));
                WINDOW_SMMD = domClass.contains(document.body,'sm-md');
                WINDOW_SZ = winSize;
            });

        },
        AddCSS: function (cssPath) {
            require(['dojo/dom-construct', 'dojo/query'], function (domCon, query) {
                var ctr = 0;
                query('link[href=”' + cssPath + '”]').forEach(function (node) {
                    ctr++;
                });
                if (ctr > 0)
                    return;
                domCon.create('link', {rel: 'stylesheet', 'href': cssPath}, document.head);
            });
        },
        AddCSS_postBootStrap: function (cssPath) {
            css_postBootStrapPaths = cssPath;
        },
        AddScript: function (scriptPath) {
            require(['dojo/dom-construct', 'dojo/query'], function (domCon, query) {
                var ctr = 0;
                query('link[src=”' + scriptPath + '”]').forEach(function (node) {
                    ctr++;
                });
                if (ctr > 0)
                    return;
                domCon.create('script', {src: scriptPath}, document.head);
            });
        },
        RequireBootStrap: function (handler) {
            if ('undefined' === typeof bootStrapLoaded) {
                bootStrapLoaded = false;
            }
            if (bootStrapLoaded === true) {
                if (handler !== undefined)
                    handler();
            }
            if ('undefined' === typeof jQuery) {
                this.RequireJQuery(this.RequireBootStrap.bind(this));
                return;
            }

            this.AddCSS(baseURL + "lib/bootstrap/css/bootstrap.min.css");
            this.AddCSS(baseURL + "lib/bootstrap/css/bootstrap-theme.min.css");
            this.AddCSS(baseURL + "lib/bootstrap_slider/bootstrap-slider.min.css");
            //this.AddCSS(baseURL + "lib/bootstrap-select-1.12.4/bootstrap-select-min.css");

            if ((typeof css_postBootStrapPaths != "undefined") && css_postBootStrapPaths.length > 0) {
                for (var i = 0; i < css_postBootStrapPaths.length; i++) {
                    this.AddCSS(css_postBootStrapPaths[i]);
                }
            }
            var self = this;
            this.AddScript(baseURL + "lib/bootstrap_slider/bootstrap-slider.min.js");
            this.AddScript(baseURL + "lib/bootstrap/js/bootstrap.min.js");
            //require(["lib/bootstrap/js/bootstrap.min",""], function (bootstrap) {
            bootStrapLoaded = true;
            if (handler !== undefined)
                handler();
            //});
        },
        RequireJQuery: function (handler) {
            if ('undefined' !== typeof jQuery) {
                handler();
                return;
            }

            jQuery = null;
            var me = this;
            require(["lib/jquery"], function (jQ) {
                if (jQuery === null) {
                    jQuery = jQ;
                    
                }
                $ = jQuery;
                require(['lib/jquery.splitter'], function (splitter) {
                    handler();
                });
            });
        },
        RequireTurf: function (handler) {
            if ('undefined' !== typeof turf) {
                if (handler !== undefined) {
                    handler();
                    return;
                }

            }
            ;
            turf = null;
            require(["lib/turf.min"], function (t) {
                turf = t;
                if (handler !== undefined) {
                    handler();
                    return;
                }
            });
        },
        RequireVincenty: function (handler) {


            if ('undefined' === typeof LatLon) {

                handler();
            } else {
                if (handler !== undefined) {
                    handler();
                }
            }


        },
        RequireLeaflet: function (handler) {
            if ('undefined' !== typeof L) {

                if (handler !== undefined) {
                    handler();
                    return;
                }
                return;
            }
            this.AddCSS('lib/leaflet-0.7.3/leaflet.css');
            L = null;
            require(["lib/leaflet-0.7.3/leaflet-src"], function (leaflet) {
                if (L === null)
                    L = leaflet;
                if (handler !== undefined) {
                    handler();
                    return;
                }
            });
        },
        RequireOpenLayers: function (handler) {
            if ('undefined' !== typeof OpenLayers) {
                if (handler !== undefined) {
                    handler();
                    return;
                }
                return;
            }
            OpenLayers = null;
            require(["lib/ol"], function (ol) {
                if (OpenLayers === null)
                    OpenLayers = ol;
                if (handler !== undefined) {
                    handler();
                    return;
                }
            });
        },
        RequireDGridCSS: function (handler) {
            if (typeof dgridLoaded === "undefined") {
                this.AddCSS(baseURL + "lib/dgrid-master/css/dgrid.css");
                dgridLoaded = true;
                if (typeof handler !== "undefined")
                    handler();
            } else {
                if (typeof handler !== "undefined") {
                    handler();
                    return;
                }
            }
        },
        RequireDojoStyles: function (handler) {
            if (typeof dojoStylesLoaded === "undefined") {
                this.AddCSS(baseURL + "lib/dojo-release-1.10.4/dijit/themes/dijit.css");
                this.AddCSS(baseURL + "lib/dojo-release-1.10.4/dijit/themes/tundra/tundra.css");
                this.AddCSS(baseURL + "lib/dojo-release-1.10.4/dijit/themes/tundra/Dialog.css");
                dojoStylesLoaded = true;
            } else {
                if (typeof handler !== "undefined") {
                    handler();
                }
                return;
            }
        }
    };
});