define([
    "sl_modules_open/sl_URL",
    "sl_modules_open/sl_Permissions",
    "dojo/_base/xhr",
    "dojo/_base/lang",
    "dojo/json",
], function (sl_url, sl_Permissions, xhr, lang, JSON) {
    return {
        baseURL: null,
        exec: function (do_cmd, params, handler, as) {
            this.baseURL = sl_url.GetAPIPath();
            //console.log(this.baseURL);
            if (!as)
                as = 'json';
            params.format = 'json';
            if (typeof sl_WAPI !== "undefined") {
                params.token = sl_WAPI.token;
            }
            var doURL = this.baseURL + do_cmd + '/';
            xhr.post({
                url: doURL,
                content: params,
                handleAs: as,
                load: function (result) {
                    handler(result);
                }
            });
            //handleAs:as,
        },
        Authenticate: function (params, optHandler) {
            sl_WAPI = params;
            this.exec('auth/authenticate/application:' + params.application, params,
                    (function (results) {
                        this.Authenticated(results, optHandler);
                    }).bind(this));
        },
        Authenticated: function (results, optHandler) {
            lang.mixin(sl_WAPI, results);
            var perms = sl_WAPI.permissions;
            sl_WAPI.permissions = new sl_Permissions();
            sl_WAPI.permissions.SetPermissions(JSON.parse(perms.substr(6)));
            if (optHandler !== undefined) {

                optHandler(results);
            }
        },
        HasToken: function () {
            return ([undefined, null, ''].indexOf(sl_WAPI.token));
        }
    }
});