define(['sl_modules_open/WAPI','lib/base64_to_blob'], function (WAPI,b64tob) {
    window.b64toBlob = b64tob;
    return {
        getIconClass: function (type, geom) {
            var layerIconType = null;
            switch (parseInt(type)) {
                case 0: //none
                    return "unknown";
                case 1: //vector
                    break;
                case 2: //raster
                    return "raster";
                case 3: //wms
                    return "wms2";
                case 4: //odbc
                    return "unknown";
                case 5: //relational
                    return "relational";
                case 6: //collection
                    return "collection";
                case 7: //smart_layer
                    return "unknown";
                case 8: //relatable
                    return "unknown";
                default:
                    return "unknown";
            }
            switch(parseInt(geom)){
                 case 0: //unknown
                    return "unknown";
                case 1: //point
                    return "point";
                case 2: //polygon
                    return "poly";
                case 3: //line
                    return "line";
                case 4: //raster
                    return "raster";
                case 5: //wms
                    return "wms2";
                case 6: //collection
                    return "collection";
                case 7: //relatable
                    return "unknown";
                default:
                    return "unknown";
            }
        },
        GetImageURI:function(handler,layerId,bbox,width,height,map_type,color,uncolor,gids,useUnnormal,features) {
            if(layerId === null) return;
            var params = {'layer':layerId,'bbox':bbox,'width':width,'height':height,'color':color,'uncolor':uncolor,"gids":gids,'maptype':map_type};
            if(features !== null) params['features'] = features;
            if(useUnnormal===true) params['unnormal']=1;
            params['base64'] = 1;
            WAPI.exec('layers/render',params,(function(data) {
                var url = window.URL || window.webkitURL;
                data.image.content = url.createObjectURL(b64toBlob(data.image.content,'image/png'))
                handler(data);
            }).bind(this),'json');
        }
      
    };
});