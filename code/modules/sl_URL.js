// In demo/myModule.js (which means this code defines
// the "demo/myModule" module):

define([
    // The dojo/dom module is required by this module, so it goes
    // in this list of dependencies.
    "dojo/dom",
    "dojo/dom-attr",
    "dojo/_base/array",
    "dojo/_base/lang"
], function (dom, domAttr, array, lang) {
    // Once all modules in the dependency list have loaded, this
    // function is called to define the demo/myModule module.
    //
    // The dojo/dom module is passed as the first argument to this
    // function; additional modules in the dependency list would be
    // passed in as subsequent arguments.


    // This returned object becomes the defined value of this module
    return {
        devURL: "https://dev.simplelalyers.com/",
        prodURL: "https://secure.simplelayers.com/",
        SetAPIPath: function (apiPath, optSandbox) {
            sl_APIPath = apiPath;
            if (optSandbox !== undefined)
                this.SetSandBox(optSandBox);
        },
        SetSandbox: function (sandbox) {
            sl_sandbox = sandbox;

        },
        SetBaseDir: function (dir) {
            sl_baseDir = dir;
            baseURL = this.GetServerPath + sl_baseDir;
            
            domAttr.set()
            console.log(baseDir);
        },
        GetServerPath: function () {

            var trimSandbox = false;
            if (arguments.length > 0) {
                var arg = arguments[0];
                if (arg == 'trimSandbox')
                    trimSandbox = true;
            }
            var url = document.location.href;
            var hasSandbox = url.indexOf('~') > 0;
            if(hasSandbox) {
                var sandbox = url.substr(url.indexOf('~')+1);
                sandbox = sandbox.substr(0,sandbox.indexOf('/'));
                this.SetSandbox(sandbox);
            }
            var pathSegs = url.split('/');
            var baseURL = pathSegs.shift();
            if (hasSandbox)
                baseURL += "/" + pathSegs.shift();

            baseURL += '//';


            var numSegs = (hasSandbox && !trimSandbox) ? 3 : 1;

            if (sl_sandbox) {
                numSegs = 1;
            }

            for (var i = 0; i < numSegs; i++)
            {
                baseURL += pathSegs[i] + '/';
            }

            if (document.sl_sandbox) {
                baseURL += '~' + document.sl_sandbox + '/';
            }
            
            return baseURL;
        },
        GetEmptyImgURL: function () {
            return baseURL + 'code/media/images/empty.png';
            //return this.prodURL + 'media/images/empty.png';
        },
        GetAPIPath: function () {
            var baseURL = sl_APIPath;

            return baseURL + 'wapi/';

        },
        GetURLParams: function (url) {
            var query = "";
            if (url.indexOf('?') >= 0) {
                var info = url.split('?');
                base = info.shift();
                query = info.join('?');
            }

            var paramData = query.split('&');
            params = [];

            array.forEach(paramData, function (item) {
                if (item == "")
                    return;
                ;
                var parts = item.split('=')
                var key = parts[0];
                var item = {};
                if (parts.length > 1) {
                    item.paramName = key;
                    item.paramValue = lang.trim(decodeURIComponent(parts[1]));
                } else {
                    item.paramName = key;
                    item.paramValue = true;
                }
                this.push(item);
            }, params);

            return params;
        },
        GetURLParamObj: function (url) {
            var query = "";
            if (url.indexOf('?') >= 0) {
                var info = url.split('?');
                base = info.shift();
                query = info.join('?');
            }

            var paramData = query.split('&');
            params = [];

            array.forEach(paramData, function (item) {
                if (item == "")
                    return;
                ;
                var parts = item.split('=')
                var key = parts[0].toLowerCase();

                if (parts.length > 1) {
                    params[key] = lang.trim(decodeURIComponent(parts[1]));
                } else {
                    params[key] = true;
                }
            }, params);

            return params;
        },
        GetURLBase: function (url) {
            var info = url.split('?');
            base = info.shift();
            return base;

        },
        URLFromParamItems: function (base, params) {
            if (base.indexOf('?') < 0)
                base += '?';
            var url = base;
            array.forEach(params, function (item, i) {
                url += '&' + item.paramName + '=' + encodeURIComponent[lang.trim(item.paramVal)];
            }, url);
            return url;
        },
        URLFromParamObj: function (baseURL, params) {
            if (baseURL.indexOf('?') < 0)
                base += '?';
            var url = baseURL;
            for (var key in params) {
                url += '&' + key + '=' + encodeURIComponent(params[key]);
            }
            return url;
        }

    };
});
