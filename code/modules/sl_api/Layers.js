define([
    "sl_modules_open/WAPI",
    "sl_modules_open/sl_URL"
], function (WAPI, sl_url) {
    if (typeof (LAYERS_OBJ) !== 'undefined') {
        return LAYERS_OBJ;
    }
    LAYERS_OBJ = {
        LAYERTYPE_NONE: 0,
        LAYERTYPE_VECTOR: 1,
        LAYERTYPE_RASTER: 2,
        LAYERTYPE_WMS: 3,
        LAYERTYPE_ODBC: 4,
        LAYERTYPE_RELATIONAL: 5,
        LAYERTYPE_COLLECTION: 6,
        LAYERTYPE_SMART_LAYER: 7,
        LAYERTYPE_RELATABLE: 8,
        baseURL: null,
        _memory: null,
        _layerList: null,
        SetMemory: function (calcMemoryObj) {
            this._memory = calcMemoryObj;
        },
        GetMemory:function() {
            return this._memory;
        },
        GetSharedView: function (by, handler) {
            WAPI.exec('layers/views/action:view/type:shared/owner:' + by, {}, handler.bind(this));
        },
        GetThumbnailURL: function (layerID) {
            this.baseURL = sl_url.GetAPIPath();
            return this.baseURL + "download/thumbnail/layer:" + layerID + "/token:" + sl_WAPI.token;
        },
        GetLoadedLayer: function (layerID, handler) {
            WAPI.exec('layers/layer/action:load/layer:' + layerID, {}, handler.bind(this));
        },
        GetDownloadLink: function (layerID, type) {
            return sl_APIPath + "?&do=download." + type + "&id=" + layerID + "&token=" + sl_WAPI.token;
        },
        SearchFeatures: function (params, handler) {
            if (typeof params.sort === "undefined") {
                params.sort = JSON.stringify([{field: "gid", direction: "ASC"}]);
            }
            WAPI.exec("features/search", params, handler);
        },
        GetLayerLegend:function(pLayerId, mapId, handler) {
            WAPI.exec('layers/layer/legend/format:json/action:list/', {'player': pLayerId, 'project': mapId}, handler);
        },
        IsLayerTabular: function (layerType) {
            return [this.LAYERTYPE_VECTOR, this.LAYERTYPE_ODBC, this.LAYERTYPE_RELATIONAL, this.LAYERTYPE_RELATABLE].indexOf(parseInt(layerType)) > -1;
        },
        GetAggData: function (user, handler) {
            if (!WAPI.HasToken())
                return;
            if ([undefined, null, ''].indexOf(user) > -1)
                return;
            WAPI.exec('all_layers/aggregate/format:json/view:shared/owner:' + user, {}, handler);
        },
        SetLayerList: function (listItems) {
            this._layerList = listItems;
        },
        GetSpatialLayerIds: function () {
            var layerIds = [];
            for (var l = 0; l < this._layerList.length; l++) {
                if (this._layerList[l].hasOwnProperty('geom')) {
                    if (this._layerList[l].geom !== null) {
                        layerIds.push(this._layerList[l].id);
                    }
                }
            }
            return layerIds;
        }


    }
    return LAYERS_OBJ;
});