/* global sl_WAPI, sl_APIPath */

define([
    "sl_modules_open/WAPI",
    "sl_modules_open/sl_URL"],
        function (WAPI, sl_url) {
            return {
                baseURL: null,
                GetSharedView: function (by, handler) {
                    WAPI.exec('map/views/action:view/type:shared/owner:' + by, {}, handler.bind(this));
                },
                GetThumbnailURL: function (layerID) {
                    this.baseURL = sl_url.GetAPIPath();
                    return this.baseURL + "download/thumbnail/map:" + mapID + "/token:" + sl_WAPI.token;
                },
//        GetLoadedLayer: function (layerID, handler) {
//            WAPI.exec('layers/layer/action:load/layer:' + layerID, {}, handler.bind(this));
//        },
//        GetDownloadLink: function (layerID, type) {
//            return sl_APIPath + "?&do=download." + type + "&id=" + layerID + "&token=" + sl_WAPI.token;
//        },
//        SearchFeatures: function (params, handler) {
//            if (typeof params.sort === "undefined") {
//                params.sort = JSON.stringify([{field: "gid", direction: "ASC"}]);
//            }
//            WAPI.exec("features/search", params, handler);
//        }
                BboxToBounds:function(bbox) {
                    if (typeof bbox === 'string') {
                        bbox = bbox.split(',');
                    }
                    var minX = bbox[0];
                    var minY = bbox[1];
                    var maxX = bbox[2];
                    var maxY = bbox[3];

                    var ul = {
                        "lng": minX,
                        "lat": maxY
                    };
                    var lr = {
                        "lng": maxX,
                        "lat": minY
                    };


                    return L.latLngBounds(ul,lr);
                },
                BoundsToBbox:function(bounds) {
                    var sw = bounds.getSouthWest();
                    var ne = bounds.getNorthEast();
                    var minX = sw.lng;
                    var minY = sw.lat;
                    var maxX = ne.lng;
                    var maxY = ne.lat;
                    var bbox = [minX,minY,maxX,maxY].join(',');
                    return bbox;
                },
                GetBbboxZoomLevel: function (bounds, mapDim) {
                    if (typeof bounds === 'string') {
                        bounds = bounds.split(',');
                    }
                    var minY = bounds[0];
                    var minX = bounds[1];
                    var maxY = bounds[2];
                    var maxX = bounds[3];

                    var boundingBox = {
                        "ul": {
                            "lng": minX,
                            "lat": maxY
                        },
                        "lr": {
                            "lng": maxX,
                            "lat": minY
                        }
                    };

                    var center = L.mapquest.util.getCenterFromBoundingBox(boundingBox);
                    console.log(center);

                    var zoom = Math.ceil(L.mapquest.util.getZoomFromBoundingBox(boundingBox));

                    return {"zoom": zoom, "center": center};
                },
                GetBBOxCenter:function(bbox) {
                    if(typeof bbox == 'string') {
                        bbox = bbox.split(',');
                    }
                    var minX = bbox[0];
                    var minY = bbox[1];
                    var maxX = bbox[2];
                    var maxY = bbox[3];
                    var center = [];
                    center[0] = minX+((maxX-minX)/2);
                    center[1] = minY+((maxY-minY)/2);
                    
                    return center;
                }
            }
        }
);