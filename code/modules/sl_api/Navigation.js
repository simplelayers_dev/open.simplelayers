/* global sl_WAPI, sl_APIPath */

define([
    "sl_modules_open/WAPI",
    "sl_modules_open/sl_URL"],
        function (WAPI, sl_url) {
            return {
                ZoomToFeature: function (layerId, features, okHandler) {
                    WAPI.exec('map/navigate', {'action': 'feature_bbox','layer_id':layerId,'features':features}, okHandler);

                }
            }
        }
);