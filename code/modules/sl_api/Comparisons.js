/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function () {
    return function () {
        this.COMPARESTR_NONE = 'select a comparison';
        this.COMPARESTR_EQUALS = 'equals';
        this.COMPARESTR_EXACTLY_EQUALS = 'exact match';
        this.COMPARESTR_NOT_EQUALS = 'does not equal';
        this.COMPARESTR_GT = 'is greater than';
        this.COMPARESTR_GT_OR_EQUAL = 'is greater than or equal to';
        this.COMPARESTR_LT = 'is less than';
        this.COMPARESTR_LT_OR_EQUAL = 'is less than or equal to';
        this.COMPARESTR_HAS = 'contains';
        this.COMPARESTR_NOT_HAS = 'does not contain';
        this.COMPARESTR_ISNULL = 'is null or empty';
        this.COMPARESTR_NOT_ISNULL = 'is not null or empty';
        this.COMPARESTR_IN = 'is in list';
        this.COMPARESTR_NOT_IN = "is not in list";
        this.COMPARESTR_ISNAN = 'is not a number';
        this.COMPARESTR_NOT_ISNAN = 'is a valid number';
        this.COMPARESTR_STARTS = 'starts with';
        this.COMPARSTR_ENDS = 'ends with';
        this.COMPARESTR_TRUE = "is true";
        this.COMPARESTR_FALSE = "is false";
        
        this.COMPARESTR_GEOM_NONE = "select a comparison",
        this.COMPARESTR_INTERSECTS ="intersects records";
        this.COMPARESTR_NOT_INTERSECTS ="doesn't intersect records";
        this.COMPARESTR_CONTAINS="encloses records areas";
        this.COMPARESTR_NOT_CONTAINS ="doesn't enclose records areas";
        
        this.COMPARESTR_IN_MEMORY = "includes records"
        this.COMPARESTR_NOT_IN_MEMORY = "doesn't include records"
        
        
        this.COMPARE_NONE = '';
        this.COMPARE_EQUALS = '==';
        this.COMPARE_EXACTLY_EQUALS = '===';
        this.COMPARE_NOT_EQUALS = '<>';
        this.COMPARE_GT = '>';
        this.COMPARE_GT_OR_EQUAL = '>=';
        this.COMPARE_LT = '<';
        this.COMPARE_LT_OR_EQUAL = '<=';
        this.COMPARE_HAS = 'contains';
        this.COMPARE_NOT_HAS = '!contains';
        this.COMPARE_ISNULL = 'isnull';
        this.COMPARE_NOT_ISNULL = 'not_isnull';
        this.COMPARE_ISNAN = 'isnan';
        this.COMPARE_NOT_ISNAN = "!isnan";
        this.COMPARE_IN = 'IN';
        this.COMPARE_NOT_IN = 'NOT IN';
        this.COMPARE_STARTS = 'starts';
        this.COMPARE_ENDS = 'ends';
        this.COMPARE_TRUE = true;
        this.COMPARE_FALSE = false;
        this.COMPARE_GEOM_NONE=0;
        this.COMPARE_INTERSECTS =1;
        this.COMPARE_NOT_INTERSECTS = -1;
        this.COMPARE_CONTAINS=2;
        this.COMPARE_NOT_CONTAINS =-2;
        this.COMPARE_IN_MEMORY = "in";
        this.COMPARE_NOT_IN_MEMORY = "not in";
        
        this.OPERATOR_AND = ' AND ';
        this.OPERATOR_OR = ' OR ';
        this.OPERATOR_MONGO_AND = '$and';
        this.OPERATOR_MONGO_OR = '$or';

        this.comparisonMap = {};
        this.comparisonMap[this.COMPARESTR_NONE] = this.COMPARE_NONE;
        this.comparisonMap[this.COMPARESTR_EQUALS] = this.COMPARE_EQUALS;
        this.comparisonMap[this.COMPARESTR_EXACTLY_EQUALS] = this.COMPARE_EXACTLY_EQUALS;
        this.comparisonMap[this.COMPARESTR_NOT_EQUALS] = this.COMPARE_NOT_EQUALS;
        this.comparisonMap[this.COMPARESTR_GT] = this.COMPARE_GT;
        this.comparisonMap[this.COMPARESTR_GT_OR_EQUAL] = this.COMPARE_GT_OR_EQUAL;
        this.comparisonMap[this.COMPARESTR_LT] = this.COMPARE_LT;
        this.comparisonMap[this.COMPARESTR_LT_OR_EQUAL] = this.COMPARE_LT_OR_EQUAL;
        this.comparisonMap[this.COMPARESTR_HAS] = this.COMPARE_HAS;
        this.comparisonMap[this.COMPARESTR_NOT_HAS] = this.COMPARE_NOT_HAS;
        this.comparisonMap[this.COMPARESTR_ISNULL] = this.COMPARE_ISNULL;
        this.comparisonMap[this.COMPARESTR_NOT_ISNULL] = this.COMPARE_NOT_ISNULL;
        this.comparisonMap[this.COMPARESTR_IN] = this.COMPARE_IN;
        this.comparisonMap[this.COMPARESTR_NOT_IN] = this.COMPARE_NOT_IN;
        this.comparisonMap[this.COMPARESTR_ISNAN] = this.COMPARE_ISNAN;
        this.comparisonMap[this.COMPARESTR_NOT_ISNAN] - this.COMPARE_NOT_ISNAN;
        this.comparisonMap[this.COMPARESTR_STARTS] = this.COMPARE_STARTS;
        this.comparisonMap[this.COMPARSTR_ENDS] = this.COMPARE_ENDS;
        this.comparisonMap[this.COMPARESTR_TRUE] = this.COMPARE_TRUE;
        this.comparisonMap[this.COMPARESTR_FALSE] = this.COMPARE_FALSE;
        this.comparisonMap[this.COMPARESTR_GEOM_NONE] = this.COMPARE_GEOM_NONE;
        this.comparisonMap[this.COMPARESTR_INTERSECTS] = this.COMPARE_INTERSECTS;
        this.comparisonMap[this.COMPARESTR_NOT_INTERSECTS] = this.COMPARE_NOT_INTERSECTS;
        this.comparisonMap[this.COMPARESTR_CONTAINS] = this.COMPARE_CONTAINS;
        this.comparisonMap[this.COMPARESTR_NOT_CONTAINS] = this.COMPARE_NOT_CONTAINS;
        this.comparisonMap[this.COMPARESTR_IN_MEMORY] = this.COMPARE_IN_MEMORY;
        this.comparisonMap[this.COMPARESTR_NOT_IN_MEMORY] = this.COMPARE_NOT_IN_MEMORY;
        
        this.anyComparisons = [];
        this.anyComparisons[this.COMPARESTR_NONE] = this.COMPARE_NONE;
        this.anyComparisons[this.COMPARESTR_EQUALS] = this.COMPARE_EQUALS;
        this.anyComparisons[this.COMPARESTR_EXACTLY_EQUALS] = this.COMPARE_EXACTLY_EQUALS;
        this.anyComparisons[this.COMPARESTR_NOT_EQUALS] = this.COMPARE_NOT_EQUALS;
        this.anyComparisons[this.COMPARESTR_GT] = this.COMPARE_GT;
        this.anyComparisons[this.COMPARESTR_GT_OR_EQUAL] = this.COMPARE_GT_OR_EQUAL;
        this.anyComparisons[this.COMPARESTR_LT] = this.COMPARE_LT;
        this.anyComparisons[this.COMPARESTR_LT_OR_EQUAL] = this.COMPARE_LT_OR_EQUAL;
        this.anyComparisons[this.COMPARESTR_HAS] = this.COMPARE_HAS;
        this.anyComparisons[this.COMPARESTR_NOT_HAS] = this.COMPARE_NOT_HAS;
        this.anyComparisons[this.COMPARESTR_ISNULL] = this.COMPARE_ISNULL;
        this.anyComparisons[this.COMPARESTR_NOT_ISNULL] = this.COMPARE_NOT_ISNULL;
        this.anyComparisons[this.COMPARESTR_IN] = this.COMPARE_IN;
        this.anyComparisons[this.COMPARESTR_NOT_IN] = this.COMPARE_NOT_IN;
        this.anyComparisons[this.COMPARESTR_ISNAN] = this.COMPARE_ISNAN;
        this.anyComparisons[this.COMPARESTR_NOT_ISNAN] - this.COMPARE_NOT_ISNAN;
        this.anyComparisons[this.COMPARESTR_STARTS] = this.COMPARE_STARTS;
        this.anyComparisons[this.COMPARSTR_ENDS] = this.COMPARE_ENDS;
        this.anyComparisons[this.COMPARESTR_TRUE] = this.COMPARE_TRUE;
        this.anyComparisons[this.COMPARESTR_FALSE] = this.COMPARE_FALSE;

        this.numericComparisons = {};
        this.numericComparisons[this.COMPARESTR_NONE] = this.COMPARE_NONE;
        this.numericComparisons[this.COMPARESTR_EXACTLY_EQUALS] = this.COMPARE_EXACTLY_EQUALS;
        this.numericComparisons[this.COMPARESTR_NOT_EQUALS] = this.COMPARE_NOT_EQUALS;
        this.numericComparisons[this.COMPARESTR_LT] = this.COMPARE_LT;
        this.numericComparisons[this.COMPARESTR_LT_OR_EQUAL] = this.COMPARE_LT_OR_EQUAL;
        this.numericComparisons[this.COMPARESTR_EXACTLY_EQUALS] = this.COMPARE_EXACTLY_EQUALS;
        this.numericComparisons[this.COMPARESTR_GT] = this.COMPARE_GT;
        this.numericComparisons[this.COMPARESTR_GT_OR_EQUAL] = this.COMPARE_GT_OR_EQUAL;
        this.numericComparisons[this.COMPARESTR_ISNAN] = this.COMPARE_ISNAN;
        this.numericComparisons[this.COMPARESTR_NOT_ISNAN] - this.COMPARE_NOT_ISNAN;
        this.numericComparisons[this.COMPARESTR_HAS] = this.COMPARE_HAS;
        this.numericComparisons[this.COMPARESTR_NOT_HAS] = this.COMPARE_NOT_HAS;
        this.numericComparisons[this.COMPARESTR_STARTS] = this.COMPARE_STARTS;
        this.numericComparisons[this.COMPARSTR_ENDS] = this.COMPARE_ENDS;
        
        this.stringComparisons = {};
        this.stringComparisons[this.COMPARESTR_NONE] = this.COMPARE_NONE;
        this.stringComparisons[this.COMPARESTR_EXACTLY_EQUALS] = this.COMPARE_EXACTLY_EQUALS;
        this.stringComparisons[this.COMPARESTR_NOT_EQUALS] = this.COMPARE_NOT_EQUALS;
        this.stringComparisons[this.COMPARESTR_HAS] = this.COMPARE_HAS;
        this.stringComparisons[this.COMPARESTR_NOT_HAS] = this.COMPARE_NOT_HAS;
        this.stringComparisons[this.COMPARESTR_ISNULL] = this.COMPARE_ISNULL;
        this.stringComparisons[this.COMPARESTR_NOT_ISNULL] = this.COMPARE_NOT_ISNULL;
        this.stringComparisons[this.COMPARESTR_IN] = this.COMPARE_IN;
        this.stringComparisons[this.COMPARESTR_NOT_IN] = this.COMPARE_NOT_IN;
        this.stringComparisons[this.COMPARESTR_STARTS] = this.COMPARE_STARTS;
        this.stringComparisons[this.COMPARSTR_ENDS] = this.COMPARE_ENDS;
        
        this.booleanComparisons = {};
        this.booleanComparisons[this.COMPARESTR_TRUE] = this.COMPARE_TRUE;
        this.booleanComparisons[this.COMPARESTR_FALSE] = this.COMPARE_FALSE;
        
        this.memoryComparisons = {};
        this.memoryComparisons[this.COMPARESTR_GEOM_NONE] = this.COMPARE_GEOM_NONE;
        this.memoryComparisons[this.COMPARESTR_IN_MEMORY] = this.COMPARE_IN_MEMORY;
        this.memoryComparisons[this.COMPARESTR_NOT_IN_MEMORY] = this.COMPARE_NOT_IN_MEMORY;
        this.memoryComparisons[this.COMPARESTR_INTERSECTS] = this.COMPARE_INTERSECTS;
        this.memoryComparisons[this.COMPARESTR_NOT_INTERSECTS] = this.COMPARE_NOT_INTERSECTS;
        this.memoryComparisons[this.COMPARESTR_CONTAINS] = this.COMPARE_CONTAINS;
        this.memoryComparisons[this.COMPARESTR_NOT_CONTAINS] = this.COMPARE_NOT_CONTAINS;
        
    };
});

