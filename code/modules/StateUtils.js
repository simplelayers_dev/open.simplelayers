// In demo/myModule.js (which means this code defines
// the "demo/myModule" module):

define([
    // The dojo/dom module is required by this module, so it goes
    // in this list of dependencies.
    "dojo/dom-class",
    "dojo/dom-attr"

], function (domClass, domAttr) {
    return {
        Hide: function (targeElement) {
            domClass.toggle(targeElement, 'hidden', true);

        },
        Show: function (targeElement) {
            domClass.toggle(targeElement, 'hidden', false);
        },
        Cloak: function (targeElement) {
            domClass.toggle(targeElement, 'invis');
        },
        UnCloak: function (targeElement) {
            domClass.toggle(targeElement, 'invis');
        },
        Enable: function (targetElement) {
            if (domAttr.has(targetElement, 'disabled')) {
                domAttr.remove(targetElement, 'disabled');
            }
        },
        Disable: function (targetElement) {
            if (!domAttr.has(targetElement, 'disabled')) {
                domAttr.set(targetElement, 'disabled','disabled');
            }
        }
    };
});
