define(["dojo/_base/declare", "dojo/dom-class","dojo/dom-attr"],
        function (declare, domClass,domAttr) {
            return declare("StateManager", [], {
                _states: null,
                _machineElement:null,
                _statePrefix:null,
                _currentState:null,
                _toggleStates:null,
                constructor: function (args) {
                    if(typeof args === 'undefined') args = {};
                    this._states = (args.hasOwnProperty('states')) ? args.states : [];
                    this._machineElement = args.hasOwnProperty('machine') ?  args.machine : null;
                    this._statePrefix = args.hasOwnProperty('prefix') ? args.prefix : 'state-';         
                    if(this._statePrefix.substr(-1,1)!=="-") this._statePrefix= this._statePrefix+"-";
                },
                
                SetToggleStates:function(onState,offState){
                  this._toggleStates = {"on":onState,"off":offState};  
                },
                SetPrefix:function(prefix){ 
                	this._statePrefix = prefix;
                	if(this._statePrefix.substr(-1,1)!=="-") this._statePrefix= this._statePrefix+"-";
                },
                SetMachine:function(element) {
                    this._machineElement = element;
                },
                AddState:function(state) {
                    this._states.push(state);
                },
                SetState:function(state) {
                    this.ToggleMachineState(false);
                    if(this._states.indexOf(state)>-1) {
                        this._currentState = this._statePrefix+state;
                    }
                    this.ToggleMachineState(true);
                },
                SetStates:function(states) {
                    this._states = states;
                },
                GetCurrentState:function() {
                    var prefix = (this._statePrefix===null) ? '' : this._statePrefix;
                    return this._currentState.substr(prefix.length);
                },
                IsState:function(state) {
                    return this.GetState() === state;
                },
                ToggleMachineState:function(optOnOff) {
                    if(this.machineElement === null) return;
                    if(this._currentState === null) return;
                    if(typeof optOnOff === 'unedefined') {
                        domClass.toggle(this._machineElement,this._currentState);
                    } else {
                        domClass.toggle(this._machineElement,this._currentState,optOnOff);
                    }
                },
                ToggleState:function(optOnOff) {
                  if(this._toggleStates === null) return;
                  if(optOnOff === true) {
                      this.SetState(this._toggleStates.on);
                      return this;
                  }
                  if(optOnOff === false) {
                      this.SetState(this._toggleStates.off);
                      return this;
                  }
                  if(this._toggleStates.on === this.GetCurrentState()) {
                      this.SetState(this._toggleStates.off);
                  } else {
                      this.SetState(this._toggleStates.on);
                  }
                  return this;
                },
                ClearStates:function() {                    
                  for(var i=0,l=this.states.length;i<l;i++) {
                        domClass.toggle(this.machineElement,this.states[i],false);
                  }  
                }
     
            });
        });

