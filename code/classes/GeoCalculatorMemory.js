/* 
 */
/* global L */

define(["dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/topic"], function (declare, lang, topic) {
    return declare('sl_classes_open/GeoCalculatorMemory', [], {
        _memoryLayer: null,
        _features: null,
        _name: null,
        countt: null,
        constructor: function (name) {
            this.name = name;
            this.Reset();
        },
        Clear: function () {
            return this.Reset();
        },
        Reset: function (notify) {
            this._memoryLayer = null;
            this._features = [];
            if (notify !== false)
                this.NotifyUpdateListeners();
            return this;
        },
        HasMemoryLayer: function () {
            return ([null, undefined, ''].indexOf(this._memoryLayer) < 0);
        },
        IsLayer: function (layerId) {
            if ([null, undefined, ''].indexOf(layerId) > -1) {
                return false;
            }
            return this._memoryLayer == layerId;
        },
        SetLayer: function (layerId) {
            this.Reset(false);
            this._memoryLayer = layerId;
            this.NotifyUpdateListeners();
            return this;
        },
        Add: function (features) {

            if (typeof features.constructor === Array) {
                features = features.split(',');
            }
            if (features.length === 0) {
                return;
            }

            for (var i = 0; i < features.length; i++) {
                if (this._features.indexOf(features[i]) < 0) {
                    this._features.push(features[i]);
                }
            }
            this.NotifyUpdateListeners();
            return this;
        },
        Remove: function (features) {
            if (features.constructor !== Array) {
                features = features.split(',');
            }
            for (var i = 0; i < features.length; i++) {
                var featureIndex = this._features.indexOf(features[i]);
                if (featureIndex > -1) {
                    this._features.splice(featureIndex, 1);
                }
            }
            if (this._features.length === 0) {
                return this.Reset();
            }
            this.NotifyUpdateListeners();
            return this;
        },
        Restore: function () {
            this.count = this._features.length;
            return {"layerId": this._memoryLayer, "features": this._features};
        },
        NotifyUpdateListeners: function () {
            this.count = this._features.length;
            topic.publish('geocalculator/updated', {"calculator": this});
        },
        FeaturesToString: function () {
            return this._features.join(',');
        }

    });
});
