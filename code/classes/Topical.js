define(["dojo/_base/declare", "dojo/topic"],
        function (declare, topic) {
            return declare("Topical", [], {
                _topicalSubcriptions: null,
                constructor: function () {
                    this._topicalSubscriptions = [];

                },
                TopicalSubscribe: function (subject, handler) {
                    this._topicalSubscriptions.push(topic.subscribe(subject, handler));
                },
                destroy: function () {
                    for (var i in this._topicalSubscriptions) {
                        this._topicalSubscriptions[i].remove();
                    }
                    this.inherited(arguments);

                }


            });
        });

