/* 
 */
/* global L */

define(["dojo/_base/declare",
    "dojo/_base/lang"], function (declare,lang) {
    return declare('sl_classes_open/InterruptableTimer', [], {
        _msecondsPerSample:300,
        _nextInterrupt:null,
        _isSampling:null,
        _args:null,
        _handler:null,
        _timerMSeconds:null,
        constructor: function (opt_msecondsPerSample) {
            if(typeof opt_msecondsPerSample !== "undefined") this._msecondsPerSample = opt_msecondsPerSample;            
        },
        MSecs:function() {
          return this._msecondsPerSample();  
        },
        Start:function(handler,timerMSeconds,opt_args) {
            this._handler = handler;
            this._timerMSeconds = timerMSeconds;
            this._args = opt_args;
            this._Interrupt();
            this._InitSampleTimer();
        },
        _Sample:function() {
                        
           if(this._sampling === false) return;
           if(this._nextInterrupt === null) {
               this._StopSampleTimer();
               return;
           }
            if(this._nextInterrupt <= (Date.now())) {
               this._handler(this._args);
               this._StopSampleTimer();
               return;
           }
           this._InitSampleTimer();           
           
           
           
        },
        _Interrupt:function() {
            this._nextInterrupt = Date.now()+this._timerMSeconds;
        },
        _InitSampleTimer:function() {
            this._sampling = true;
            setTimeout(this._Sample.bind(this), this._msecondsPerSamplme);
           
        },
        _StopSampleTimer:function() {
            this._sampling = false;
            this._handler = null;
            this._args = null;
        },
        
        
    });
});
