define(["dojo/_base/declare",
    "dojo/on",
    "dojo/Evented",
    "dojo/query",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/topic",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dijit/registry",
    "dojo/dom-class",
    "dojo/dom-style",
    "sl_modules_open/sl_api/Comparisons",
    "sl_classes_open/StateManager",
    "sl_modules_open/sl_api/Layers",
    "dojo/text!./CriteriaItem.tpl.html"
], function (declare,
        on,
        Evented,
        query,
        domAttr,
        domClass,
        domCon,
        topic,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        registry,
        domClass,
        domStyle,
        Comparisons,
        StateManager,
        Layers,
        template
        ) {
    return declare("CriteriaItem", [StateManager, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        first: null,
        fields: [],
        baseClass: "criteriaitem",
        _comparisons: null,
        _optionTpl: "",
        _fieldType: null,
        constructor: function (first, fields) {

            this.first = first;
            this.fields = fields;
            this._comparisons = new Comparisons();
        },
        postCreate: function () {
            this.SetMachine(this.domNode);
            this.SetPrefix('criterion')
            this.AddState('spatial');
            this.AddState('non-spatial');
            this.SetToggleStates('spatial', 'non-spatial');

            on(this.destroy_item, 'click', this.Destroy.bind(this));
            on(registry.byId(this.field_select), "change", this.CheckFieldType.bind(this));
            
            this._optionTpl = this.op_option_tpl.outerHTML;
            domCon.empty(this.operator_select);
            on(this.operator_select, 'change', this.HandleOperatorSelect.bind(this));
            if (this.first) {
                domClass.add(this.andor_select, "invis");
            }
            for (var i = 0, l = this.fields.length; i < l; i++) {
                if (['the_geom'].indexOf(this.fields[i].name) > -1)
                    continue;
                domCon.place('<option value="' + this.fields[i].name + '">' + this.fields[i].display + '</option>', this.field_select);
            }

            this.CheckFieldType();
        },
        HandleOperatorSelect: function (event) {
            var isBoolean = false;
            switch (this.operator_select.value) {
                case 'includes records':
                case "doesn't include records":
                    this.value_input.value = "{memory}";
                    break;
                case 'is true':
                case 'is_false':
                    domClass.toggle(this.value_input,'hidden');
                    isBoolean = true;
                    break;
                default:
                    if (this.field_select.value === "memory") {
                        this.value_input.value = "";
                    }
                    
            }
            domClass.toggle(this.value_input,'hidden',isBoolean);
            if (this.field_select.value !== "memory") {
                if (this.value_input.value === "{memory}") {
                    this.value_input.value = "";
                }
            }
            domClass.toggle(this.bufferContainer, 'hidden', this.value_input.value === "{memory}");
        },
        IsMemory: function () {
            return this.GetCurrentState() === 'spatial';
        },
        CheckFieldType: function () {
            domCon.empty(this.operator_select);

            var req = "";
            for (var i = 0, l = this.fields.length; i < l; i++) {
                if (this.fields[i].name === this.field_select.value) {
                    req = this.fields[i].requirement;
                    break;
                }
            }
            if (this.field_select.value !== "memory") {
                if (this.value_input.value === "{memory}") {
                    this.value_input.value = "";
                }
            }
            domClass.toggle(this.bufferContainer, 'hidden', this.value_input.value === "{memory}");


            this.SetOperatorOptions(req);
        },
        SetOperatorOptions: function (type) {
            this._fieldType = type;
            domCon.empty(this.operator_select);
            var options = [];
            this.SetState('non-spatial');
            switch (type) {
                case 'float':
                case 'int':
                    options = this._comparisons.numericComparisons;
                    break;
                case 'boolean':
                    options = this._comparisons.booleanComparisons;
                    break;
                case 'memory':
                    this.SetState('spatial');
                    options = this._comparisons.memoryComparisons;
                    break;
                case "any":
                    options = this._comparisons.anyComparisons;
                    break;
                default:
                    options = this._comparisons.stringComparisons;
                    break;
            }
            for (var o in options) {
                var option = options[o];
                var tpl = this._optionTpl.substr(0);
                tpl = tpl.replace(/\{value\}/g, option);
                tpl = tpl.replace(/\{label\}/g, o);
                tpl = tpl.replace(/data\-dojo\-attach\-point\=\"[^\"]+\"/g, "");
                if (type !== 'memory') {
                    tpl = tpl.replace(/req\-spatial/g, "req-nonspatial");
                }
                domCon.place(domCon.toDom(tpl), this.operator_select);
            }
        },

        IsFirst: function () {
            this.first = true;
            domClass.toggle(this.andor_select, "invis", this.first);
        },
        IsNotFirst: function () {
            this.first = false;
            domClass.toggle(this.andor_select, "invis", this.first);
        },
        IsMemory: function () {
            return this.field_select.value === 'memory';
        },
        Destroy: function () {
            this.destroy();
            topic.publish('criteria_item/destroy', {target: this});
        },
        SetData: function (field, compare, value, buffer, unit, andOr) {
            for (var i = 0; i < this.field_select.options.length; i++) {
                var opt = this.field_select.options[i];
                if (opt.value === field) {
                    this.field_select.options.selectedIndex = i;
                    this.CheckFieldType();
                    break;
                }
            }
            for (i = 0; i < this.operator_select.options.length; i++) {
                if (this.operator_select.options[i].value === compare) {
                    this.operator_select.selectedIndex = i;
                    break;
                }
            }
            if ([null, undefined].indexOf(value) < 0) {
                this.value_input.value = value;
            }
            domClass.toggle(this.bufferContainer, 'hidden', value === "{memory}");

            if (buffer !== undefined) {
                this.buffer_input.value = buffer;
            }
            if (unit !== undefined) {
                for (i = 0; i < this.units_sel.options.length; i++) {
                    if (this.units_sel.options[i] === unit) {
                        this.units_sel.selectedIndex = i;
                    }
                }
            }
            if (andOr !== undefined) {
                for (i = 0; i < this.andor_select.options.length; i++) {
                    if (this.andor_select.options[i] === unit) {
                        this.andor_select.selectedIndex = i;
                    }
                }
            }

        },
        GetData: function () {
            var data = {};

            data.field = this.field_select.value;
            data.compare = this._comparisons.comparisonMap[this.operator_select.value];
            data.value = this.value_input.value;
            if (data.value === "{memory}") {
                data.value = Layers.GetMemory().FeaturesToString();
            }

            data.modifier = "";
            data.fields = "";
            data.andor = "";
            data.is_not = "";

            if (data.field === 'memory') {
                var buffer = parseFloat(this.buffer_input.value.trim());
                if (!isNaN(buffer)) {
                    var units = this.units_sel.value;
                    switch (units) {
                        case 'ft':
                            buffer = buffer * 0.3048;
                            break;
                        case 'mi':
                            buffer = buffer * 1609.34;
                            break;
                        case 'km':
                            buffer = buffer * 1000;
                            break;
                        case 'm':
                        default:
                            break;
                    }
                }
                data['buffer'] = buffer;
                //data['field'] = 'gid';
            }

            if (!this.first) {
                if (this.andor_select.value.substr(-3) === "not") {
                    data.is_not = 1;
                    data.andor = this.andor_select.value.substr(0, -3);
                } else {
                    data.andor = this.andor_select.value;
                }
            }
            return data;
        }
    });
});