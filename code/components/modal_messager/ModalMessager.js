define(["dojo/_base/declare", "dojo/on", "dojo/dom-attr",
    "dojo/dom-construct", "dojo/query!css3", "dijit/_WidgetBase",
    "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin",
    'dojo/dom-class', "sl_classes_open/InterruptableTimer",
    "sl_modules_open/ParamUtil",
    "dojo/text!./ModalMessager.tpl.html"], function (declare, on, domAttr,
        domCon, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, domClass, InteruptableTimer, ParamUtil, template) {
    return declare('modal_messager/ModalMessager', [_WidgetBase, _TemplatedMixin,
        _WidgetsInTemplateMixin], {
        templateString: template,
        currentState: null,
        isOn: null,
        baseClass: 'ModalMessager',
        modalHTML: null,
        templateString: template,
        _opened:false,
        constructor: function () {

        },
        postCreate: function () {
            document.body.appendChild(this.domNode);
            on(this.modal_message, 'click', this.ShowModal.bind(this));
        },
        Open:function() {
            if(this._opened) return;
            this._opened = true;
            this.modal_message.click();
        },
        Notify: function (message, opt_title, opt_time) {
            // Button labled "Close" at bottom of the modal area
            if (ParamUtil.IsNot(opt_title))
                domClass.toggle(this.modalTitle, 'hidden', true);
            else
                this.modalTitle.innerHTML = opt_title;

            domClass.toggle(this.buttonOne, 'hidden', false);
            domClass.toggle(this.barData, 'hidden', true);
            this.buttonOne.value = "Close";
            this.message.innerHTML = message;
            var timeDelay = 2500;
            if (opt_time != null)
                timeDelay = opt_time * 100;
            if ((message.length) / 0.015 > 2500)
                timeDelay = (message.length) / 0.015;
            var timer = new InteruptableTimer();
            timer.Start(this.HideModal.bind(this), timeDelay);
            this.Open();
        },
        ShowProgress: function (message, isCancelable, opt_percent, opt_title) {
            if (ParamUtil.IsNot(opt_title))
                domClass.toggle(this.modalTitle, 'hidden', true);
            else
                this.modalTitle.innerHTML = opt_title;

            if (ParamUtil.Is(isCancelable)) {
                this.buttonOne.innerHTML = 'Cancel';
                domClass.toggle(this.buttonOne, 'hidden', true);
                domClass.toggle(this.buttonTwo, 'hidden', false);
                
            } else {
                domClass.toggle(this.buttonOne, 'hidden', true);
                domClass.toggle(this.buttonTwo, 'hidden', true);
            }
            this.message.innerHTML = message;

            if (ParamUtil.Is(opt_percent) && opt_percent >= 0 && opt_percent <= 1.0)
            {
                var percent = Math.floor(opt_percent * 100.0);
                var newProgress = "w-" + percent;
                domClass.toggle(this.barData, 'hidden', false);
                domClass.toggle(this.progressBar, newProgress, true);
                this.progressBar.style = "width: " + percent + "%";
                this.progressBar.innerHTML = percent + "%";
                if (opt_percent == 1.0)
                {
                    var timeDelay = 3000;
                    if ((message.length) / 0.015 > 2500)
                        timeDelay = (message.length) / 0.015;
                    var timer = new InteruptableTimer();
                    timer.Start(this.HideModal.bind(this), timeDelay);
                }
            }
            this.Open();

        },
        Confirm: function (message, okHandler, cancelHandler, opt_title) {
            // Ok button
            // Cancel button
            if (ParamUtil.IsNot(opt_title))
                domClass.toggle(this.modalTitle, 'hidden', true);
            else
                this.modalTitle.innerHTML = opt_title;
            domClass.toggle(this.buttonOne, 'hidden', false);
            domClass.toggle(this.buttonTwo, 'hidden', false);
            domClass.toggle(this.barData, 'hidden', true);
            this.buttonOne.innerHTML = "Ok";
            this.buttonTwo.innerHTML = "Cancel";
            if (ParamUtil.Is(okHandler))
                on(this.buttonOne, 'click', okHandler);
            if (ParamUtil.Is(cancelHandler))
                on(this.buttonTwo, 'click', cancelHandler);
            on(this.buttonOne, 'click', this.HideModal());
            on(this.buttonTwo, 'click', this.HideModal());
            this.message.innerHTML = message;
            this.Open();
        },
        ShowModal: function () {
            //    var access = $("#myModal");
            //              access.modal('show');
            //    domClass.toggle(this.modalMessage, 'hidden', false);
        },
        HideModal: function () {
            this._opened = false;
            $("#myModal").modal('hide');
            $('.modal-backdrop').remove();
            //      domClass.toggle(this.modalMessage, 'hidden', true);
        }
    }
    )
});
