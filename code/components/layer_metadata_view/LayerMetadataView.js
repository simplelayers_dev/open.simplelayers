define(["dojo/_base/declare", "dojo/on", "dojo/dom-attr", "dojo/dom-class",
    "dijit/_WidgetBase", "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin", 'dojo/dom', 'dojo/dom-construct',
    'dojo/query', 'sl_modules_open/WAPI', 'sl_modules_open/sl_URL',
    "dojo/text!./ui.tpl.html"],
        function (declare, dojoOn, domAttr, domClass, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, dom, domCon, query, wapi, sl_url,
                template) {
            return declare('sl_components/layer_metadata_view/LayerMetadataView', [
                _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                // Some default values for our author
                // These typically map to whatever you're passing to the constructor
                baseClass: 'LayerMetadataView',
                templateString: template,
                _itemTemplate: null,
                _layerId: null,
                _created: false,
                _setup: false,
                constructor: function (args) {
                    if (args.hasOwnProperty('layer')) {
                        this._layerId = args.layer;
                    }
                },
                SetLayer: function (layerId) {
                    this._layerId = layerId;
                    if (this._created === false)
                        return;
                    if( this._setup === false ) {
                        var templates = this.item_tpl;
                        this._itemTemplate = templates.innerHTML;
                        domCon.destroy(templates);
                    }
                    wapi.exec('layers/layer/action:get_metadata/', {
                        layer: this._layerId
                    }, this.HandleLoadedMetadata.bind(this));
                    this._setup = true;
                },
                postCreate: function () {
                    this._created = true;
                    if (this._layerId !== null) {
                        if (!this._setup) {
                            this.SetLayer(this._layerId);
                        }
                    }

                },
                HandleLoadedMetadata: function (info) {
                    this._metadata = info.response.results;
                    if (this._metadata.hasOwnProperty('metadata')) {
                        this._metadata = this._metadata.metadata;
                    }
                    if (this._metadata.hasOwnProperty('SimpleLayers')) {
                        if (this._metadata.SimpleLayers.hasOwnProperty('Name')) {
                            this.layerName_head.innerHTML = this._metadata.SimpleLayers.Name;
                        }
                    }

                    domCon.empty(this.metadata_ul);
                    this.DisplayMetadata(this._metadata, 0);
                },
                DisplayMetadata: function (metadata, level, inline, includeSpacers,
                        prefix, index) {

                    if (typeof (includeSpacers) == 'undefined')
                        includeSpacers = inline !== true;
                    if (typeof (prefix) == 'undefined')
                        prefix = '';
                    if (typeof (index) == 'undefined')
                        index = null;

                    for (var m in metadata) {
                        var tpl = this._itemTemplate.substr(0);
                        var i = 0;
                        if (m == '_attributes') {
                            this.DisplayMetadata(metadata[m], level + 1, false, true, '@');
                            continue;
                        }
                        tpl = tpl.replace(/\{opt_inline\}/g,
                                (inline === true) ? 'inline_spacer' : '');

                        var spacers = "";
                        if (includeSpacers === true) {
                            for (i = 0; i < level; i++) {
                                spacers += "<span class='spacer'></span>";
                            }
                        }
                        tpl = tpl.replace(/\{spacers\}/g, spacers);
                        
                        
                        if (metadata[m] === null)
                            continue;
                        if (metadata[m].constructor === String) {
                            if (m.toLowerCase() == "value") {
                                tpl = tpl.replace(/\{value\}/g, metadata[m]);
                                tpl = tpl.replace(/\{state\}/g, 'metaitem-val');
                            } else {
                                tpl = tpl.replace(/\{label\}/g, prefix + m);
                                tpl = tpl.replace(/\{value\}/g, metadata[m]);
                                tpl = tpl.replace(/\{state\}/g, 'metaitem-all');
                            }
                        } else {
                            tpl = tpl.replace(/\{label\}/g, prefix + m);
                            tpl = tpl.replace(/\{state\}/g, 'metaitem-lbl');
                        }

                        var node = domCon.toDom(tpl);
                        domCon.place(node, this.metadata_ul);
                        if (index !== null) {
                            continue;
                        }
                        if (metadata[m].constructor === Array) {
                            for (var mi = 0, ml = metadata[m].length; mi < ml; mi++) {
                                // this.DisplayMetadata(metadata[m],level
                                // +1,false,true,m+'['+mi+']<span class="spacer"/>','',mi);
                                var title = m + ' [' + mi + ']';
                                var titleEntry = {};
                                titleEntry[title] = "";
                                this.DisplayMetadata(titleEntry, level + 1);
                                this.DisplayMetadata(metadata[m][mi], level + 2, false, true);
                            }
                        } else if (metadata[m].constructor === Object) {
                            this.DisplayMetadata(metadata[m], level + 1, false, true, '',
                                    null);
                        }
                    }

                }
            });
        });