define(['dojo/_base/declare', "dojo/on", "dojo/dom-construct", "dojo/dom-class",
    "dojo/dom-attr", "dijit/_WidgetBase", "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin", "dojo/dom", "dojo/text!./RecordCounter.tpl.html"],
        function (declare, on, domCon, domClass, domAttr, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, dom, template) {
            return declare('RecordCounter', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                templateString: template,
                value: 25,
                parent: null,
                baseClass: 'record_counter',
                constructor: function () {
                    
                },
                postCreate: function () {
                    on(this.records_per_page, "change", this.HandleChange.bind(this));
                },
                SetParent: function (parent) {
                    this.parent = parent;
                },
                SetValue: function (val) {
                    if([null,undefined,-1].indexOf(val)>=0) return;
                    this.value = val;
                    this.records_per_page.value = val;
                },
                HandleChange: function () {
                    this.value = parseInt(this.records_per_page.value);
                    if(this.parent != null) {
                        this.parent.UpdateRecords();
                    }
                }
            });
        });
