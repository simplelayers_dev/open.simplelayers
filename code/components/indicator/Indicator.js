define(["dojo/_base/declare", "dojo/dom-class", "dijit/_WidgetBase", "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin", "sl_classes_open/StateManager",
    "dojo/text!./Indicator.tpl.html"], function (declare, domClass, _WidgetBase,
        _TemplatedMixin, _WidgetsInTemplateMixin, StateManager, template) {
    return declare('Indicator', [StateManager, _WidgetBase,
        _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        baseClass: "indicator flex-item flex-container col justify-center align-items-center req-indicator-on req-indicator-label_only notVisible",
        calcs: null,
        metric: null,

        postCreate: function () {
            this.SetMachine(this.domNode);
            this.SetStates(['on', 'off', 'label_only']);
            this.SetPrefix('indicator');
            this.SetState('off');
        },
        SetLabel: function (label, includeIndicator) {
            if (typeof label !== 'undefined') {
                this.indicator_lbl.innerHTML = label;
            }
            if (typeof includeIndicator === undefined)
                includeIndicator = true;
            domClass.toggle(this.indicator, 'notVisible', !includeIndicator);
            //if(!includeIndicator) this.SetState('label_only');
        },
        Show: function (label) {
            this.SetLabel(label);
            domClass.toggle(this.indicator, 'notVisible', false);
            this.SetState('on');
        },
        Hide: function () {
            this.SetState('off');
        }

    });
});