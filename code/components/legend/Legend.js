define(["dojo/_base/declare", "dojo/dom-class", "dojo/dom-attr", "dojo/dom-construct","dojo/_base/lang", "dijit/_WidgetBase", "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin", "sl_classes_open/StateManager",
    "dojo/text!./Legend.tpl.html"], function (declare, domClass, domAttr, domCon, lang,
        _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, StateManager,
        template) {
    return declare('sl_components_open/legend/Legend', [StateManager, _WidgetBase,
        _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        baseClass: "Legend",
        calcs: null,
        metric: null,

        postCreate: function () {
            console.log('containerNode',this.containerNode);
            this.SetMachine(this.domNode);
            //this.SetStates(['on', 'off']);
            //this.SetPrefix('widget');
            //this.SetState('on');            
        },
        ShowLayerLegend:function(layerInfo) {
            if(!layerInfo.hasOwnProperty('legend')) {
                return false;
            }
            if(!layerInfo.legend.hasOwnProperty('classes')) {
                return false;
            }
            domCon.empty(this.listing);
            var thisId = domAttr.get(this.domNode,'widgetid');
            for(var i in layerInfo.legend.classes) {
                var cls = layerInfo.legend.classes[i];
                console.log(cls);
                var item = "<tr><td>";
                var iconId = thisId+'_icon_'+i;
                item+= "<img id=\""+iconId+"\"class=\"legend-icon\" src=\""+cls.icon+"\" title=\""+cls.class_name+"\"></img></td>";
                item+="<td><label for=\""+iconId+"\" title=\""+cls.class_name+"\" class=\"legend-label\">"+cls.class_name+"<label></td></tr>";
                console.log(item);
                var itemNode = domCon.toDom(item);
                domCon.place(itemNode,this.listing);
            }
        }
    });
});