define(["dojo/_base/declare",
    "dojo/on",
    "dojo/query",
    "dojo/topic",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    'dojo/dom-class',
    'dojo/dom-style',
    "dojo/text!./Pane.tpl.html"
],
        function (declare,
                on,
                query,
                topic,
                domAttr,
                domCon,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                domClass,
                domStyle,
                template) {
            return declare('sl_components_open/pane/Pane', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                baseClass: 'open-sl-Pane',
                templateString: template,
                _subscriptions: null,
                _subcontentsReady: null,
                postCreate: function () {
                    this._subcontentsReady = false;
                    this._subscriptions = [];
                    this._subscriptions.push(topic.subscribe('pane_content_updated', this.ChangeListener.bind(this)));
                },
                ChangeListener: function (event) {
                    if (!this._subcontentsReady) {
                        this._subcontentsReady = true;
                        for (var i = 0, l = this.containerNode.children.length; i < l; i++) {
                            var child = this.containerNode.children[l - i - 1];
                            var targets = ['headerBar', 'paneBody', 'footerBar'];

                            for (var t = 0; t < targets.length; t++) {
                                var target = targets[t];
                                var targetElements = query('div[data-pane-target="' + target + '"]', this.domNode);
                                for (var te = 0; te < targetElements.length; te++) {
                                    var targetElement = targetElements[te];
                                    domCon.place(targetElement, this[target]);
                                }
                            }

                        }
                        on(window, 'resize', this.HandleWindowResize.bind(this));
                        this.HandleWindowResize();
                    }
                    for (var child in this.paneBody.children) {
                        if (this.paneBody.children[child].innerHTML === event.container.innerHTML) {
                            this.HandleWindowResize();
                        }
                    }
                },
                HandleWindowResize: function () {
                    setTimeout(function () {
                        var headerHeight = this.headerBar.offsetHeight;
                        var footerHeight = this.footerBar.offsetHeight;
                        this.paneBody.style.height = (this.domNode.offsetHeight - headerHeight - footerHeight) + 'px';
                    }.bind(this),100);
                },
                HideFooter: function () {
                    domClass.toggle(this.footerBar, 'hidden', true);
                    domClass.toggle(this.domNode, 'with-footer', false);
                },
                ShowFooter: function () {
                    domClass.toggle(this.footerBar, 'hidden', false);
                    domClass.toggle(this.domNode, 'with-footer', true);
                },
                HideHeader: function () {
                    domClass.toggle(this.headerBar, 'hidden', true);
                    domClass.toggle(this.domNode, 'with-header', false);
                },
                ShowHeader: function () {
                    domClass.toggle(this.headerBar, 'hidden', false);
                    domClass.toggle(this.domNode, 'with-header', true);
                },
                destroy: function () {
                    for (var i = 0; i < this._subscriptions.length; i++) {
                        this._subscriptions[i].remove();
                    }
                },
                Toggle: function (isHidden) {
                    if (isHidden !== undefined) {
                        domClass.toggle(this.domNode, 'hidden', isHidden);
                        return this.IsHidden();
                    }
                    domClass.toggle(this.domNode, 'hidden', isHidden);
                    this.HandleWindowResize();
                    return this.IsHidden();
                    
                },
                IsHidden: function () {
                    return domClass.contains(this.domNode, 'hidden');
                }

            }

            );
        });
