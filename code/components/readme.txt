The components folder should be where you put your components

Each component should have:
* a folder named for the component
* a subfolder named templates where all UI templates are stored each with an extension of .tpl.html
* a file named widget.js which is the main controller for the widget.
* a css folder with css file(s) specific to the widget.
* additional js files required for the component*

For component named SomeWidgetComponent

./some_widget_component/
./some_widget_component/templates/some_widget_component.tpl.html
./some_widget_compoment/css/some_widget_component.css
./some_widget_component/widget.js
