define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-attr",
		 "dojo/dom-class",
		 "dojo/dom-style",
		 "dojox/widget/ColorPicker",
		 "dijit/ColorPalette",
		 "dijit/form/DropDownButton",
		 "dojo/fx/Toggler",
		 "dijit/Dialog",
		 "dojo/dom-construct",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dojo/text!./templates/dialog.tpl.html"],
		function (declare,
				  on,
				  domAttr,
				  domClass,
				  domStyle,
				  Picker,
				  Palette,
				  DropDownButton,
				  Toggler,
				  Dialog,
				  domCon,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
				  template) {
		    return declare('dialog/dialog', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    templateString: template,
                baseClass:'colorPicker_dialog',
				displayText: "",
				count: null,
				currentState: "standard",
				myDialog: null,
				boolToggle: null,
			    startup: function () {
					boolToggle = false;
					this.inherited(arguments);
					this.ToggleState(this.currentState);
					
					on(this.stateButton, "click", this.StateButtonHandler.bind(this));
					on(this.okButton, "click", this.OkButtonHandler.bind(this));
					// this.toggler.hide();
					// domClass.add(this.toggler, "class", "hidden");
					 // myDialog = new Dialog({
						// title: "My Dialog",
						// content: "Test content.",
						// style: "width: 300px"
					// });
					//myDialog.show();
					on(this.textCode,"keyup",this.TextColorChange.bind(this));
					on(this.colorPicker,"change",this.ColorChange.bind(this));
					on(this.colorPalette,"change",this.ColorChange.bind(this));
					
			    },
				SetValue: function(color){
					this.colorPicker.set("value",color);
					this.textCode.value = color;
				},
				ColorChange: function(){
					this.textCode.value = this.GetHexcode();
				},
				TextColorChange: function(){
					if(this.textCode.value.length == 7)
						this.colorPicker.set("value",this.textCode.value);
				},
				StateButtonHandler: function (){
					this.ToggleState();
				},
				ToggleState: function(optState)
				{
					if (this.currentState == "standard"){
						this.currentState = "advanced";
					}
					else {
						this.currentState = "standard";
					}
					
					if (optState != null){
						this.currentState = optState;
					}
					var temp;
					switch (this.currentState){
						case "standard": 
							//this.temp = this.Color_Picker.getValue();
							domClass.add(this.colorPicker.domNode, "hidden");
							domClass.remove(this.colorPalette.domNode, "hidden");
							this.stateButton.value = "Advanced";
							//this.text_code.value = this.getHexcode();
							// domClass.remove(this.button.domNode, "dijitDialogPaneActionBar");
							// domClass.add(this.button.domNode, "dijitDialogPaneActionBar2");
							break;
						case "advanced":
							this.temp = this.colorPalette.value;
							domClass.remove(this.colorPicker.domNode, "hidden");
							domClass.add(this.colorPalette.domNode, "hidden");
							this.colorPicker.set("value",this.temp);
							this.stateButton.value = "Standard";
							//this.text_code.value = this.getHexcode();
							// domClass.remove(this.button.domNode, "dijitDialogPaneActionBar2");
							// domClass.add(this.button.domNode, "dijitDialogPaneActionBar");
							break;
					}
				},
				GetHexcode: function(){
					var color;
					switch (this.currentState){
						case "standard":
							color = this.colorPalette.value;
							break;
						case "advanced":
							color = this.colorPicker.getValue();
							break;
					}
					return color;
				},
				OkButtonHandler: function(){
					this.NotifyListeners();
				},
				NotifyListeners: function(event){
					this.textCode.value = this.GetHexcode();
					on.emit(this.domNode, 'color_selected', { bubbles: true, cancelable: true, color: this.GetHexcode() });
				}
			});
		}
	);
