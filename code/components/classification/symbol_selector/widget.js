define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-construct",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
         'sl_modules_open/WAPI',
		 "dojo/text!./templates/ui.tpl.html"],
		function (declare,
				  on,
				  domCon,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
                  wapi,
				  template) {
		    return declare('sample/sample_widget2', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    data: null,
			    baseClass:'symbol_selector',
			    templateString: template,
			    postCreate: function () {
			        wapi.exec('classification/fonts/',{'action':'list'},this.FontsLoaded.bind(this));			        
			        // this would also work:
			        //wapi.exec('classification/fonts/action:list',{},this.FontsLoaded.bind(this));
			    },
			    FontsLoaded:function(event) {
			    	console.log(event.results);
			    }
			    
			});
		}
	);
