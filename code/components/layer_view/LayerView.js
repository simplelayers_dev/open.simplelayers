/* global catalog */

define(['sl_components_open/layer_list/layer_item/LayerItem',
    "dojo/topic",
    'dstore/Memory',
    'dgrid/Selection',
    'dstore/Trackable',
    'dgrid/OnDemandList',
    'dojo/_base/declare',
    'dojo/dom-class',
    'dgrid/extensions/DijitRegistry',
    "dojo/on",
    "sl_modules_open/layerImage",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_modules_open/sl_api/Layers",
    "sl_components_open/layer_preview/LayerPreview",
    "sl_components_open/pane/Pane",
    "dojo/text!./LayerView.tpl.html"],
        function (LayerItem, topic, Memory, Selection, Trackable, List, declare, domClass, Registry, on, LayerImage,
                _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Layers, LayerPreview, Pane, template) {
            return declare('sl_components_open/layer_view/LayerView', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                list: null,
                data: null,
                searchKeys: ["tags", "name", "description", "geom"],
                TrackableMemory: null,
                layerItemList: [],
                visibleList: [],
                templateString: template,
                baseClass: 'layer_view flex-item flex-container',
                _isFullView: null,
                topicHandlers: null,
                postCreate: function () {
                    Layers.GetSharedView(catalog, this.ListLayers.bind(this));
                    on(this.toggle_btn, 'click', this.HandleToggle.bind(this));
                    topic.subscribe('layer_peview/full-view/changed', this.HandleToggle.bind(this));
                    on(window, 'resize', this.HandleResize.bind(this));
                    this.HandleResize();
                    this.topicHandlers = [];

                    this.topicHandlers.push(topic.subscribe('layeritem/memory/recall', this.HandleMemoryRecall.bind(this)))
                },
                destroy: function () {
                    for (var i in this.topicHandlers) {
                        topic.remove(this.topicHandlers[i]);
                    }
                },
                HandleMemoryRecall: function (event) {
                    for (var i = 0; i < this.layerItemList.length; i++) {
                        if (this.layerItemList[i].id === event.layerId) {
                            this.layerPreview.SetPreview(this.layerItemList[i], 'features');
                            break;
                        }
                    }
                },
                HandleResize: function () {

                    if (['xs', 'sm'].indexOf(WINDOW_SZ) > -1) {
                        if (this.layerPane.IsHidden() === false) {
                            this.workspacePane.Toggle(true);
                        } else {
                            this.workspacePane.Toggle(false);
                        }
                    } else {
                        this.layerPane.Toggle(false);
                        this.workspacePane.Toggle(false);
                    }
                },
                HandleToggle: function (event) {
                    if (event.hasOwnProperty('preventDefault')) {
                        event.preventDefault();
                    }

                    this.Toggle();
                },
                Toggle: function () {
                    var isHidden = this.layerPane.Toggle();
                    domClass.toggle(this.workspacePane.domNode, 'full-width', isHidden);

                    if (['xs', 'sm'].indexOf(WINDOW_SZ) < 0) {
                        this.workspacePane.Toggle(false);
                    } else {
                        this.workspacePane.Toggle(!isHidden && (['xs', 'sm'].indexOf(WINDOW_SZ) > -1));
                    }
                },
                ListLayers: function (jsonData) {
                    Layers.GetAggData(catalog, this.AggDataLoaded.bind(this));
                    var data = [{id: "", name: "All Layers", type: '*', type_label: "", 'tags': '', 'description': ""}];
                    this.data = data.concat(jsonData['results']);
                    this.TrackableMemory = declare([Memory, Trackable]);
                    var memory = new this.TrackableMemory({data: this.data});
                    MyList = declare([List, Registry, Selection]);

                    this.list = new MyList({
                        collection: memory,
                        renderRow: this.RenderRow.bind(this),
                        selectionMode: 'single'
                    }, 'list').placeAt(this.layers_attach_point);

                    this.list.on('dgrid-select', this.SelectHandler.bind(this));
                    this.list.startup();

                    on(this.filter, 'keyup', this.FilterLayers.bind(this));
                    Layers.SetLayerList(this.data);
                    this.Refresh();
                    
                    
                },
                AggDataLoaded: function (event) {
                    this.data[0].data = event.response.results[0];
                    console.log(this.data[0]);

                },
                FilterLayers: function (event) {
                    var s = this.filter.value.toLowerCase();
                    var searchTags = s.charAt(0) === '#' ? true : false;
                    if (searchTags) {
                        s = s.substr(1, s.length);
                    }
                    this.visibleList = [];

                    for (var i = 0, l = this.data.length; i < l; i++) {
                        var keep = false;
                        for (var a in this.searchKeys) {
                            var obj = this.data[i][this.searchKeys[a]];
                            if (a == 3) {
                                obj = LayerImage.getIconClass(this.data[i]["type"], this.data[i]["geom"]);
                            }
                            if ((obj + "").toLowerCase().indexOf(s) >= 0) {
                                keep = true;
                                break;
                            }
                            if (searchTags) {
                                break;
                            }
                        }
                        if (!keep) {
                            domClass.add(this.layerItemList[i].domNode, 'hidden');
                        } else {
                            domClass.remove(this.layerItemList[i].domNode, 'hidden');
                            this.visibleList.push(this.layerItemList[i].domNode);
                        }
                    }

                    this.Refresh();
                },
                Refresh: function () {
                    for (var i = 0; i < this.visibleList.length; i++) {
                        if (i % 2 == 0) {
                            domClass.replace(this.visibleList[i], 'color-secondary2', 'color-secondary1');
                        } else {
                            domClass.replace(this.visibleList[i], 'color-secondary1', 'color-secondary2');
                        }
                    }
                    topic.publish('pane_content_updated', {target: this, container: this.layers_attach_point});
                },
                RenderRow: function (data, options) {
                    var item = new LayerItem(data, this);
                    this.visibleList.push(item.domNode);
                    this.layerItemList.push(item);
                    return item.domNode;
                },
                SelectHandler: function (event) {
                    this.list.clearSelection();

                    if (['xs', 'sm'].indexOf(WINDOW_SZ) > -1) {
                        this.Toggle();
                    }
                    this.layerPreview.SetPreview(this.layerItemList[event.rows[0].element.rowIndex]);
                }
            });
        });
