define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/io-query",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "sl_components_open/view_manager/ViewManager",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/layer_view/LayerView",
    "dojo/text!./SLCatalog.tpl.html"],
        function (declare, on, domCon, ioquery, domAttr, _WidgetBase, _TemplatedMixin, ViewManager, _WidgetsInTemplateMixin, LayerView, template) {
            return declare('sl_components_open/sl_catalog/SLCatalog', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                templateString: template,
                baseClass: 'sl_catalog flex-container col',
                constructor: function () {
                    var url = document.location.href;
                    var hasQuery = url.indexOf('?') > -1;
                    catalog = 1043; //default catalog
                    if (hasQuery) {
                        var info = ioquery.queryToObject(url.substring(url.indexOf("?") + 1, url.length));
                        if (info.hasOwnProperty('catalog')) {
                            catalog = info.catalog;
                        }
                    }
                },
                postCreate: function () {
                    domAttr.set(this.options, "id", this.id + "_view_selctor");
                    domAttr.set(this.label, "for", this.id + "_view_selctor");

                    this.viewManager.StartUp();

                    for (var view in this.viewManager.views) {
                        var a = domCon.place('<option title="' + view + '" value=' + view + '>' + this.GetName(view) + '</option>', this.options);
                    }
                    on(this.options, 'change', this.HandleActionButton.bind(this));
                },
                GetName: function (name) {
                    name = name.replace("_", " ");
                    return name.toLowerCase().replace(/\b(\w)/g, s => s.toUpperCase());
                },
                HandleActionButton: function (event) {
                    this.viewManager.GoToView(domAttr.get(event.target, "value"));
                }
            });
        });
