define(["dojo/_base/declare", "dojo/dom-class", "dojo/dom-attr", "dojo/dom-construct","dojo/_base/lang", "dijit/_WidgetBase", "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin", "sl_classes_open/StateManager",
    "dojo/text!./WSWidget.tpl.html"], function (declare, domClass, domAttr, domCon, lang,
        _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, StateManager,
        template) {
    return declare('sl_components_open/ws_widget/WSWidget', [StateManager, _WidgetBase,
        _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        baseClass: "WSWidget",
        calcs: null,
        metric: null,

        postCreate: function () {
            console.log('containerNode',this.containerNode);
            this.SetMachine(this.domNode);
            this.SetStates(['on', 'off']);
            this.SetPrefix('widget');
            this.SetState('on');
            var heading = domAttr.get(this.domNode, 'data-heading');
            if ([undefined, null].indexOf(heading) > -1) {
                heading = "";
            }
            var contentClass = domAttr.get(this.domNode,'data-content-class');
            if(contentClass == "none") {
                domClass.toggle(this.containerNode,'content',false);                
            } else if(contentClass === null) {
                domClass.toggle(this.containerNode,'content',true);
            } else {
                var classes = this.contentClass.split(' ');
                for(var c in classes) { 
                    var clss = classes[c];
                    domClass.toggle(this.containerNode,clss,true);
                }                
            }
            
            this.heading.innerHTML = heading;
            //domCon.place(this.containerNode,this.domNode);
            for( var i in this.containerNode.childNodes) {
                var node = this.containerNode.childNodes[i];
                var target = domAttr.get(node,'data-target');
                if(target === "heading-right") {
                  domCon.place(node,this.heading_right);
                   break;
                }
            }
        },

        Show: function () {
             this.SetState('on');             
        },
        Hide: function () {
            this.SetState('off');            
        },
        Toggle:function(onOrOff) {
            if([undefined,null].indexOf(state) === undefined) {
                var state =this.GetCurrentState();
                if(state == 'on' ) onOrOff = fakse;
                if(state == 'off') onOrOff = true;
            }
            if(onOrOff) {
                this.Show();
            } else {
                this.Hide();
            }
            
            
        }        
       

    });
});