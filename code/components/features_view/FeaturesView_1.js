define(['dojo/_base/declare', "dojo/on","dojo/query", "dojo/dom-construct", "dojo/dom-class", "dojo/dom-style", "dojo/dom-attr", "sl_modules_open/sl_api/Layers", "dojo/topic",
    'dgrid/Selection', 'dstore/Memory', 'dstore/Trackable', 'dgrid/OnDemandGrid', 'dgrid/extensions/DijitRegistry', 'dgrid/extensions/ColumnResizer',
    "sl_components_open/record_counter/RecordCounter",
    "sl_components_open/search_criteria/SearchCriteria",
    "sl_classes_open/GeoCalculatorMemory",
    "sl_classes_open/StateManager",
    "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "dojo/dom", "dojo/text!./FeaturesView.tpl.html"],
        function (declare, on, query,domCon, domClass, domStyle, domAttr, Layers, topic, Selection, Memory,
                Trackable, Grid, Registry, ColumnResizer, RecordCounter, SearchCriteria, GeoCalculatorMemory, StateManager, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, dom, template) {
            return declare('FeaturesView', [StateManager, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                loadedItem: null,
                layerItem: null,
                parent: null,
                currStart: 0,
                currSort: null,
                count: 0,
                filters: [],
                data: null,
                gridObj: null,
                searchableKeys: [],
                templateString: template,
                baseClass: 'features_view',
                _layerId: null,
                _memory: null,
                _bbox: null,
                topicHandlers: null,
                constructor: function () {
                    MyGrid = declare([Grid, Registry, Selection, ColumnResizer]);
                    this._memory = new GeoCalculatorMemory('features_memory');
                    this.topicHandlers = [];
                },
                postCreate: function () {
                    this.SetMachine(this.domNode);
                    this.SetPrefix('mode');
                    this.AddState('layer');
                    this.AddState('all-layer');
                    this.SetToggleStates('layer','all-layer');
                    this.SetState('layer');
                    on(this.left, 'click', this.PageLeft.bind(this));
                    on(this.right, 'click', this.PageRight.bind(this));
                    on(this.grid, 'dgrid-select', this.SelectHandler.bind(this));
                    on(this.search_button, 'click', this.SearchButtonHandler.bind(this));
                    on(this.searchToggle, 'click', this.ToggleAdvSearch.bind(this));
                    on(this.grid, 'dgrid-sort', this.HandleSort.bind(this));
                    on(this.record_start, 'change', this.GoToRecord.bind(this));
                    this.topicHandlers.push(topic.subscribe('search_criteria/resize', this.SearchCriteriaResized.bind(this)));
                    this.topicHandlers.push(topic.subscribe('features/multisearch/requested', this.MultiCriteriaSearch.bind(this)));
                    //this.SearchCriteriaResized({target: this.searchCriteria});
                    this.recordCounter.parent = this;
                    Layers.SetMemory(this._memory);

                },

                destroy: function () {
                    for (var i in this.topicHandlers) {
                        this.topicHandlers[i].remove();
                    }
                    return this.inherited(arguments);
                },
                SearchCriteriaResized: function (event) {
                    if (event.target !== this.searchCriteria)
                        return;

                    var height = "Calc(100% - " + this.searchCriteria.domNode.offsetHeight + "px)"
                    if (domClass.contains(this.searchCriteria.domNode, 'hidden'))
                        height = "Calc(100%)";
                    this.gridContainer.style.height = height;

                },
                Init: function (loadedItem, layerItem, parent, autoSearch) {
                    if (autoSearch !== false) {
                        autoSearch = true;
                    }
                    this.parent = parent;

                    if (autoSearch) {
                        this.Busy("Loading");
                        this.record_text.innerHTML = "Loading...";
                    }
                    this.loadedItem = loadedItem;
                    this.layerItem = layerItem;

                    this.currStart = 0;
                    this.recordCounter.SetValue(25);

                    this.filters = [];
                    this.searchableKeys = [];
                    if (layerItem.data.type === "*") {
                        this.ToggleState(true);
                        this.searchableKeys = loadedItem.field_names;
                    } else {
                        this.ToggleState(false);
                    }
                    this.currSort = null;

                    //this.searchCriteria.Setup(this.searchableKeys);
                    domCon.empty(this.grid);

                    //this.SearchDialog.Refresh();
                    this._layerId = this.layerItem.id;
                    this.searchCriteria.ResetImages();

                    
                    if (autoSearch) {
                        this.searchCriteria.Setup(this.searchableKeys, this.loadedItem['searchtip'], this._layerId);
                        Layers.SearchFeatures({layer: this.layerItem.id, first: this.currStart, limit: this.recordCounter.value}, this.HandleFeatures.bind(this));
                    } else {
                        this._layerId = "*";
                       
                        this.searchCriteria.Setup(this.searchableKeys, this.loadedItem['searchtip'], this._layerId);
                        var me = this;
                        setTimeout(function () {
                            me.searchCriteria.ZoomToBbox(me.loadedItem.bbox);
                        }, 50);

                    }
                    
                        var nodes = query('.record-counter-ui',this.domNode);
                        for( var n=0;n< nodes.length;n++) {
                            var node = nodes[n];
                            domClass.toggle(node,'hidden',this._layerId === '*');
                            
                        }
                            
                    
                    this.SearchCriteriaResized({target: this.searchCriteria});
                },
                GoToRecord: function (event) {
                    if (/^[0-9]+$/.test(this.record_start.value) && parseInt(this.record_start.value) > 0 && parseInt(this.record_start.value) < this.count) {
                        domClass.remove(this.record_start, "invalid");
                        this.Busy('loading');
                        this.currStart = parseInt(this.record_start.value) - 1;
                        Layers.SearchFeatures({layer: this.layerItem.id, sort: this.currSort, first: this.currStart, limit: this.recordCounter.value, count: this.count, filters: this.filters}, this.RefreshGrid.bind(this));
                    } else {
                        //wrong input
                        domClass.add(this.record_start, "invalid");
                    }
                },
                SearchButtonHandler: function (event) {
                    this._bbox = null;
                    this._bbox = this.searchCriteria.ResetMapLimit();
                    //var criteria = this.searchCriteria.GetCriteria();

                    this.MultiCriteriaSearch(this.recordCounter.value);
                    //this.SearchDialog.Show('Loading');
                },
                ToggleAdvSearch: function (event) {
                    domClass.toggle(this.advSearch, 'hidden');
                    this.SearchCriteriaResized({target: this.searchCriteria});
                },
                HandleFeatures: function (resultset) {
                    if (resultset.hasOwnProperty('status')) {
                        if (resultset.status !== 'ok') {
                            this.searchCriteria.ResetImages();
                            this.parent.progress_indicator.Hide();
                            return;
                        }
                    }
                    this.count = resultset.paging.count;
                    this.data = resultset.resultset.results;
                    var ids = [];
                    for (i = 0, l = this.data.length; i < l; i++) {
                        ids.push(this.data[i].gid);
                    }
                    var unids = resultset.hasOwnProperty('unpaged_ids') ? resultset.unpaged_ids : null;
                    this.searchCriteria.SetResultInfo(ids, unids, resultset.layer_bbox, resultset.paged_bbox, resultset.unpaged_bbox);

                    domCon.empty(this.grid);
                    this.UpdateGrid(resultset);
//                    /this.searchResults
                    this.searchCriteria.UpdateLayerImages();
                },
                RenderCells: function (object, value, node, options) {
                    domCon.create('b', {'innerHTML': value, 'title': value}, node);
                },
                RenderActionsCell: function (object, value, node, options) {

                    //var cb = domCon.create('input',{type:'checkbox','class':'form-check-input',value:value,name:value},node);
                    //on(cb,'click',this.HandleItemCB.bind(this));
                    var z2f = domCon.create('button', {type: 'button', class: 'btn btn-sm btn-primary', 'name': value}, node);
                    domCon.create('span', {'class': 'glyphicon glyphicon-record'}, z2f);
                    on(z2f, 'click', this.HandleZoomToFeature.bind(this));

                    var offset = this.currStart + this._rowCtr;

                    var ibtn = domCon.create('button', {type: 'button', class: 'btn btn-sm btn-primary', 'name': 'info', 'data-value': value, 'data-index': offset}, node)
                    domCon.create('span', {'class': 'glyphicon glyphicon-chevron-right'}, ibtn);
                    on(ibtn, 'click', this.HandleInfoButton.bind(this));
                    this._rowCtr += 1;
                    domClass.toggle(node, 'layout-left', true);
                    //domCon.create("button",{type:'button','class':'btn btn-sm'},node);
                    //TODO: RenderActionCell
                },
                HandleInfoButton: function (event) {
                    this.parent.HandleFeature(parseInt(domAttr.get(event.currentTarget, 'data-index')));
                },
                HandleZoomToFeature: function (event) {

                    topic.publish('features/zoom', {'layerId': this.layerItem.id, 'feature': event.currentTarget.name});

                },
                HandleItemCB: function (event) {
                    console.log('clicked', event.currentTarget.name, event.currentTarget.value);
                },
                RenderURLCells: function (object, value, node, options) {
                    domCon.create('a', {'href': value, 'innerHTML': value, 'title': value}, node);
                },
                Busy: function (message) {
                    this.parent.progress_indicator.Show(message);
                    this.searchCriteria.Updating();
                },
                _MakeSearchParams: function () {
                    var memory = Layers.GetMemory();
                    var params = {layer: this.layerItem.id, first: this.currStart, limit: this.recordCounter.value}
                    var mapLimit = this.searchCriteria.GetMapLimit();
                    if (mapLimit !== false) {
                        params['bbox'] = mapLimit;
                    }
                    var criteria = this.searchCriteria.GetCriteria();

                    for (var c = criteria.length - 1; c >= 0; c--) {
                        var criterion = criteria[c];
                        if (criteria[c].field === 'memory') {
                            if (!memory.HasMemoryLayer()) {
                                continue;
                            }
                            criteria.splice(c, 1);
                            var memoryInfo = memory.Restore();
                            params['memoryLayer'] = memoryInfo.layerId
                            params['intersectMode'] = criterion.compare;
                            params['gids'] = Layers.GetMemory().FeaturesToString();
                            if (criterion.hasOwnProperty('buffer')) {
                                params['buffer'] = criterion.buffer;
                            }
                        }
                    }
                    this.filters = JSON.stringify(criteria);
                    params.filters = this.filters;
                    this._params = params;
                    return this._params;
                    ;
                },
                RestoreMemory: function () {
                    this.searchCriteria.RestoreMemory();
                    //Layers.SearchFeatures(params, this.HandleFeatures.bind(this));
                },
                MultiCriteriaSearch: function (numrows, criteria) {

                    this.recordCounter.SetValue(numrows);
                    this.currStart = 0;
                    this.currSort = null;
                    this._MakeSearchParams();
                    this.Busy('Loading');
                    this.searchCriteria.ResetImages(false);
                    Layers.SearchFeatures(this._params, this.HandleFeatures.bind(this));
                },
                PageLeft: function (event) {
                    if(this._layerId === '*') return;
                    this._bbox = this.searchCriteria.GetMapLimit();
                    this.currStart = this.currStart - this.recordCounter.value >= 0 ? this.currStart - this.recordCounter.value : 0;
                    domClass.remove(this.record_start, "invalid");
                    this.Busy('Loading');
                    if (this._params === null)
                        this.MakeSearchParams();
                    this._params['first'] = this.currStart;
                    Layers.SearchFeatures(this._params, this.HandleFeatures.bind(this));
                },
                PageRight: function (event) {
                    if(this._layerId === '*') return;
                    this._bbox = this.searchCriteria.GetMapLimit();
                    this.currStart = this.currStart + this.recordCounter.value;
                    domClass.remove(this.record_start, "invalid");
                    if (this._params === null)
                        this.MakeSearchParams();
                    this._params['first'] = this.currStart;
                    this.Busy('Loading');
                    this._params['first'] = this.currStart;
                    Layers.SearchFeatures(this._params, this.HandleFeatures.bind(this));
                },
                SelectHandler: function (event) {
                    var sels = [];
                    for (var item in this.gridObj.selection) {
                        if (this.gridObj.selection[item] === true)
                            sels.push(item);
                    }
                    topic.publish('features/selchange', {'layerId': this._layerId, 'features': sels});
                    //this.parent.HandleFeature(event.rows[0].element.rowIndex + this.currStart);
                    //domClass.toggle(event.rows[0].element, "dgrid-selected");
                    //this.gridObj.selection[(event.rows[0].id)] = false;
                },
                _rowCtr: 0,
                UpdateGrid: function () {
                    this._rowCtr = 0;
                    console.log('processing');
                    this.Busy('Processing');
                    if (this.currStart === 0) {
                        domClass.add(this.left, "disabled");
                        domAttr.set(this.left, "disabled", "disabled");
                    } else {
                        domClass.remove(this.left, "disabled");
                        domAttr.remove(this.left, "disabled");
                    }
                    var end = this.currStart + this.recordCounter.value;
                    if (end > this.count) {
                        end = this.count;
                        domClass.add(this.right, "disabled");
                        domAttr.set(this.right, "disabled", "disabled");
                    } else {
                        domClass.remove(this.right, "disabled");
                        domAttr.remove(this.right, "disabled");
                    }
                    this.record_start.value = (1 + this.currStart);
                    this.record_text.innerHTML = " to " + end + " of " + this.count;

                    TrackableMemory = declare([Memory, Trackable], {
                        sort: function (sorted) {
                            sorted = [];
                            return this.inherited(arguments);
                        }
                    });
                    var columns = [];

                    var visibleKeys = [];//, searchableKeys = [];
                    var atts = this.loadedItem.layerAttributes;
                    for (var i = 0, len = atts.length; i < len; i++) {
                        if (atts[i].visible) {
                            visibleKeys.push(atts[i]);
                            if (atts[i].searchable) {
                                this.searchableKeys.push(atts[i]);
                            }
                        }
                    }


                    if (visibleKeys.length > 0) {
                        visibleKeys[0].display = "id";
                        visibleKeys[0].name = "gid";
                    }


                    var memory = new TrackableMemory({data: this.data, idField: "gid", idProperty: "gid"});
                    columns.push({"field": 'gid', 'label': '', renderCell: this.RenderActionsCell.bind(this)});
                    for (var i = 0, len = visibleKeys.length; i < len; i++) {
                        switch (visibleKeys[i].requirement) {
                            case "url":
                                columns.push({
                                    field: visibleKeys[i].name,
                                    label: visibleKeys[i].display,
                                    renderCell: this.RenderURLCells.bind(this)
                                });
                                break;
                            default:
                                columns.push({
                                    field: visibleKeys[i].name,
                                    label: visibleKeys[i].display,
                                    renderCell: this.RenderCells.bind(this)
                                });
                        }
                    }

                    this.gridObj = new MyGrid(
                            {
                                collection: memory,
                                columns: columns,
                                selectionMode: 'single',
                                minRowsPerPage: 1000,
                                maxRowsPerPage: 1000,
                                loadingMessage: 'Loading data...',
                                noDataMessage: 'No results found.',
                                bufferRows: 10
                            }
                    );
                    this.gridObj.placeAt(this.grid);
                    //this.searchCriteria.UpdateLayerImages(500,500);
                    setTimeout(this._DelayedStart.bind(this), 500);
                },
                _DelayedStart: function () {
                    this.gridObj.startup();
                    this.parent.progress_indicator.Hide();
                },
                HandleSort: function (event) {
                    this.record_text.innerHTML = "Loading...";
                    event.preventDefault();
                    this.searchCriteria.ResetImages(false);
                    var sort = event.sort[0];
                    var order = sort.descending ? "DESC" : "ASC";
                    var prop = sort.property;
                    this.gridObj.updateSortArrow(event.sort, true);

                    this.currSort = JSON.stringify([{field: prop, direction: order}]);

                    this.Busy('Loading');

                    Layers.SearchFeatures({layer: this.layerItem.id, sort: this.currSort, first: this.currStart, limit: this.recordCounter.value, filters: this.filters}, this.HandleFeatures.bind(this));
                },

                UpdateRecords: function () {
                    this.searchCriteria.ResetImages(false);
                    this.Busy('Loading');
                    Layers.SearchFeatures({layer: this.layerItem.id, first: this.currStart, limit: this.recordCounter.value, filters: this.filters}, this.HandleFeatures.bind(this));
                },
                RefreshGrid: function (results) {
                    console.log('Processing');
                    this.parent.progress_indicator.SetLabel("Processing", false);
                    setTimeout((function () {
                        this._DelayedRefresh(results)
                    }).bind(this), 500);

                },
                _DelayedRefresh: function (results) {
                    this.data = results.resultset.results;
                    this.count = results.paging.count;

                    if (this.currStart === 0) {
                        domClass.add(this.left, "disabled");
                        domAttr.set(this.left, "disabled", "disabled");
                    } else {
                        domClass.remove(this.left, "disabled");
                        domAttr.remove(this.left, "disabled");
                    }
                    var end = this.currStart + this.recordCounter.value;
                    if (end > this.count) {
                        end = this.count;
                        domClass.add(this.right, "disabled");
                        domAttr.set(this.right, "disabled", "disabled");
                    } else {
                        domClass.remove(this.right, "disabled");
                        domAttr.remove(this.right, "disabled");
                    }
                    this.record_start.value = (1 + this.currStart);
                    this.record_text.innerHTML = " to " + end + " of " + this.count;

                    var memory = new TrackableMemory({data: this.data, idField: "gid", idProperty: "gid"});

                    this.gridObj.set("collection", memory);
                    this.gridObj.refresh();

                    this.RefreshColors();

                    this.parent.progress_indicator.Hide();
                }
               

            });
        });
