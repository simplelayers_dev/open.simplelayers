define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-attr",
		 "dojo/dom-class",
		 "dojo/dom-style",
		 "dijit/form/Select",
		 "dijit/form/CheckBox",
		 'dojo/_base/json',
		 "dojo/json",
		 "dojo/dom-construct",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dojo/text!./templates/font_selector.tpl.html"],
		function (declare, 
				  on,
				  domAttr,
				  domClass,
				  domStyle,
				  selector,
				  checkBox,
				  baseJson,
				  json,
				  domCon,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
				  template) {
		    return declare('font_selector/font_selector', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    templateString: template,
                baseClass:'font_selector',
				
				driver: null,
				fonts: null,
				temp: null,
				weightsArray: null,
				jsonString: null,
				italicBool: true,
				currentValue: null,
				
				
			    postCreate: function () {
			    	this.fonts = [];
			    	this.weightsArray = [];
			    	
			    	this.typeSelector.on("change", this.UpdateFontWeights.bind(this));
			    	this.weightSelector.on("change", this.UpdateItalicOption.bind(this));
			    	this.italicOption.on("change", this.NotifyListeners.bind(this));
			    },
			    SetDriver: function(driver){
			    	this.driver = baseJson.fromJson(driver);
			    	console.log(this.driver);
			    	
			    	this.UpdateFontTypes();
			    },
			    UpdateFontTypes: function(){
			    
			    	var firstItem = null;
			    	for(var i = 0; i < this.driver.fonts.length; i++){
			    		var currentType = this.driver.fonts[i];
			    		if(i == 0){
			    			firstItem = {label: currentType.name, value: ""+i, value_obj: currentType.font, selected: true};
			    			this.fonts.push(firstItem);
			    		}
			    		else
			    			this.fonts.push({label: currentType.name, value: ""+i, value_obj: currentType.font});
			    		
			    	}
			    	
			    	console.log(this.fonts);
			    	this.typeSelector.set('options',this.fonts);
			    	//this.typeSelector.selectedIndex = 0;
			    	
			    	this.typeSelector.set('value', firstItem.value);
			    	//this.UpdateFontWeights();
			    },
			    UpdateFontWeights: function(value){
			    	this.weightsArray =[];
			    	var currentType = this.fonts[value].value_obj;
			    	var secondItem = null;
			    	for(var i = 0; i < currentType.styles.length; i++){
			    		var currentStyle = currentType.styles[i];
			    		if(i == 0){
			    			secondItem = {label: currentStyle.name, value: ""+i, value_obj: currentStyle, selected: true};
			    			this.weightsArray.push(secondItem);
			    		}
			    		else
			    			this.weightsArray.push({label: currentStyle.name, value: ""+i, value_obj: currentStyle});
			    	}
			    	console.log(this.weightsArray);
			    	this.weightSelector.set('options',this.weightsArray);
			    	//this.weightSelector.selectedIndex = 0;
			    	
			    	this.weightSelector.set('value',secondItem.value);
			    	this.UpdateItalicOption(secondItem.value);
			    },
			    UpdateItalicOption: function(value){
			    	var currentStyle = this.weightsArray[value].value_obj;
			    	this.italicBool = false;
			    	if(currentStyle.italic == true){
			    		domClass.remove(this.italicOption.domNode, "hidden");
			    		this.currentValue = currentStyle.value_i;
				    }
			    	else{
			    		domClass.add(this.italicOption.domNode, "hidden");
			    		this.currentValue = currentStyle.value;
			    	}
			    	this.NotifyListeners();
			    },
			    SetValue: function(fontName){
			    	this.UpdateFontTypes();
			    	var temporary;
			    	for(var x = 0; x < this.fonts.length; x++){
			    		var tempValue = this.fonts[x].value_obj.styles;
			    		for(var y = 0; y < tempValue.length; y++){
			    			if(tempValue.value_obj.value == fontName){
			    				this.currentValue = tempValue.value_obj.value;
			    				this.typeSelector.set('value', this.fonts[x]);
			    				this.weightSelector.set('value',tempValue[y]);
			    				this.UpdateItalicOption();
			    			}
			    			else{
			    				this.currentValue = tempValue.value_obj.value_i;
			    				this.typeSelector.set('value', this.fonts[x]);
			    				this.weightSelector.set('value',tempValue[y]);
			    				this.UpdateItalicOption();
			    			}
			    		}
			    	}
			    },
			    GetValue: function(){
			    	
			    },
			    NotifyListeners: function(){
			    	on.emit(this.domNode, 'change', { bubbles: true, cancelable: true, value: this.currentValue });
			    }
				
			});
		}
	);
	