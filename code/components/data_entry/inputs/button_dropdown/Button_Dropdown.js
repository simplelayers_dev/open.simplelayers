define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/dom-style",
    "sl_components_open/data_entry/inputs/InputClass",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./Button_Dropdown.tpl.html"],
        function (declare,
                on,
                dom,
                domCon,
                domAttr,
                domClass,
                domStyle,
                InputClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            'Button_Dropdown',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                /**
                                * Part of the Open SimpleLayers project
                                * https://bitbucket.org/simplelayers_dev/open.simplelayers
                                * 
                               **/
                                data: null,
                                name: null,
                                templateString: template,
                                baseClass: 'button_dropdown flex-container flex-item row',
                                constructor: function (data) //needs placeholder, label, buttontext, options, rows
                                {
                                    var currentThis = this;
                                    this.data = data;
                                    this.heightCalculated = false;
                                    this.optionData = [];
                                    this.selectedItem = {
                                        set Set(e) {
                                            this.item = e;
                                            currentThis.textinput.value = domAttr.get(e, "title");
                                            currentThis.selectedIndex.index = currentThis.optionData.indexOf(e);
                                            domClass.add(currentThis.div, "has-success");
                                        },
                                        get Option() {
                                            return domAttr.get(this.item, "title");
                                        },
                                        item: null
                                    };
                                    this.selectedIndex = {
                                        set Set(num) {
                                            this.index = num;
                                            currentThis.HandleActionButton({type: 'click', target: currentThis.optionData[num].firstChild});
                                        },
                                        get Option() {
                                            return currentThis.selectedItem.Option;
                                        },
                                        index: 0
                                    };
                                    this.numRows = {
                                        set Set(num) {
                                            this.rows = num;
                                            currentThis.heightCalculated = true;
                                            domStyle.set(currentThis.optionlist, {
                                                maxHeight: 12 + (num * domStyle.get(currentThis.optionData[0], "height")) + "px",
                                                overflowY: "auto"
                                            });
                                        },
                                        rows: 5
                                    };
                                },
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("placeholder"))
                                    {
                                        domAttr.set(this.textinput, "placeholder", this.data["placeholder"]);
                                    } else
                                    {
                                        domAttr.set(this.textinput, "placeholder", "");
                                    }
                                    if (this.data.hasOwnProperty("label"))
                                    {
                                        this.label.innerHTML = this.data["label"];
                                    } else
                                    {
                                        this.label.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("buttontext"))
                                    {
                                        this.buttontext.innerHTML = this.data["buttontext"] + '<span class="caret"></span>';
                                    } else
                                    {
                                        this.buttontext.innerHTML = '<span class="caret"></span>';
                                    }
                                    if (this.data.hasOwnProperty("options"))
                                    {
                                        if (Array.isArray(this.data["options"])) {
                                            var op = this.data["options"];
                                        } else {
                                            var op = this.data["options"].split(",");
                                        }
                                        for (var i = 0, len = op.length; i < len; i++)
                                        {
                                            var option = op[i];
                                            if (option.length > 20) {
                                                option = option.substr(0, 20) + "...";
                                            }
                                            var a = domCon.place('<li title="' + op[i] + '"><a>' + option + '</a></li>', this.optionlist);
                                            on(a, 'click', this.HandleActionButton.bind(this));
                                            this.optionData.push(a);
                                        }
                                    } else
                                    {
                                        this.optionlist.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("rows"))
                                    {
                                        this.numRows.rows = this.data["rows"];
                                    } else
                                    {
                                        this.numRows.rows = domAttr.get(this.div, "data-rows");
                                    }

                                    var buttonDropdownID = this.id + "button_dropdown";
                                    domAttr.set(this.label, "for", buttonDropdownID);
                                    domAttr.set(this.textinput, "id", buttonDropdownID);
                                    
                                    on(this.textinput, 'keyup', this.FilterOptions.bind(this));
                                    on(this.buttontext, "click", function () {
                                        if (!this.heightCalculated) {
                                            jQuery(this.buttontext).dropdown("toggle");
                                            this.numRows.Set = this.numRows.rows;
                                            jQuery(this.buttontext).dropdown("toggle");
                                        }
                                        this.AvailWhenHandler();
                                    }.bind(this));
                                },
                                HandleActionButton: function (event)
                                {
                                    if (typeof event === 'undefined') {
                                        this.selectedItem.Set = this.optionData[this.selectedIndex.index];
                                    } else {
                                        this.selectedItem.Set = event.target.parentNode;
                                    }
                                    this.FilterOptions(event);
                                },
                                FilterOptions: function (event)
                                {
                                    if ((domAttr.get(this.buttontext, "aria-expanded") === "false") || !domAttr.has(this.buttontext, "aria-expanded")) {
                                        jQuery(this.buttontext).dropdown("toggle");
                                        if (!this.heightCalculated) {
                                            this.numRows.Set = this.numRows.rows;
                                        }
                                        this.textinput.focus();
                                    }
                                    var text = this.textinput.value.trim().toLowerCase();
                                    var validCount = 0;
                                    var validIndex = 0;
                                    for (var i = 0, len = this.optionData.length; i < len; i++)
                                    {
                                        if (domAttr.get(this.optionData[i], "title").toLowerCase().substring(0, text.length) !== text) {
                                            domClass.add(this.optionData[i], "hidden");
                                        } else {
                                            domClass.remove(this.optionData[i], "hidden");
                                            validCount++;
                                            validIndex = i;
                                        }
                                    }
                                    if (validCount === 0) {
                                        domClass.add(this.div, "has-error");
                                        domClass.remove(this.div, "has-success");
                                    } else {
                                        domClass.remove(this.div, "has-error");
                                        if (event.type === 'click') {
                                            //don't do anything if user selects an option from dropdown
                                        } else if (validCount === 1 && text === domAttr.get(this.optionData[validIndex], "title").toLowerCase()) {
                                            this.selectedIndex.Set = validIndex;
                                        } else {
                                            domClass.remove(this.div, "has-success");
                                        }
                                    }
                                    this.AvailWhenHandler();
                                },
                                GetValue: function ()
                                {
                                    return this.textinput.value;
                                },
                                SetValue: function (val)
                                {
                                    this.textinput.value = val;
                                    this.FilterOptions(this);
                                },
                                Disable: function () {
                                    this.textinput.readOnly = this.disableBoolean;
                                    this.buttontext.disabled = this.disableBoolean;
                                }
                            }
                    );
        }
);