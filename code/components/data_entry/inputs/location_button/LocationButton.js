define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/text!./LocationButton.tpl.html"],
        function (declare,
                on,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                template) {
            return declare("LocationButton", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        /**
                         * Part of the Open SimpleLayers project
                         * https://bitbucket.org/simplelayers_dev/open.simplelayers
                         * 
                         **/
                        data: null,
                        name: null,
                        locationObject: {},
                        templateString: template,
                        baseClass: 'location_button flex-container flex-item',
                        postCreate: function ()
                        {
                            if (this.data.hasOwnProperty("locateButton"))
                            {
                                this.locateButton.value = this.data["locateButton"];
                            } else
                            {
                                this.locateButton.value = "";
                            }

                            if (this.data.hasOwnProperty("latField"))
                            {
                                this.latField = this.data["latField"];
                            }

                            if (this.data.hasOwnProperty("lonField"))
                            {
                                this.lonField = this.data["lonField"];
                            }

                            if (this.data.hasOwnProperty("accuracyField"))
                            {
                                this.accuracyField = this.data["accuracyField"];
                            }

                            var locateButtonId = this.id + 'locationbutton';
                            domAttr.set(this.label, "for", locateButtonId);
                            domAttr.set(this.locateButton, "id", locateButtonId);

                            on(this.locateButton, 'click', this.HandleActionButton.bind(this));
                        },
                        HandleActionButton: function (event)
                        {
                            if (navigator.geolocation) {
                                navigator.saveWidget = this;
                                navigator.geolocation.getCurrentPosition(this.UserPosition, this.PositionError);
                            } else {
                                alert("This browser does not support geolocation");
                            }
                            this.AvailWhenHandler();
                        },
                        UserPosition: function (position)
                        {
                            this.latitude = position.coords.latitude.toFixed(4);
                            this.longitude = position.coords.longitude.toFixed(4);
                            this.accuracy = position.coords.accuracy;
                            navigator.saveWidget.SetForm();
                        },
                        PositionError: function (error)
                        {
                            alert("Location could not be accessed");
                        },
                        SetForm: function ()
                        {
                            var foundWidget;
                            var locationLabel = "";

                            if (typeof this.latField !== 'undefined') {
                                foundWidget = this.parentForm.getInput(this.latField);
                                if (typeof foundWidget !== 'undefined') {
                                    if (foundWidget.hasOwnProperty("textinput")) {
                                        if(this.latitude !== null && this.latitude.length !== 0) {
                                            foundWidget.SetValue(this.latitude);
                                            this.locationObject["latitude"] = this.latitude;
                                            locationLabel = locationLabel.concat("Latitude:" + this.latitude + " ");
                                            this.latitude = "";
                                        }
                                    }
                                }
                            }
                            if (typeof this.lonField !== 'undefined') {
                                foundWidget = this.parentForm.getInput(this.lonField);
                                if (typeof foundWidget !== 'undefined') {
                                    if (foundWidget.hasOwnProperty("textinput")) {
                                        if(this.longitude !== null && this.longitude.length !== 0) {
                                            foundWidget.SetValue(this.longitude);
                                            this.locationObject["longitude"] = this.longitude;
                                            locationLabel = locationLabel.concat("Longitude:" + this.longitude + " ");
                                            this.longitude = "";
                                        }
                                    }
                                }
                            }
                            if (typeof this.accuracyField !== 'undefined') {
                                foundWidget = this.parentForm.getInput(this.accuracyField);
                                if (typeof foundWidget !== 'undefined') {
                                    if (foundWidget.hasOwnProperty("textinput")) {
                                        if(this.accuracy !== null && this.accuracy.length !== 0) {
                                            foundWidget.SetValue(this.accuracy);
                                            this.locationObject["accuracy"] = this.accuracy;
                                            locationLabel = locationLabel.concat("Accuracy:" + this.accuracy);
                                            this.accuracy = "";
                                        }
                                    }
                                }
                            }

                            this.SetValue(locationLabel);
                            this.AvailWhenHandler();
                        },
                        SetValue: function (toSet) {
                            var value = toSet;
                            if (typeof value === 'string' && value.length !== 0) {
                                try {
                                    value = JSON.parse(value);
                                } catch (e) {}
                                this.locationData.innerHTML = value;
                            } else if (typeof value === 'object' && Object.keys(value).length !== 0) { // checks if value is object and not empty
                                if (value.hasOwnProperty("latitude")) {
                                    this.latitude = value.latitude;
                                }
                                else if (typeof this.latitude === 'undefined') {
                                    this.latitude = "";
                                }
                                if (value.hasOwnProperty("longitude")) {
                                    this.longitude = value.longitude;
                                }
                                else if (typeof this.longitude === 'undefined') {
                                    this.longitude = "";
                                }
                                if (value.hasOwnProperty("accuracy")) {
                                    this.accuracy = value.accuracy;
                                }
                                else if (typeof this.accuracy === 'undefined') {
                                    this.accuracy = "";
                                }
                                
                                if(typeof navigator.saveWidget === 'undefined') {
                                    this.SetForm();
                                }
                                else {
                                    navigator.saveWidget.SetForm();
                                }
                            }
                        },
                        GetValue: function () {
                            if (Object.keys(this.locationObject).length === 0) {
                                return false;
                            }
                            return this.locationObject;
                        },
                        Disable: function () {
                            this.locateButton.disabled = this.disableBoolean;
                        }
                    });
        });


