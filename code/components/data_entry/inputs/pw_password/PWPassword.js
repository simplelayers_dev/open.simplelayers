define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/text!./PWPassword.tpl.html"],
        function (declare,
                on,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                template) {
            return declare('PWPassword', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        /**
                         * Part of the Open SimpleLayers project
                         * https://bitbucket.org/simplelayers_dev/open.simplelayers
                         * 
                         **/
                        data: null,
                        name: null,
                        templateString: template,
                        baseClass: 'password flex-container flex-item row',
                        postCreate: function ()
                        {
                            if (this.data.hasOwnProperty("placeholder"))
                            {
                                domAttr.set(this.passinput, "placeholder", this.data["placeholder"]);
                            } else
                            {
                                domAttr.set(this.passinput, "placeholder", "");
                            }

                            if (this.data.hasOwnProperty("help"))
                            {
                                this.help.innerHTML = this.data["help"];
                            } else
                            {
                                this.help.innerHTML = "";
                            }

                            var passwordInputId = this.id + 'passinput';
                            domAttr.set(this.label, "for", passwordInputId);
                            domAttr.set(this.passinput, "id", passwordInputId);
                            on(this.passinput, 'keyup', this.AvailWhenHandler.bind(this));
                        },
                        HandleActionButton: function (event)
                        {

                        },
                        SetValue: function (toSet)
                        {
                            this.passinput.value = toSet;
                        },
                        GetValue: function ()
                        {
                            return this.passinput.value;
                        },
                        Disable: function () {
                            this.passinput.readOnly = this.disableBoolean;
                        }
                    });
        });


