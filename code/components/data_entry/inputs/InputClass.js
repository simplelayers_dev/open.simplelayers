define(["dojo/_base/declare", "dojo/dom-class", "dojo/on"],
        function (declare, domClass, on)
        {
            return declare
                    (
                            'inputBaseClass', [],
                            {
                                data: null,
                                name: null,
                                parentForm: null,
                                constructor: function (data)
                                {
                                    this.data = data;
                                },
                                SetCommonAttributes: function () {
                                    if (this.data.hasOwnProperty("visible"))
                                    {
                                        this.visible = this.data["visible"].toString() === "true";
                                    } else
                                    {
                                        this.visible = true;
                                    }
                                    
                                    this.Toggle(this.visible);

                                    if (this.data.hasOwnProperty("name"))
                                    {
                                        this.name = this.data["name"];
                                    } else
                                    {
                                        this.name = "";
                                    }
                                    
                                    var type = this.data["type"] || this.data["componentType"];
                                    if (this.data.hasOwnProperty("label"))
                                    {
                                        this.label.innerHTML = this.data["label"];
                                    } else if (!(type === "inputGroup" || type === "grp"))
                                    {
                                        this.label.innerHTML = "";
                                    }
                                    
                                    if (this.data.hasOwnProperty("value")) {
                                        if(typeof this.SetValue === 'function') {
                                            this.SetValue(this.data["value"]);
                                        }
                                    }
                                    
                                    if (this.data.hasOwnProperty("avail-when"))
                                    {
                                        this.IsAvailable(this.data["avail-when"]);
                                    }
                                    
                                    if(this.data.hasOwnProperty("disable"))
                                    {
                                        var disableData = this.data["disable"];
                                        this.disableBoolean = disableData.toString() === "true";
                                        if(typeof this.Disable === 'function') {
                                                this.Disable();
                                        }
                                    } else
                                    {
                                        this.disableBoolean = false;
                                    }
                                    on(this.domNode, 'click', this.DisableStart.bind(this));
                                },
                                Toggle: function (state) {
                                    if (state === 'undefined')
                                    {
                                        state = true;
                                    }
                                    this.visible = state;
                                    state = !state;
                                    domClass.toggle(this.domNode, "hidden", state);
                                },
                                GetValueIfVis: function () {
                                    if (this.visible === false)
                                    {
                                        return null;
                                    }
                                    return this.GetValue();
                                },
                                SetParentForm: function (form) {
                                    this.parentForm = form;
                                },
                                IsAvailable: function (when) {
                                    if (typeof when === 'undefined') {
                                        when = this.data["avail-when"];
                                    }
                                    if (typeof when !== 'undefined') {
                                        var split = when.split("=");
                                        var notOperator = false;
                                        
                                        if(split[0].startsWith("!")) {
                                            split[0] = split[0].substring(1);
                                            notOperator = true;
                                        }
                                        else if(split[0].endsWith("!")) {
                                            split[0] = split[0].substring(0, split[0].length - 1);
                                            notOperator = true;
                                        }
                                        
                                        var property = null;
                                        
                                        if(split[0].startsWith("#")) {
                                            split[0] = split[0].substring(1);
                                            var propertySplit = split[0].split(".");
                                            
                                            // name of the widget
                                            split[0] = propertySplit[0];
                                            
                                            // name of the property that is looked for if set
                                            property = propertySplit[1];
                                        }
                                        
                                        if (typeof this.parentForm.getInput(split[0]) !== 'undefined') {
                                            var input = null;
                                            
                                            if(property !== null) {
                                                input = this.parentForm.getInput(split[0]).data;
                                                if(input.hasOwnProperty(property)) {
                                                    input = input[property];
                                                }
                                                else {
                                                    input = null;
                                                }
                                            }
                                            else {
                                                input = this.parentForm.getInput(split[0]).GetValueIfVis();
                                            }
                                            split[1] = split[1].toLowerCase();
                                            
                                            if (input !== null) {
                                                // converts boolean to string using toString()
                                                input = input.toString().toLowerCase();
                                            }
                                            var isMatch = input === split[1];
                                            if(notOperator) {
                                                isMatch = !isMatch;
                                            }
                                            this.Toggle(isMatch);
                                        }
                                    }

                                },
                                DisableStart: function (event) {
                                    var target = event.target.nodeName;
                                    
                                    // so disable toggle does not trigger based off of the input or label selection
                                    if(target === "DIV") {
                                        
                                        var type = this.data["type"] || this.data["componentType"];
                                        if(this.parentForm.disableToggle && !(type === "inputGroup" || type === "grp")) {
                                            this.disableBoolean = !this.disableBoolean;
                                            
                                            if(this.disableBoolean) {
                                                this.domNode.style.backgroundColor = "#00cccc";
                                            }
                                            else {
                                                this.domNode.style.backgroundColor = "#eee";
                                            }
                                            if(typeof this.Disable === 'function') {
                                                this.Disable();
                                            }
                                        }
                                    }
                                },
                                AvailWhenHandler: function (event) {
                                    this.parentForm.itemChanged();
                                },
                                ButtonAvailWhenHandler: function (event) {
                                    if (this.parentForm) {
                                        this.availWhenToggle = !this.availWhenToggle;
                                        this.parentForm.itemChanged();
                                    }
                                }
                            });
        });


