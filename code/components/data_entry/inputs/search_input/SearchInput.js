define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "sl_components_open/data_entry/inputs/InputClass",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./SearchInput.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                InputClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            /**
                             * Part of the Open SimpleLayers project
                             * https://bitbucket.org/simplelayers_dev/open.simplelayers
                             * 
                             **/
                            'SearchInput',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                data: null,
                                name: null,
                                templateString: template,
                                baseClass: 'search_input flex-container flex-item row',
                                constructor: function (data)
                                {
                                    this.data = data;
                                },
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("placeholder"))
                                    {
                                        domAttr.set(this.searchinput, "placeholder", this.data["placeholder"]);
                                    } else
                                    {
                                        domAttr.set(this.searchinput, "placeholder", "");
                                    }
                                    if (this.data.hasOwnProperty("label"))
                                    {
                                        this.label.innerHTML = this.data["label"];
                                    } else
                                    {
                                        this.label.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("help"))
                                    {
                                        this.help.innerHTML = this.data["help"];
                                    } else
                                    {
                                        this.help.innerHTML = "";
                                    }
                                    var searchInputID = this.id + "searchinput";
                                    domAttr.set(this.label, "for", searchInputID);
                                    domAttr.set(this.searchinput, "id", searchInputID);
                                    domAttr.set(this.searchinput, "name", searchInputID);
                                    on(this.searchinput, 'keyup', this.AvailWhenHandler.bind(this));
                                },
                                HandleActionButton: function (event)
                                {

                                },
                                GetValue: function ()
                                {
                                    return this.searchinput.value;
                                },
                                SetValue: function (val)
                                {
                                    this.searchinput.value = val;
                                    this.searchInput_readonly.innerHTML = this.searchinput.value;
                                },
                                Disable: function () {
                                    this.searchinput.readOnly = this.disableBoolean;
                                }
                            }
                    );
        }
);