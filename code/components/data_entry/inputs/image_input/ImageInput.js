define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/text!./ImageInput.tpl.html"],
        function (declare,
                on,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                template) {
            return declare("ImageInput", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        data: null,
                        name: null,
                        templateString: template,
                        baseClass: 'ImageInput',
                        postCreate: function ()
                        {
                            if (this.data.hasOwnProperty("placeholder"))
                            {
                                domAttr.set(this.urlText, "placeholder", this.data["placeholder"]);
                            } else
                            {
                                domAttr.set(this.urlText, "placeholder", "");
                            }

                            if (this.data.hasOwnProperty("urlButton"))
                            {
                                this.urlButton.value = this.data["urlButton"];
                            } else
                            {
                                this.urlButton.value = "Test url";
                            }

                            if (this.data.hasOwnProperty("help"))
                            {
                                this.help.innerHTML = this.data["help"];
                            } else
                            {
                                this.help.innerHTML = "";
                            }

                            this.Toggle(this.visible);

                            var urlInputId = this.id + 'urlinput';
                            domAttr.set(this.label, "for", urlInputId);
                            domAttr.set(this.urlText, "id", urlInputId);

                            on(this.urlText, 'keyup', this.AvailWhenHandler.bind(this));
                            on(this.urlButton, 'click', this.HandleActionButton.bind(this));
                        },
                        HandleActionButton: function (event)
                        {
//                            window.open(this.GetValue(), "_blank");
//                            if (document.getElementById('img-thumbnail') !== null)
//                            {
//                                domAttr.set(this.anchor, "href", chooser.value);
//                                domAttr.set(this.image, "src", chooser.value);
//                            } else {
                                domAttr.set(this.anchor, "href", this.GetValue());
                                domAttr.set(this.image, "src", this.GetValue());
//                            }
                            var pic=document.getElementById("file_chooser").value;
                            domAttr.set(this.anchor,"href", pic);
                            domAttr.set(this.image,"src", pic);
                            


                        },
                        SetValue: function (toSet)
                        {
                            this.urlText_readonly.innerHTML = toSet;
                            this.urlText_readonly.href = toSet;
                            inputNode.value = chooser.value.replace("C:\\fakepath\\", "");
                            this.urlText.value = toSet;
                            this.thumbnail_readonly.innerHTML=toSet;
                            
                        },
                        GetValue: function ()
                        {
                            return this.urlText.value;
                        },
                        Disable: function () {
                            this.urlText.readOnly = this.disableBoolean;
                        }


                    });
        });
