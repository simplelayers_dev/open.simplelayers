define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "sl_components_open/data_entry/inputs/InputClass",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./Checkbox.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                InputClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            'Checkbox',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                /**
                                 * Part of the Open SimpleLayers project
                                 * https://bitbucket.org/simplelayers_dev/open.simplelayers
                                 * 
                                 **/
                                data: null,
                                name: null,
                                templateString: template,
                                baseClass: "checkbox flex-container flex-item row",
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("help"))
                                    {
                                        this.help.innerHTML = this.data["help"];
                                    } else {
                                        this.help.innerHTML = "";
                                    }

                                    var cbId = this.id + 'cb';
                                    domAttr.set(this.cb, "id", cbId);
                                    domAttr.set(this.label, "for", cbId);
                                    on(this.cb, 'change', this.AvailWhenHandler.bind(this));
                                },
                                HandleActionButton: function (event)
                                {
                                    // is this necessary?
                                },
                                GetValue: function ()
                                {
                                    return this.cb.checked === true;
                                },
                                SetValue: function (toSet)
                                {
                                    var checked = toSet === true;
                                    if (toSet === "true")
                                        checked = true;
                                    this.cb.checked = checked;
                                },
                                Disable: function () {
                                    this.cb.disabled = this.disableBoolean;
                                }
                            }
                    );
        }
);