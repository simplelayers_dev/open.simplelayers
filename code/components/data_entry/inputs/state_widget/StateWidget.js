var states;
var selectedState;
define(["dojo/_base/declare", //imported js files
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./StateWidget.tpl.html",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/dom-construct"],
        function (declare, //variables for the imported js files
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template,
                InputClass,
                domCon)
        {
            return declare
                    (
                            "StateWidget", 
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass], 
                            {
                                templateString: template,
                                baseClass: "StateWidget",
                                data : null,
                                name : null,
                                postCreate: function () 
                                {
                                    states = {
                                   "AL": "Alabama",
                                   "AK": "Alaska",
                                   "AS": "American Samoa",
                                   "AZ": "Arizona",
                                   "AR": "Arkansas",
                                   "CA": "California",
                                   "CO": "Colorado",
                                   "CT": "Connecticut",
                                   "DE": "Delaware",
                                   "DC": "District Of Columbia",
                                   "FM": "Federated States Of Micronesia",
                                   "FL": "Florida",
                                   "GA": "Georgia",
                                   "GU": "Guam",
                                   "HI": "Hawaii",
                                   "ID": "Idaho",
                                   "IL": "Illinois",
                                   "IN": "Indiana",
                                   "IA": "Iowa",
                                   "KS": "Kansas",
                                   "KY": "Kentucky",
                                   "LA": "Louisiana",
                                   "ME": "Maine",
                                   "MH": "Marshall Islands",
                                   "MD": "Maryland",
                                   "MA": "Massachusetts",
                                   "MI": "Michigan",
                                   "MN": "Minnesota",
                                   "MS": "Mississippi",
                                   "MO": "Missouri",
                                   "MT": "Montana",
                                   "NE": "Nebraska",
                                   "NV": "Nevada",
                                   "NH": "New Hampshire",
                                   "NJ": "New Jersey",
                                   "NM": "New Mexico",
                                   "NY": "New York",
                                   "NC": "North Carolina",
                                   "ND": "North Dakota",
                                   "MP": "Northern Mariana Islands",
                                   "OH": "Ohio",
                                   "OK": "Oklahoma",
                                   "OR": "Oregon",
                                   "PW": "Palau",
                                   "PA": "Pennsylvania",
                                   "PR": "Puerto Rico",
                                   "RI": "Rhode Island",
                                   "SC": "South Carolina",
                                   "SD": "South Dakota",
                                   "TN": "Tennessee",
                                   "TX": "Texas",
                                   "UT": "Utah",
                                   "VT": "Vermont",
                                   "VI": "Virgin Islands",
                                   "VA": "Virginia",
                                   "WA": "Washington",
                                   "WV": "West Virginia",
                                   "WI": "Wisconsin",
                                   "WY": "Wyoming"};
                                    for(var a in states)
                                        {

                                                domCon.create("option", {name: a, innerHTML: states[a], value: a}, this.list);

                                        }
                                    if (this.data.hasOwnProperty("help"))
                                        {
                                                this.help.innerHTML = this.data["help"];
                                        }
                                        else
                                        {
                                                this.help.innerHTML = "";
                                        }
                                },
                                GetValue: function ()
                                {
                                var select = this.list;
                                for(var a in states)
                                {
                                    if(select[select.selectedIndex].value === a)
                                    {
                                        selectedState = states[a];
                                        return selectedState;
                                    }

                                }

                                },
                                SetValue: function (index)
                                {  
                                    var select = this.list;
                                    this.state_readonly.innerHTML = index;
                                    if(index==="")
                                    {
                                        select.selectedIndex = 0;
                                    }
                                }
                            }
                    );
        }
);

