define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./TextArea.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            /**
                             * Part of the Open SimpleLayers project
                             * https://bitbucket.org/simplelayers_dev/open.simplelayers
                             * 
                             **/
                            'TextArea',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
                            {
                                data: null,
                                name: null,
                                templateString: template,
                                baseClass: 'text_area flex-container flex-item col',
                                constructor: function (data)
                                {
                                    if(typeof data === "object") {
                                        this.data = data;
                                    }
                                    else {
                                        this.data = JSON.parse(data);
                                    }
                                },
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("placeholder"))
                                    {
                                        this.textarea.innerHTML = this.data["placeholder"];
                                    } else
                                    {
                                        this.textarea.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("value"))
                                    {
                                        this.textarea.value = JSON.stringify(this.data["value"], null, 4);
                                    } else
                                    {
                                        this.textarea.value = "";
                                    }
                                    if (this.data.hasOwnProperty("label"))
                                    {
                                        this.label.innerHTML = this.data["label"];
                                    } else
                                    {
                                        this.label.innerHTML = "";
                                    }
                                },
                                HandleActionButton: function (event)
                                {

                                },
                                textAreaText: function ()
                                {
                                    return this.textarea.value;
                                },
                                SetValue: function (val)
                                {
                                    this.textarea.value = val;
                                }
                            }
                    );
        }
);