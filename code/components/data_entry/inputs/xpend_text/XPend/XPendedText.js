define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "sl_components_open/data_entry/inputs/InputClass",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./XPendedText.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                InputClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            /**
                             * Part of the Open SimpleLayers project
                             * https://bitbucket.org/simplelayers_dev/open.simplelayers
                             * 
                             **/
                            'XPendedText',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                data: null,
                                name: null,
                                templateString: template,
                                baseClass: 'xpended_text flex-container flex-item row',
                                constructor: function (data)
                                {
                                    this.data = data;
                                },
                                postCreate: function ()
                                {
                                    // since domCon.destroy does not work,
                                    // this changes the class of the other template and innerHTML to empty
                                    var xPend = this.append;
                                    var xPendID = this.id;
                                    if (this.data["x"] === "append")
                                    {
                                        xPendID += "append";
                                        domAttr.set(this.prepend, "class", "");
                                        this.prepend.innerHTML = "";
                                        // domCon.destroy("prepend");
                                    }
                                    // defaults to prepend if x is invalid
                                    else
                                    {
                                        xPendID += "prepend";
                                        xPend = this.prepend;
                                        domAttr.set(this.append, "class", "");
                                        this.append.innerHTML = "";
                                        // domCon.destroy("append");
                                    }

                                    if (this.data.hasOwnProperty("placeholder"))
                                    {
                                        domAttr.set(this.xpendedtext, "placeholder", this.data["placeholder"]);
                                    } else
                                    {
                                        domAttr.set(this.xpendedtext, "placeholder", "");
                                    }
                                    if (this.data.hasOwnProperty("label"))
                                    {
                                        this.label.innerHTML = this.data["label"];
                                    } else
                                    {
                                        this.label.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("help"))
                                    {
                                        this.help.innerHTML = this.data["help"];
                                    } else
                                    {
                                        this.help.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("xpend"))
                                    {
                                        xPend.innerHTML = this.data["xpend"];
                                    } else
                                    {
                                        xPend.innerHTML = "";
                                    }
                                    domAttr.set(this.label, "for", xPendID);
                                    domAttr.set(this.xpendedtext, "id", xPendID);
                                    on(this.xpendedtext, 'keyup', this.AvailWhenHandler.bind(this));
                                },
                                HandleActionButton: function (event)
                                {

                                },
                                GetValue: function ()
                                {
                                    return this.xpendedtext.value;
                                },
                                SetValue: function (val)
                                {
                                    this.xpendedtext.value = val;
                                    this.xPendedText_readonly.innerHTML = this.xpendedtext.value;
                                },
                                Disable: function () {
                                    this.xpendedtext.readOnly = this.disableBoolean;
                                }
                            }
                    );
        }
);