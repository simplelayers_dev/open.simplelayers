define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/text!./Select.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                template)
        {
            return declare('Select', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        /**
                         * Part of the Open SimpleLayers project
                         * https://bitbucket.org/simplelayers_dev/open.simplelayers
                         * 
                         **/
                        data: null,
                        name: null,
                        templateString: template,
                        baseClass: 'select flex-container flex-item row',
                        postCreate: function ()
                        {
                            if (this.data.hasOwnProperty("options"))
                            {
                                for (var i = 0; i < this.data["options"].length; ++i)
                                {
                                    var option = this.data["options"][i];
                                    if (typeof option.value === 'undefined')
                                    {
                                        option.value = option.name;
                                    }
                                    domCon.create("option", {name: option.name, innerHTML: option.label, value: option.value}, this.select);
                                }
                            }

                            var selectInputId = this.id + "selectinput";
                            domAttr.set(this.label, "for", selectInputId);
                            domAttr.set(this.select, "id", selectInputId);
                            domAttr.set(this.select, "name", selectInputId);
                            on(this.select, 'change', this.AvailWhenHandler.bind(this));
                        },
                        HandleActionButton: function (event)
                        {

                        },
                        GetValue: function ()
                        {
                            var numIndex = this.select.selectedIndex;
                            if (numIndex === -1)
                                return -1;
                            return this.data["options"][numIndex].value;
                        },
                        SetValue: function (index)
                        {
                            // Resets the selectedIndex back to the first option.
                            if (index.trim().length === 0) {
                                this.select.selectedIndex = 0;
                            }

                            // If the index received is a number, select the numeric index.
                            else if (parseInt(index)) {
                                if (index < this.data["options"].length) {
                                    this.select.selectedIndex = index;
                                }
                            }

                            // If the index received is a text value, select the index with the given value.
                            else {
                                for (var numIndex in this.data["options"]) {
                                    if (index === this.data["options"][numIndex].value) {
                                        this.select.selectedIndex = numIndex;
                                    }
                                }
                            }
                        },
                        Disable: function () {
                            this.select.disabled = this.disableBoolean;
                        }
                    }
            );
        }
);