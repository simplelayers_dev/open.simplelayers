var cbox = [];
//var a;
define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "sl_components_open/data_entry/inputs/checkbox/Checkbox",
    "dojo/text!./CheckboxGroup.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                Checkbox,
                template)
        {
            return declare('CheckboxGroup', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        /**
                         * Part of the Open SimpleLayers project
                         * https://bitbucket.org/simplelayers_dev/open.simplelayers
                         * 
                         **/
                        data: null,
                        name: null,
                        templateString: template,
                        baseClass: 'CheckboxGroup',
                        postCreate: function ()
                        {
                            if (this.data.hasOwnProperty("options"))
                            {
                                for (var i = 0; i < this.data["options"].length; ++i)
                                {
                                    var option = this.data["options"][i];
                                    if (typeof option.value === 'undefined')
                                    {
                                        option.value = option.name;
                                    }
                                    if(this.data["layout"] === "horizontal")
                                    {
                                        if(option.symbol !== "")
                                        {
//                                            a = new Checkbox(option);
//                                            a.SetCommonAttributes();  
                                            var node = domCon.toDom('<label class="checkbox-inline"><input type="checkbox" name="' +this.data["name"]+ '" value="'+option.value+'"><span class="'+option.symbol+'"></span>'+option.name+'</label>');
                                        }
                                        else
                                        {
//                                            a = new Checkbox(option);
//                                            a.SetCommonAttributes();
                                            var node = domCon.toDom('<label class="checkbox-inline"><input type="checkbox" name="' +this.data["name"]+ '" value="'+option.value+'">'+option.name+'</label>');
                                        }
                                    }
                                    else
                                    {
                                        if(option.symbol !== "")
                                        {
//                                            a = new Checkbox(option);
//                                            a.SetCommonAttributes();
                                            var node = domCon.toDom('<div class="checkbox"><label><input type="checkbox" name="' +this.data["name"]+ '" value="'+option.value+'"><span class="'+option.symbol+'"></span>'+option.name+'</label></div>');
                                        }
                                        else
                                        {
//                                            a = new Checkbox(option);
//                                            a.SetCommonAttributes();
                                            var node = domCon.toDom('<div class="checkbox"><label><input type="checkbox" name="' +this.data["name"]+ '" value="'+option.value+'">'+option.name+'</label></div>');
                                        }
                                    }
//                                    a.placeAt(this.cbContainer);  
                                    domCon.place(node,this.cbContainer);
//                                    cbox[i] = document.createElement('input');
//                                    cbox[i].type = "checkbox";
//                                    domCon.create(cbox[i], {name: option.name, innerHTML: option.label, value: option.value}, this.cbContainer);
//                                    domCon.place("<b>'"+option.name+"' </b>",cbox[i],"before");
//                                    domCon.place("<br>",cbox[i],"after");
                                }
                                if (this.data.hasOwnProperty("help"))
                                {
                                   this.help.innerHTML = this.data["help"];
                                } 
                                else 
                                {
                                   this.help.innerHTML = "";
                                }
                            }
//                            if (this.data.hasOwnProperty("help"))
//                            {
//                                 this.help.innerHTML = this.data["help"];
//                            }
//                             else
//                             {
//                                 this.help.innerHTML = "";
//                             }
//                            var selectInputId = this.id + "selectinput";
//                            domAttr.set(this.label, "for", checkboxGroupId);
//                            domAttr.set(this.select, "id", checkboxGroupId);
//                            domAttr.set(this.select, "name", checkboxGroupId);
//                            on(this.cbContainer, 'change', this.AvailWhenHandler.bind(this));
                        },
                        HandleActionButton: function (event)
                        {

                        },
                        GetValue: function ()
                        {
//                            console.log(this.parentForm.GetEntryForm().checkbox_option[0].value);
                            var tf1 = false;
                            var cboxes = "";
                            for (var i = 0; i < this.data["options"].length; ++i)
                            {  
//                                console.log(this.data["name"]);
//                                console.log(this.parentForm.GetEntryForm().dude[i]);
                                var option = this.data["options"][i];
                                if(this.parentForm.GetEntryForm()[this.data["name"]][i].checked)
                                {
//                                        return this.parentForm.GetEntryForm().checkbox_option[i].value;
                                    if(this.data["layout"] === "horizontal")
                                    {
                                        cboxes += option.name + ", ";
                                        tf1 = true;
                                    }
                                    else if(this.data["layout"] === "vertical")
                                    {
                                        cboxes += option.name + ", " + "<br>";
                                    }
                                }
                            }
                            if(tf1 === true)
                            {
                                cboxes = cboxes.substring(0,cboxes.length - 2);
                            }
                            else if(tf1 === false)
                            {
                               cboxes = cboxes.substring(0, cboxes.length - 6);
//                               cboxes = cboxes.replace(/<br>/g,"");
                            }
                            return cboxes;
//                            var cboxes = "";
//                            var numIndex = this.select.selectedIndex;
//                            for (var i = 0; i < this.data["options"].length; ++i)
//                            {  
//                                if(cbox[i].checked)
//                                {
//                                   cboxes += cbox[i].name + ", ";
//                                }
//                            }
//                           return cboxes;
                        },
                        SetValue: function (index)
                        {
                            // Resets the selectedIndex back to the first option.
//                            if (index.trim().length === 0) {
//                                var x = this.cbContainer;
//                                cbox[parseInt(index)].checked = true;
                            if(index.trim().length !== 0)
                            {
                                this.checkboxGroup_readonly.innerHTML = index;
                            }
                            if(index.trim().length === 0)
                            {
                                for(var i = 0; i < this.data["options"].length; i++)
                                {
//                                        this.state_readonly.innerHTML = index;
                                        this.parentForm.GetEntryForm()[this.data["name"]][i].checked = false;
                                        this.checkboxGroup_readonly.innerHTML = "";
                                }
                            }   
//                             If the index received is a number, select the numeric index.
                            else if (parseInt(index) >= 0) 
                            {
                                if (index < this.data["options"].length)
                                {
                                    this.parentForm.GetEntryForm()[this.data["name"]][index].checked = true;
                                }
                            }                      
                            // If the index received is a text value, select the index with the given value.
                            else {
                                for (var numIndex in this.data["options"]) {
                                    if (index === this.data["options"][numIndex].name) {
                                        this.parentForm.GetEntryForm()[this.data["name"]][numIndex].checked = true;
                                    }
                                }
                            }
                        }
                        ,
                        Disable: function () 
                        {
                            for (var i = 0; i < this.data["options"].length; ++i)
                            {
                               this.parentForm.GetEntryForm()[this.data["name"]][i].disabled = this.disableBoolean;
                            }
//                            this.cbContainer.disabled = this.disableBoolean;
                        }
                    }
            );
        }
);

//commented out JSON file stuff          
//      {
//            "componentType":"checkboxGroup",
//            "name":"checkboxGroup",
//            "label":"Select an option",
//            "layout":"vertical",
//            "options": [
//                {"name":"checkbox_option1", "label":"Checkbox Label 1", "value":"checkbox1", "symbol":"sl_comms icon requests"},
//                {"name":"checkbox_option2", "label":"Checkbox Label 2", "value":"checkbox2", "symbol":""},
//                {"name":"checkbox_option3", "label":"Checkbox Label 3", "value":"checkbox3", "symbol":""}
//            ]
//        },
//        {
//            "componentType":"checkboxGroup",
//            "name":"checkboxGroup",
//            "label":"Select an option",
//            "layout":"vertical",
//            "options": [
//                {"name":"View Only", "label":"view only", "value":"view only", "symbol":"sl_priv icon view"},
//                {"name":"Edit", "label":"edit", "value":"edit", "symbol":"sl_priv icon edit"},
//                {"name":"Owner", "label":"owner", "value":"owner", "symbol":"sl_priv icon owner"},
//                {"name":"None", "label":"none", "value":"none", "symbol":"sl_priv icon none"},
//                {"name":"Limited Public Access", "label":"limited public access", "value":"limited public access", "symbol":"sl_priv icon lpa"}
//            ]
//        },
//        {
//            "componentType":"checkboxGroup",
//            "name":"checkboxGroup",
//            "label":"Select an option",
//            "layout":"vertical",
//            "options": [
//                {"name":"Point", "label":"point", "value":"point", "symbol":"layer-icon point"},
//                {"name":"Polygon", "label":"poly", "value":"poly", "symbol":"layer-icon poly"},
//                {"name":"Collection", "label":"collection", "value":"collection", "symbol":"layer-icon collection"},
//                {"name":"Line", "label":"line", "value":"line", "symbol":"layer-icon line"},
//                {"name":"Raster", "label":"raster", "value":"raster", "symbol":"layer-icon raster"},
//                {"name":"WMS", "label":"wms", "value":"wms", "symbol":"layer-icon wms"}
//
//            ]
//         }
//