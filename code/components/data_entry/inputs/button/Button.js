define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "sl_components_open/data_entry/inputs/InputClass",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./Button.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                InputClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            'Button',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                /**
                                 * Part of the Open SimpleLayers project
                                 * https://bitbucket.org/simplelayers_dev/open.simplelayers
                                 * 
                                 **/
                                data: null,
                                name: null,
                                availWhenToggle: true,
                                templateString: template,
                                baseClass: 'button flex-container flex-item row',
                                constructor: function (data)
                                {
                                    this.data = data;
                                },
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("label"))
                                    {
                                        this.label.innerHTML = this.data["label"];
                                    } else
                                    {
                                        this.label.innerHTML = "";
                                    }
                                    if (this.data.hasOwnProperty("singlebutton"))
                                    {
                                        this.singlebutton.innerHTML = this.data["singlebutton"];
                                    } else
                                    {
                                        this.singlebutton.innerHTML = "";
                                    }

                                    var buttonID = this.id + "button";
                                    domAttr.set(this.label, "for", buttonID);
                                    domAttr.set(this.singlebutton, "id", buttonID);
                                    domAttr.set(this.singlebutton, "name", buttonID);
                                    on(this.singlebutton, 'click', this.ButtonAvailWhenHandler.bind(this));
                                },
                                HandleActionButton: function (event)
                                {

                                },
                                // GetValue returns "visible" or "hidden" for availWhen in order to affect any widgets depending on the button.
                                // GetValue does not refer to the button's visibility.
                                GetValue: function () {
                                    if (this.availWhenToggle) {
                                        return "visible";
                                    }
                                    return "hidden";
                                },
                                SetValue: function (val) {
                                    // Toggles availWhen and the button's visibilty back to its original state if val is empty.
                                    if (val.trim().length === 0) {
                                        this.availWhenToggle = true;
                                        if (this.data.hasOwnProperty("visible"))
                                        {
                                            this.visible = this.data["visible"];
                                        } else {
                                            this.visible = true;
                                        }
                                        this.Toggle(this.visible);
                                    } else if (val === "visible") {
                                        this.availWhenToggle = true;
                                    } else if (val === "hidden") {
                                        this.availWhenToggle = false;
                                    }
                                },
                                Disable: function () {
                                    this.singlebutton.disabled = this.disableBoolean;
                                }
                            }
                    );
        }
);