define(["dojo/_base/declare",
                "dojo/on",
                "dojo/dom-attr",
                "dijit/_WidgetBase",
                "dijit/_TemplatedMixin",
                "dijit/_WidgetsInTemplateMixin",
                "sl_components_open/data_entry/inputs/InputClass",
                "dojo/text!./URLInput.tpl.html"],
            function (declare,
                              on,
                              domAttr,
                              _WidgetBase,
                              _TemplatedMixin,
                              _WidgetsInTemplateMixin,
                              InputClass,
                              template) {
                return declare("URLInput", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        data: null,
                        name: null,
                        templateString: template,
                        baseClass:'url_input flex-container flex-item row',
                        postCreate: function()
                        {                            
                            if(this.data.hasOwnProperty("placeholder"))
                            {
                                domAttr.set(this.urlText, "placeholder", this.data["placeholder"]);
                            }
                            else
                            {
                                domAttr.set(this.urlText, "placeholder", "");
                            }
                            
                            if(this.data.hasOwnProperty("urlButton"))
                            {
                                this.urlButton.value = this.data["urlButton"];
                            }
                            else
                            {
                                this.urlButton.value = "";
                            }
                            
                            if (this.data.hasOwnProperty("help"))
                            {
                                    this.help.innerHTML = this.data["help"];
                            }
                            else
                            {
                                    this.help.innerHTML = "";
                            }
                            
                            this.Toggle(this.visible);
                            
                            var urlInputId = this.id + 'urlinput';
                            domAttr.set(this.label, "for", urlInputId);
                            domAttr.set(this.urlText, "id", urlInputId);
                            
                            on(this.urlText, 'keyup', this.AvailWhenHandler.bind(this));
                            on(this.urlButton, 'click', this.HandleActionButton.bind(this));
                        },
                        HandleActionButton: function(event)
                        {
                            window.open(this.GetValue(), "_blank");
                        },
                        SetValue: function(toSet)
                        {
                            this.urlText_readonly.innerHTML = toSet; 
                            this.urlText_readonly.href = toSet;
                            this.urlText.value = toSet;
                        },
                        GetValue: function()
                        {
                            return this.urlText.value;
                        },
                        Disable: function () { 
                            this.urlText.readOnly = this.disableBoolean;
                        }
                    });
          });