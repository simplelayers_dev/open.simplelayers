define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/text!./RadioGroup.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                domClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                template)
        {
            return declare('RadioGroup', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                    {
                        /**
                         * Part of the Open SimpleLayers project
                         * https://bitbucket.org/simplelayers_dev/open.simplelayers
                         * 
                         **/
                        data: null,
                        name: null,
                        templateString: template,
                        baseClass: 'RadioGroup',
                        postCreate: function ()
                        {
                            var radioGroupId = this.id + "radiogroup";
                            if (this.data.hasOwnProperty("options"))
                            {
                                for (var i = 0; i < this.data["options"].length; ++i)
                                {
                                    var option = this.data["options"][i];
                                    if (typeof option.value === 'undefined')
                                    {
                                        option.value = option.name;
                                    }
                                    // var entry = domCon.create("div",{},this.radiogroup);
                                    // var input = domCon.create("input", {type: "radio", name: "radio_option", id: radioGroupId, value: option.value}, this.radiogroup);
                                    // var label = domCon.create("label",{innerHTML: option.label, for:radioGroupId}, this.radiogroup);
                                    if (this.data["alignment"] === 'horizontal')
                                    {
                                        var node = domCon.toDom('<label class="radio-inline" for="'+radioGroupId+i+'"><input type="radio" name="radio_option" id="'+radioGroupId+i+'"value="'+option.value+'">'+option.label+'</label>');
                                        domCon.place(node,this.radiogroup,"last");
                                    }
                                    else
                                    {
                                        var node = domCon.toDom('<div class="radio"><label for="'+radioGroupId+i+'"><input type="radio" name="radio_option" id="'+radioGroupId+i+'"value="'+option.value+'">'+option.label+'</label></div>');
                                        domCon.place(node,this.radiogroup,"last");
                                    }
                              }
                            }
                            
                            domAttr.set(this.label, "for", radioGroupId);
                            domAttr.set(this.radiogroup, "id", radioGroupId);
                            domAttr.set(this.radiogroup, "name", radioGroupId);
                            
                            on(this.radiogroup, 'change', this.AvailWhenHandler.bind(this));
                        },
                        HandleActionButton: function (event)
                        {

                        },
                        GetValue: function ()
                        {
                            return this.parentForm.GetEntryForm().radio_option.value;
                        },
                        SetValue: function (index)
                        {
                            // Resets the selectedIndex back to the first option.
                            if (index.trim().length === 0) {
                                this.parentForm.GetEntryForm().radio_option.value = this.parentForm.GetEntryForm().radio_option[0].value;
                            }

                            // If the index received is a number, select the numeric index.
                            else if (parseInt(index)) {
                                if (index < this.parentForm.GetEntryForm().radio_option.length) {
                                    this.parentForm.GetEntryForm().radio_option.value = this.parentForm.GetEntryForm().radio_option[index].value;
                                }
                            }

                            // If the index received is a text value, select the index with the given value.
                            else {
                                this.parentForm.GetEntryForm().radio_option.value = index;
                            }
                        },
                        Disable: function () {
                            this.radiogroup.disabled = this.disableBoolean;
                        }
                    }
            );
        }
);