define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_components_open/data_entry/inputs/InputClass",
    "dojo/text!./Text.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                InputClass,
                template)
        {
            return declare
                    (
                            /**
                             * Part of the Open SimpleLayers project
                             * https://bitbucket.org/simplelayers_dev/open.simplelayers
                             * 
                             **/
                            'Text',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                data: null,
                                name: null,
                                templateString: template,
                                baseClass: 'text_input flex-container flex-item row',
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("placeholder"))
                                    {
                                        domAttr.set(this.textinput, "placeholder", this.data["placeholder"]);
                                    } else
                                    {
                                        domAttr.set(this.textinput, "placeholder", "");
                                    }
                                    if (this.data.hasOwnProperty("help"))
                                    {
                                        this.help.innerHTML = this.data["help"];
                                    } else
                                    {
                                        this.help.innerHTML = "";
                                    }

                                    var textInputId = this.id + 'textinput';
                                    domAttr.set(this.textinput, "id", textInputId);
                                    domAttr.set(this.label, "for", textInputId);
                                    on(this.textinput, 'keyup', this.AvailWhenHandler.bind(this));
                                },
                                HandleActionButton: function (event)
                                {

                                },
                                GetValue: function ()
                                {
                                    return this.textinput.value;
                                },
                                SetValue: function (toSet)
                                {
                                    this.textinput_readonly.innerHTML = toSet;
                                    this.textinput.value = toSet;
                                },
                                Disable: function () {
                                    this.textinput.readOnly = this.disableBoolean;
                                }
                            }
                    );
        }
);