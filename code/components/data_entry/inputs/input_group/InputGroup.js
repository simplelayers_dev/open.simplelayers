define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "sl_components_open/data_entry/inputs/InputClass",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./InputGroup.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                InputClass,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                template)
        {
            return declare
                    (
                            'InputGroup',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, InputClass],
                            {
                                /**
                                 * Part of the Open SimpleLayers project
                                 * https://bitbucket.org/simplelayers_dev/open.simplelayers
                                 * 
                                 **/
                                data: null,
                                name: null,
                                inputs: null,
                                templateString: template,
                                baseClass: "input_group flex-container flex-item col",
                                constructor: function (data)
                                {
                                    this.data = data;
                                },
                                postCreate: function ()
                                {
                                    if (this.data.hasOwnProperty("items"))
                                    {
                                        inputs = this.data["items"];
                                    } else
                                    {
                                        inputs = [];
                                    }
                                },
                                
                                // returns JSON data for inputs in Input Group
                                GetInputs: function ()
                                {
                                    return inputs;
                                }
                            }
                    );
        });