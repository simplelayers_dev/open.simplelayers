define(["dojo/_base/declare", "dojo/on", "dojo/dom-construct",
    "dojo/dom-attr", "dojo/dom-class", "dijit/_WidgetBase",
    "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "dijit/registry",
    "dojo/dom", "sl_modules_open/env", "sl_classes_open/StateManager",
    "dojo/text!./DataEntry.tpl.html"],
        function (declare, on, domCon, domAttr, domClass, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, registry, dom, Env, StateManager, template) {
            return declare('DataEntry', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, StateManager], {
                /**
                 * Part of the Open SimpleLayers project
                 * https://bitbucket.org/simplelayers_dev/open.simplelayers
                 * 
                 **/
                data: null,
                inputs: null,
                disableToggle: false,
                templateString: template,
                baseClass: 'data_entry flex-container flex-item',
                Start: function (JSONObj) {
                    this.data = JSONObj;

                    Env.RequireBootStrap();
                    Env.AddCSS_postBootStrap([baseURL + "flex_box.css", baseURL + "open_sl.css"]);
                    
                    var obj = this.data;
                    this.inputs = {};
                    if (obj.hasOwnProperty("formName")) {
                        var name = obj['formName'];
                        this.formName.innerHTML = name;
                    } else {
                        domClass.toggle(this.formName, 'hidden', true);
                    }

                    if (obj.hasOwnProperty("widgets")) {
                        var widgetsArray = obj['widgets'];
                        var widgetJSON;
                        for (var i = 0; i < widgetsArray.length; i++) {
                            widgetJSON = widgetsArray[i];

                            if (widgetJSON.hasOwnProperty("componentType")) {
                                this.addInput(widgetJSON);
                            } else if (widgetJSON.hasOwnProperty("type")) {
                                this.addInput(widgetJSON);
                            }
                        }
                    }
                    this.itemChanged();

                    if (obj["submit"] !== "hidden") {
                        require(["sl_components_open/data_entry/inputs/button/Button"], (function (Button) {
                            var submitButton = new Button({
                                singlebutton: "Submit"
                            });
                            submitButton.placeAt(this.fieldSet);
                            on(submitButton, 'click', this.SubmitHandler.bind(this));
                        }).bind(this));
                    }

                    // StateManager
                    this.SetMachine(this.domNode);
                    this.SetPrefix("data_entry");
                    this.SetStates(["readonly", "edit"]);
                    var initialState = domAttr.get(this.domNode, "data_entry-startmode");
                    if (initialState === "edit") {
                        this.SetState(initialState);
                    } else {
                        this.SetState("readonly");
                    }
                },
                Reset: function () {
                    for (var inputName in this.inputs) {
                        this.inputs[inputName].Reset();
                    }
                },
                HandleActionButton: function (event) {

                },
                addInput: function (widgetJSONObject, optTarget) {
                    if (typeof optTarget === "undefined") {
                        optTarget = this.fieldSet;
                    }

                    var type = widgetJSONObject.hasOwnProperty('type') ? widgetJSONObject.type : null;
                    if (type === null) {
                        if (widgetJSONObject.hasOwnProperty("componentType")) {
                            type = widgetJSONObject.componentType;
                        }
                    }

                    if (type !== null) {
                        var widget;
                        switch (type) {
                            case "input":
                            case "txt":
                            case "textInput":
                                require(["sl_components_open/data_entry/inputs/text/Text"], (function (TextInput) {
                                    widget = new TextInput(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "btn":
                            case "button":
                                require(["sl_components_open/data_entry/inputs/button/Button"], (function (Button) {
                                    widget = new Button(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "sel":
                            case "selectBasic":
                                require(["sl_components_open/data_entry/inputs/select/Select"], (function (SelectBasic) {
                                    widget = new SelectBasic(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "cbgrp":
                            case "checkboxGroup":
                                require(["sl_components_open/data_entry/inputs/checkbox_group/CheckboxGroup"], (function (CheckboxGroup) {
                                    widget = new CheckboxGroup(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "btn_drp":
                            case "buttonDrop":
                                require(["sl_components_open/data_entry/inputs/button_dropdown/Button_Dropdown"], (function (ButtonDrop) {
                                    widget = new ButtonDrop(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "pwd":
                            case "passwordInput":
                                require(["sl_components_open/data_entry/inputs/pw_password/PWPassword"], (function (PWPassword) {
                                    widget = new PWPassword(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                                
                            case "image_input":
                                require(["sl_components_open/data_entry/inputs/image_input/ImageInput"], (function (ImageInput) {
                                    widget = new ImageInput(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "url":
                            case "urlInput":
                                require(["sl_components_open/url_input/URLInput"], (function (URLInput) {
                                    widget = new URLInput(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "srch":
                            case "searchInput":
                                require(["sl_components_open/data_entry/inputs/search_input/SearchInput"], (function (SearchInput) {
                                    widget = new SearchInput(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "xpend":
                            case "xPendedText":
                                require(["sl_components_open/data_entry/inputs/xpend_text/XPend/XPendedText"], (function (XPendedText) {
                                    widget = new XPendedText(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "loc8":
                            case "locationButton":
                                require(["sl_components_open/data_entry/inputs/location_button/LocationButton"], (function (LocationButton) {
                                    widget = new LocationButton(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "grp":
                            case "inputGroup":
                                require(["sl_components_open/data_entry/inputs/input_group/InputGroup"], (function (InputGroup) {
                                    widget = new InputGroup(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "cb":
                            case "checkbox":
                                require(["sl_components_open/data_entry/inputs/checkbox/Checkbox"], (function (Checkbox) {
                                    widget = new Checkbox(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "sw":
                            case "stateWidget":
                                require(["sl_components_open/data_entry/inputs/state_widget/StateWidget"], (function (StateWidget) {
                                    widget = new StateWidget(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            case "rad":
                            case "radioGroup":
                                require(["sl_components_open/data_entry/inputs/radio_group/RadioGroup"], (function (RadioGroup) {
                                    widget = new RadioGroup(widgetJSONObject);
                                    this.WidgetCreation(widget, optTarget, type);
                                }).bind(this));
                                break;
                            default:
                                return;
                        }
                    }
                },
                WidgetCreation: function (widget, optTarget, type) {
                    widget.placeAt(optTarget);
                    if (widget.data.hasOwnProperty("name"))
                    {
                        this.inputs[widget.data.name] = widget;
                    }
                    if (typeof widget.SetParentForm === 'function')
                    {
                        widget.SetParentForm(this);
                    }
                    if (typeof widget.SetCommonAttributes === 'function')
                    {
                        // Moved function SetCommonAttributes() to run after widget creation and not when the widget is being initialized.
                        // This is necessary as the IsAvailable() function in InputClass.js calls
                        // functions SetParentForm(form) and getInput(inputName), which can only happen after the widget has been created.
                        widget.SetCommonAttributes();
                    }

                    // widget placement for inputGroup
                    if (type === "inputGroup" || "grp") {
                        if (typeof widget.GetInputs === 'function')
                        {
                            var inputGroup = widget.GetInputs();
                            for (var i = 0; i < inputGroup.length; i++) {
                                this.addInput(inputGroup[i], widget.domNode);
                            }
                        }
                    }
                },
                getInput: function (inputName) {
                    return this.inputs[inputName];
                },
                // updates visibility on widgets based on availWhen
                itemChanged: function (event) {
                    for (var inputName in this.inputs) {
                        var input = this.inputs[inputName];

                        // updates visibility on current widget based on if the widget is in an input group and if the parent widget is visible
                        var parentWidget = registry.getEnclosingWidget(input.domNode.parentNode);
                        var parentWidgetType = parentWidget.data["componentType"] || parentWidget.data["type"];
                        if (parentWidget && (parentWidgetType === "inputGroup" || parentWidgetType === "grp")) {
                            if (typeof input.Toggle === 'function') {
                                input.Toggle(parentWidget.visible);
                            }
                        }

                        if (typeof input.IsAvailable === 'function')
                        {
                            input.IsAvailable();
                        }

                        this.onChange({data: this.getValues()});
                    }
                },
                onChange: function (event) {
                    //extension point
                },
                // returns all named visible widgets
                getValues: function () {
                    var output = {};

                    for (var inputName in this.inputs) {
                        var input = this.inputs[inputName];
                        var type = input.data.hasOwnProperty('type') ? 'type' : 'componentType';
                        if (!(input.data[type] === "inputGroup" || input.data[type] === "grp") && typeof input.GetValueIfVis === 'function')
                        {
                            if (input.GetValueIfVis() !== null) {
                                output[inputName] = input;
                            }
                        }
                    }
                    return output;
                },
                SetFields: function (fieldInfo) {
                    for (var field in fieldInfo) {
                        if (fieldInfo.hasOwnProperty(field)) {
                            if (this.inputs.hasOwnProperty(field)) {
                                if (typeof fieldInfo[field] === 'object') {
                                    if (fieldInfo[field].hasOwnProperty('value')) {
                                        this.inputs[field].SetValue(fieldInfo[field].value);
                                    }
                                } else {
                                    this.inputs[field].SetValue(fieldInfo[field]);
                                }
                            }
                        }
                    }
                    this.itemChanged();
                },
                ClearForm: function () {
                    {
                        for (var name in this.inputs) {
                            if (typeof this.inputs[name].SetValue === 'function') {
                                this.inputs[name].SetValue("");
                            }
                        }
                        this.itemChanged();
                    }
                },
                SetForm: function (JSONObj)
                {
                    this.ClearForm();
                    var inputs = {};
                    for (var name in JSONObj) {
                        inputs = this.getValues();
                        var value = JSONObj[name];
                        if (typeof inputs[name] !== 'undefined' && typeof inputs[name].SetValue === 'function') {
                            inputs[name].SetValue(value);
                            this.itemChanged();
                        }
                    }
                },
                DisableToggle: function () {
                    this.disableToggle = !this.disableToggle;

                    if (this.disableToggle) {
                        for (var inputName in this.inputs) {
                            if (this.inputs[inputName].disableBoolean === true) {
                                this.inputs[inputName].domNode.style.backgroundColor = "#00cccc";
                            }
                        }
                    } else {
                        for (var inputName in this.inputs) {
                            if (this.inputs[inputName].disableBoolean === true) {
                                this.inputs[inputName].domNode.style.backgroundColor = "#eee";
                            }
                        }
                    }
                },
                ReadOnlyToggle: function () {
                    if (this.GetCurrentState() === "readonly") {
                        this.SetState("edit");
                    } else {
                        // This updates the form although it may seem redundant.
                        // Sets the value to match whatever is inputted before toggling to readonly mode so the two divs are the same.
                        var inputs = this.getValues();
                        for (var name in inputs) {
                            var input = inputs[name];
                            // GetValue is not needed to be checked if it is a function because this.getValues() already checks for the function
                            if (typeof input.SetValue === 'function') {
                                input.SetValue(input.GetValue());
                            }
                        }

                        this.SetState("readonly");
                    }
                },
                GetEntryForm: function () {
                    return this.entryForm;
                },
                SubmitHandler: function () {
                    this.itemChanged();
                    var inputs = this.getValues();

                    // gets the last item of the array
                    var lastItem = inputs[Object.keys(inputs)[Object.keys(inputs).length - 1]];

                    // defaultSpaces sets the number of spaces that preceed each of the inputs and their values
                    var defaultSpaces = 4;

                    var formatting = "";
                    for (var i = 0; i < defaultSpaces; i++) {
                        formatting += " ";
                    }

                    var outputString = "";

                    // checks if lastItem exists, determines if the size of inputs is greater than zero
                    if (lastItem) {
                        outputString += "{\n";
                        for (var name in inputs) {
                            if (typeof inputs[name].GetValue === 'function') {
                                outputString += formatting + '"' + name + '":';
                                if (typeof inputs[name].GetValue() === 'string') {
                                    outputString += '"' + inputs[name].GetValue() + '"';
                                } else {
                                    outputString += inputs[name].GetValue();
                                }
                                // if the loop reaches the end of the array, do not put a comma
                                if (name === lastItem.name) {
                                    outputString += '\n';
                                }
                                // else put a comma and continue looping through the array
                                else {
                                    outputString += ',\n';
                                }
                            }
                        }
                        outputString += "}";
                    }
                    this.onFormSubmit(outputString);
                    return outputString;
                },
                onFormSubmit: function (data) {
                    // Used as extension point for custom event, called 'formsubmit'.
                }
            });
        });