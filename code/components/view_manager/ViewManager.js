define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./ViewManager.tpl.html"],
        function (declare, on, dom, domCon, domClass, domAttr, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
            return declare('sl_components/view_manager/ViewManager', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                views: {},
                templateString: template,
                baseClass: 'view_manager flex-item flex-container',
                StartUp: function () {
                    var first = true;
                    var children = this.domNode.childNodes;
                    for (var i = 0, l = children.length; i < l; i++) {
                        var viewName = domAttr.get(children[i], "data-view");
                        if (viewName) {
                            this.views[viewName] = children[i];
                            if (first) {
                                first = false;
                                continue;
                            }
                            domClass.add(children[i], "hidden");
                        }
                    }
                },
                GoToView: function (viewName) {
                    for (var view in this.views) {
                        if (view == viewName) {
                            domClass.remove(this.views[view], "hidden");
                            continue;
                        }
                        domClass.add(this.views[view], "hidden");
                    }
                }
            });
        });
