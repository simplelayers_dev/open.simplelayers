define(['dstore/Memory', "dojo/query", 'dojo/topic', 'dgrid/Selection', 'dstore/Trackable', 'dgrid/OnDemandList', 'dojo/_base/declare',
    'dgrid/extensions/DijitRegistry', "dojo/on", "dojo/dom-geometry", "dojo/dom-style", "dojo/dom-construct", "dojo/dom-class", "dijit/Dialog",
    "dojo/dom-attr", "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin",
    "sl_modules_open/env",
    "sl_modules_open/sl_api/Navigation",
    "sl_components_open/criteria_item/CriteriaItem", "dojo/dom", "sl_modules_open/sl_api/Layers", "sl_modules_open/layerImage", "sl_modules_open/sl_URL",
    "sl_modules_open/sl_api/Maps",
    "sl_components_open/controls/MapPanel.ctrl",
    "sl_modules_open/StateUtils",
    "dojo/text!./SearchCriteria.tpl.html"],
        function (Memory, query, topic, Selection, Trackable, List, declare, Registry, on, domGeo, domStyle, domCon, domClass, Dialog, domAttr, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, Env, Navigation, CriteriaItem, dom, Layers, LayerImage, slURL, slMaps, MapPanelCtrl, StateUtils, template) {

            return declare('sl_components_open/search_criteria/SearchCriteria', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                critlist: null,
                criteriaItemList: [],
                parent: null,
                templateString: template,
                baseClass: 'SearchCriteria',
                _searchableKeys: null,
                _bbox: null,
                _ready: false,
                _ids: [],
                _unpagedIds: [],
                _layerId: null,
                _mapSize: null,
                partialLoad: false,
                _unpaged: null,
                _paged: null,
                _map: null,
                _zooming: true,
                _bboxMode: "layer",
                _selectedBboxMode: "layer",
                _pageReady: false,
                _allReady: false,
                _navControl: null,
                _navHooked: false,
                _initialMapHeight: null,
                _mapPanel: null,
                _selections: null,
                _reqMemoryButtons: null,
                _allLayerLookup: null,
                constructor: function (args) {
                    if (args.hasOwnProperty('searchableKeys'))
                        this._searchableKeys = args.searchableKeys;
                },
                topicHandlers: [],
                destroy: function () {
                    for (var i in this.topicHandlers) {
                        this.topicHandlers[i].remove();
                    }
                    return this.inherited(arguments);
                },
                Setup: function (searchableKeys, searchTip, layerId) {

                    if (this._layerId == layerId)
                        return;
                    this._layerId = layerId;
                    if (this._layerId == '*') {
                        this._allLayerLookup = searchableKeys;
                        var sks = [];
                        var keys = Object.keys(searchableKeys);
                        for (var k = 0; k < keys.length; k++) {
                            var key = keys[k];
                            sks.push({'name': key, 'requirement': 'any', 'display': key});
                        }

                        this._searchableKeys = sks;
                    } else {
                        this._allLayerLookup = null;
                        this._searchableKeys = searchableKeys;
                    }
                    domClass.toggle(this.map, 'invis', true);
                    this._layerId = layerId;
                    this.partialLoad = null;
                    this.criteriaItemList = [];
                    domCon.empty(this.criteriaContainer);
                    if ([null, undefined].indexOf(searchTip) > -1) {
                        searchTip = "";
                    }
                    this._mapPanel.SetInstructions(searchTip);
                    this._mapPanel.SetInfoHeading('Search Tip:')

                    this.ListCriteria();
                    this.UpdateList();
                    this._selections = [];
                    this._bbox = null;
                    //Layers.GetMemory().Reset();
                    //this.UpdateLayerImages(500,500);
                    //this.domNode.style.height= this.titleArea.offsetHeight;
                },
                postCreate: function () {

                    on(this.addCriteriaButton, 'click', this.HandleAddCriteriaButton.bind(this));
                    on(this.wholeLayer_btn, 'click', this.HandleNavButton.bind(this));
                    on(this.allResults_btn, 'click', this.HandleNavButton.bind(this));
                    on(this.currentResults_btn, 'click', this.HandleNavButton.bind(this));
                    on(this.M_restore_btn, 'click', this.HandleMemoryButton.bind(this));
                    on(this.M_add_btn, 'click', this.HandleMemoryButton.bind(this));
                    on(this.M_sub_btn, 'click', this.HandleMemoryButton.bind(this));
                    on(this.M_clear_btn, 'click', this.HandleMemoryButton.bind(this));
                    this._reqMemoryButtons = [this.M_restore_btn, this.M_sub_btn, this.M_clear_btn];
                    on(this.domNode, 'dblclick', function (event) {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                    });
                    //on(this.cancelButton, 'click', this.HandleCancelButton.bind(this));
                    //on(this.searchButton, 'click', this.HandleSearchButton.bind(this));
                    if (this._searchableKeys !== null) {
                        this.Setup(searchableKeys);
                        this._ready = true;
                    }
                    domClass.toggle(this.map, 'invis', true);
                    this._map = L.mapquest.map(this.map, {
                        center: [37.7749, -122.4194],
                        layers: L.mapquest.tileLayer('hybrid'),
                        zoom: 12,
                    });
                    //this._map.addControl(L.mapquest.control());
                    this._navControl = L.mapquest.navigationControl();
                    this._map.removeControl(this._map.zoomControl);
                    this._mapPanel = new MapPanelCtrl();
                    this._mapPanel.AddToMap(this._map);
                    this._mapPanel.AddToPanel(this.panelContent);
                    this._mapPanel.AddToTitle(this.navOptions);
                    /*on(navButton,'click',(function(event) {
                     event.stopPropagation();
                     this.ResetImages(true);
                     this.UpdateLayerImages());
                     }).bind(this));
                     */
                    this._map.addControl(this._navControl);
                    this._map.on('dragend', (function (e) {
                        if (e.hard) {
                            // moved by bounds
                        } else {
                            this.HandleZoomEnd(e);
                        }
                    }).bind(this));
                    this._map.on('zoomend', (function (e) {
                        if (e.hard) {
                            // moved by bounds
                        } else {
                            this.HandleZoomEnd(e);
                        }
                    }).bind(this));
                    this._map.on('zoomstart', this.HandleZoomStart.bind(this));
                    this._map.on('dragstart', this.HandleZoomStart.bind(this));
                    setTimeout(this.RefreshMapSize.bind(this), 20);
                    var imgBounds = slMaps.BboxToBounds('-180,-90,180,90');
                    this._unpaged = L.imageOverlay(slURL.GetEmptyImgURL(), imgBounds);
                    this._paged = L.imageOverlay(slURL.GetEmptyImgURL(), imgBounds);
                    this._selFeatureLyr = L.imageOverlay(slURL.GetEmptyImgURL(), imgBounds);
                    this._unpaged.addTo(this._map);
                    this._paged.addTo(this._map);
                    this._selFeatureLyr.addTo(this._map);
                    //
//var overlayPane = $(this.map).find('.leaflet-overlay-pane')[0];
                    //domCon.place(this.staticMap_img,overlayPane);
                    //domCon.place(this.staticMap2_img,overlayPane);

                    domClass.toggle(this.map, 'invis', true);
                    this.topicHandlers.push(topic.subscribe('criteria_item/destroy', this.DestroyItem.bind(this)));
                    this.topicHandlers.push(topic.subscribe('features/zoom', this.HandleFeatureZoom.bind(this)));
                    this.topicHandlers.push(topic.subscribe('features/selchange', this.HandleFeatureSelChange.bind(this)));
                    this.topicHandlers.push(topic.subscribe('geocalculator/updated', this.HandleMemoryUpdates.bind(this)));
                    on(window, 'resize', this.RefreshMapSize.bind(this));
                },
                HandleMemoryUpdates: function (event) {
                    switch (event.calculator.HasMemoryLayer()) {
                        case true:
                            for (var i in this._reqMemoryButtons) {
                                StateUtils.Enable(this._reqMemoryButtons[i]);
                            }
                            break;
                        case false:
                            for (var i in this._reqMemoryButtons) {
                                StateUtils.Disable(this._reqMemoryButtons[i]);
                            }
                            break;
                    }
                },
                ResetMapLimit: function () {
                    this._bbox = null;
                },
                HandleMemoryButton: function (event) {
                    var button = event.currentTarget;
                    var memory = Layers.GetMemory();
                    var data = null;
                    switch (button) {
                        case this.M_restore_btn:
                            this.RestoreMemory();
                            break;
                        case this.M_add_btn:
                            if (!memory.HasMemoryLayer()) {
                                memory.SetLayer(this._layerId);
                            }
                            memory.Add(this._selections);
                            break;
                        case this.M_sub_btn:
                            memory.Remove(this._selections);
                            break;
                        case this.M_clear_btn:
                            memory.Clear();
                            break;
                    }
                    data = memory.Restore();
                    console.log(data);
                },
                HandleFeatureSelChange: function (event) {
                    this._selections = event.features;
                    this.UpdateLayerImages(this._selFeatureLyr);
                },
                HandleFeatureZoom: function (event) {
                    this._selections = (event.feature.constructor === Array) ? event.feature : event.feature.split(',');
                    Navigation.ZoomToFeature(event.layerId,
                            event.feature,
                            this.HandleZoomToFeature.bind(this));
                },
                HandleZoomToFeature: function (event) {
                    this.HandleZoomStart();
                    var bbox = event.bbox.split(',');
                    this._map.fitBounds([[bbox[1], bbox[0]], [bbox[3], bbox[2]]]);
                    this.HandleZoomEnd();
                },
                ZoomToBbox: function (bbox) {
                    this.HandleZoomStart();
                    if (bbox.constructor !== Array) {
                        bbox = bbox.split(',');
                    }
                    console.log(bbox);
                    this._map.fitBounds([[bbox[1], bbox[0]], [bbox[3], bbox[2]]]);
                    this.HandleZoomEnd();
                    this.Updating();
                },
                HandleZoomStart: function () {

                    this.ResetImages(true);
                    this._zooming = true;
                    this._bboxMode = "map";
                },
                HandleZoomEnd: function () {
                    this.UpdateLayerImages();
                    this._zooming = false;
                },
                Updating: function () {
                    domClass.toggle(this.map, 'invis', false);
                },
                RefreshMapSize: function () {
                    this.map.style.height = this.staticMapContainer.offsetHeight + 'px';
                    this.map.style.width = this.staticMapContainer.offsetWidth + 'px';
                    this._mapSize = {"width": this.staticMapContainer.offsetWidth, "height": this.staticMapContainer.offsetHeight};
                    this._map.invalidateSize(true);
                    this._mapPanel.domNode.style.height = this.staticMapContainer.offsetHeight + 'px';
                },
                RestoreMemory: function () {
                    this.SetMemoryCriteria();
                    topic.publish('features/multisearch/requested');
                },
                HandleAddCriteriaButton: function () {
                    this.AddCriterion();
                },
                SetMemoryCriteria: function () {
                    this.ClearAllItems();
                    this.AddCriterion();
                    var criterion = this.criteriaItemList[0];
                    criterion.SetData('memory', 'includes records', "{memory}", '-.1', 'ft');
                },
                AddCriterion: function () {
                    var keys = [];
                    if (Layers.GetMemory().count > 0) {
                        var memoryKey = {'display': 'memory', 'name': 'memory', 'searchable': true, 'type': 'memory', 'z': -(this._searchableKeys.length + 2), "requirement": "memory"};
                        keys.push(memoryKey);
                    }
                    keys = keys.concat(this._searchableKeys);
                    var item = new CriteriaItem((this.criteriaItemList.length === 0 ? true : false), keys);
                    this.domNode.style.height =
                            this.criteriaItemList.push(item);
                    this.UpdateList();
                },
                DestroyItem: function (event) {
                    for (var i = this.criteriaItemList.length - 1; i >= 0; i--) {
                        if (this.criteriaItemList[i] === event.target) {
                            this.criteriaItemList.splice(i, 1);
                        }
                    }
                    if (this.criteriaItemList.length > 0) {
                        this.criteriaItemList[0].IsFirst();
                    }

                    this.UpdateList();
                },
                ClearAllItems: function () {
                    this.criteriaItemList.splice(0); //, this.criteriaItemList.length);                      
                    this.UpdateList();
                },
                HandleCancelButton: function () {
                    this.dialog.hide();
                },
                UpdateList: function () {
                    setTimeout(this.RefreshMapSize.bind(this), 20);
                    this.critlist.refresh();
                    var lh = 6.5;
                    var oldHeight = this.criteriaContainer.style.height;
                    var mapHeight = this.staticMapContainer.offsetHeight;
                    if (mapHeight == 0) {
                        setTimeout(this.UpdateList.bind(this), 21);
                        return;
                    }
                    //this.domNode.style['min-height'] =  "Calc("+mapHeight+" - "+this.titleArea.offsetHeight +'px)';

                    this.criteriaContainer.style.height = "Calc(" + mapHeight + "px - " + this.titleArea.offsetHeight + 'px - 7em)'
                    this.criteriaContainer.firstChild.style.height = this.criteriaContainer.style.height; // (this.criteriaItemList.length * lh) + "em";
                    /*this.criteriaContainer.style.height = (this.criteriaItemList.length * lh) + "em + .5em";
                     this.criteriaContainer.firstChild.style.height = (this.criteriaItemList.length * lh) + "em";
                     
                     if((this.criteriaContainer.offsetHeight + this.titleArea.offsetHeight) > mapHeight) {
                     this.criteriaContaine.style.height = (mapHeight-this.titleArea.offsetHeight) +'px -1em';
                     this.criteriaContainer.firstChild.style.height = (mapHeight-this.titleArea.offsetHeight)+'px -1em';
                     }*/
                    topic.publish('search_criteria/resize', {target: this});
                    //topic.publish('search_criteria/resize', {target: this})

                },
                SelectHandler: function () {

                },
                HandleSearchButton: function () {
                    /* var data = [];
                     
                     for (var i = 0, l = this.criteriaItemList.length; i < l; i++) {
                     data.push(this.criteriaItemList[i].GetData());
                     }
                     
                     this.dialog.hide();
                     */
                    //this.parent.MultiCriteriaSearch(parseInt(this.recordCounter.value), data);
                },
                ListCriteria: function () {
                    domCon.destroy(this.critlist);
                    var TrackableMemory = declare([Memory, Trackable]);
                    var memory = new TrackableMemory({data: this.criteriaItemList});
                    //MyCritList = declare([List, Registry, Selection]);
                    MyCritList = declare([List, Registry]);
                    this.critlist = new MyCritList({
                        collection: memory,
                        renderRow: this.RenderRow.bind(this),
                        //selectionMode: 'single'
                    }, 'critlist').placeAt(this.criteriaContainer);
                    this.critlist.on('dgrid-select', this.SelectHandler.bind(this));
                    this.critlist.startup();
                },
                RenderRow: function (data, options) {
                    return data.domNode;
                },
                Refresh: function () {
                    this.criteriaItemList.splice(0, this.criteriaItemList.length);
                    //this.recordCounter.SetValue(25);
                    this.critlist.refresh();
                },
                Show: function () {
                    //var coords = domGeo.position(this.parent.domNode);

                    //this.dialog.show();
                    //domStyle.set(this.dialog.domNode, {left: (coords.x + 50) + "px", top: coords.y + "px", bottom: coords.y + "px", right: 50 + "px"});
                    this._mapPenl.SetInstructions(this.parent.loadedItem["serch_tip"]);
                },
                GetCriteria: function () {
                    var criteria = [];
                    var memCount = 0;
                    var ands = [];
                    var ors = [];
                    var hadAnds = false;
                    var hadOrs = false;
                    var criteriaLookup = [];
                    var lookUp;
                    for (var i = 0, l = this.criteriaItemList.length; i < l; i++) {
                        var criterion = this.criteriaItemList[i].GetData();
                        if (!this.criteriaItemList[i].IsMemory()) {

                            if (this._allLayerLookup !== null) {
                                //For each criteria item when we are dealing with All Layers

                                // Use a criteriaLookup for aggregating info and relating to data retreived for the criterion.
                                criteriaLookup[i] = {andLayers: [], orLayers: [], "criterion": criterion};
                                if (i === 0) {
                                    ands = ands.concat(this._allLayerLookup[criterion.field]);
                                    hadAnds = null;
                                }
                                switch (criterion.andor.toLowerCase()) {
                                    case 'and':
                                        // In the case of an criterion ...
                                        // get the layer ids for the given field.
                                        lookUp = this._allLayerLookup[criterion.field];
                                        // update criteria lookup, add each layer id to the add layers for the criterion
                                        for (var ii = 0; ii < lookUp.length; ii++) {
                                            criteriaLookup[i].andLayers.push(lookUp[ii]);
                                        }
                                        // this first item is treated like an and, so add it to the ands and set the hadAdd flag.
                                        if (i > 0) {

                                            // prune any layer ids from the existing ands that are not in the new set of layer ids.
                                            for (var iii = ands.length - 1; iii >= 0; iii--) {
                                                var id = ands[iii];
                                                var index = lookUp.indexOf(id);
                                                if (index < 0)
                                                    ands.splice(index, 1);
                                            }
                                            // we don't flag hadAnds in the i===0 check because technically the first item is neither and no or.
                                            // so if it hadAnds is null then this is the first true anded criteria.
                                            if (ands === null) {
                                                if (ands.length > 0)
                                                    hadAnds = true;
                                                ;
                                            }

                                        }
                                        break;
                                    case '':
                                    case 'or':
                                        // For ors things are simpler, get the lookup and add any layer ids we don't have
                                        // to the cirteria look up's orLayers.
                                        lookUp = this._allLayerLookup[criterion.field];
                                        if (lookUp === undefined)
                                            break;
                                        for (var ii = 0; ii < lookUp.length; ii++) {
                                            criteriaLookup[i].orLayers.push(lookUp[ii]);
                                            hadOrs = true;
                                            if (ors.indexOf(ii) < 0) {
                                                ors.push(lookUp[ii]);
                                            }
                                        }
                                        break;
                                }

                            } else {
                                // if not all layers, accept the criterion.
                                criteria.push(criterion);
                            }
                        } else {
                            memCount++;
                            if (memCount > -1) {
                                if (memCount > 1) {
                                    memCount = -1;
                                    alert('Only one criteria may be specified; the first memory based criteria will be used others will be ignored');
                                } else {
                                    criteria.push(this.criteriaItemList[i].GetData());
                                }
                            }
                        }
                    }
                    if (this._allLayerLookup !== null) {
                        // Second pass: figure out which criteria to actually keep.
                        // Loop through the criteriaLookup which should be in the same
                        // order as the criteriaItemList, if an item is an and criteria and
                        // it its andLayers are in the aggregated ands array use the criteria.
                        // else if the item is an "or" or "" criteria and an orsLayer entry is in
                        // ors keep it.
                        var cLayers;
                        for (var c = 0; c < criteriaLookup.length; c++) {
                            if (criteriaLookup.hasOwnProperty(c) === false)
                                continue;
                            var cLookup = criteriaLookup[c];
                            if (cLookup.criterion.andor.toLowerCase() === 'and') {
                                if (ands.length > 0) {

                                    cLayers = cLookup.andLayers;
                                    cLookup.andLayers = cLookup.andLayers.filter(function (n) {
                                        return ands.indexOf(n) > -1;
                                    });
                                    if (cLookup.andLayers.length > 0) {
                                        criteria.push(cLookup);
                                        break;
                                    }
                                }
                                continue;
                            } else if (["or", ""].indexOf(cLookup.criterion.andor.toLowerCase()) > -1) {
                                if (ors.length > 0) {
                                    cLayers = cLookup.orLayers;
                                    cLookup.orLayers = cLookup.orLayers.filter(function (n) {
                                        return ors.indexOf(n) > -1;
                                    });
                                    if (cLookup.orLayers.length > 0) {
                                        criteria.push(cLookup);
                                    }
                                }
                                continue;
                            }
                        }
                        // at this point there will either be an empty criteria list because there were no criteria
                        // or because there were no tables in common for the combination of anded criteria.
                        // or criteria, however, should be relatively inclusive.
                        if ((criteria.length === 0) && hadAnds) {
                            alert('The anded criteria you entered will not be able to match available layers. For anded criteria to work each table must have all the corresponding anded fields');
                        }
                    }
                    return criteria;
                },
                CriteriaToLayerCriterionMap: function (criteria) {
                    var mapping = {};
                    for (var c = 0; c < criteria.length; c++) {
                        var criterionInfo = criteria[c];
                        var l;
                        var layer;

                        if (criterionInfo.field === 'memory') {
                            var layers = Layers.GetSpatialLayerIds();
                            for (l in layers) {
                                layer = layers[l];
                                if (mapping.hasOwnProperty(layer) === false) {
                                    mapping[layer] = [];
                                }
                                if (mapping[layer].indexOf(criterionInfo) < 0) {
                                    mapping[layer].push(criterionInfo);
                                }

                            }

                        } else {

                            for (l = 0; l < criterionInfo.andLayers.length; l++) {
                                layer = criterionInfo.andLayers[l];
                                if (mapping.hasOwnProperty(layer) === false) {
                                    mapping[layer] = [];
                                }
                                if (mapping[layer].indexOf(criterionInfo.criterion) < 0) {
                                    mapping[layer].push(criterionInfo.criterion);
                                }
                            }
                            for (l = 0; l < criterionInfo.orLayers.length; l++) {
                                layer = criterionInfo.orLayers[l];
                                if (mapping.hasOwnProperty(layer) === false) {
                                    mapping[layer] = [];
                                }
                                if (mapping[layer].indexOf(criterionInfo.criterion) < 0) {
                                    mapping[layer].push(criterionInfo.criterion);
                                }
                            }
                        }
                    }
                    return mapping;
                },
                GetMapLimit: function () {
                    var isLimited = this.limit_cb.checked === true;
                    if (isLimited) {
                        if (this._bbox === null) {
                            var bounds = this._map.getBounds();
                            var bbox = slMaps.BoundsToBbox(bounds);
                            return bbox;
                        } else {
                            return this._bbox;
                        }
                    } else {
                        return false;
                    }
                },
                SetResultInfo: function (ids, unpagedIds, layer_bbox, paged_bbox, unpaged_bbox) {
                    var navButton = jQuery('button.leaflet-control-mapquest-navigation-reset')[0];
                    this._initialMapHeight = this.map.offsetHeight;
                    if (!this._navHooked) {
                        var new_element = navButton.cloneNode(true);
                        navButton.parentNode.replaceChild(new_element, navButton);
                        on(new_element, 'click', (function (event) {
                            this._bboxMode = this._selectedBboxMode;
                            this.ResetImages(true);
                            this.UpdateLayerImages();
                        }).bind(this))
                        this._navHooked = true;
                    }
                    this.RefreshMapSize();
                    this.partialLoad = null;
                    this.ResetImages();
                    this._ids = ids;
                    this._unpagedIds = unpagedIds;
                    console.log(this._layer_bbox);
                    this._layer_bbox = layer_bbox;
                    this._paged_bbox = paged_bbox;
                    this._unpaged_bbox = unpaged_bbox;
                    this._bboxMode = this._selectedBboxMode;
                },
                UpdateLayerImages: function (opt_targetLayer) {
                    if (this._layerId == '*')
                        return;
                    this._allReady = false;
                    this._pageReady = false;
                    var width = this._mapSize.width;
                    var height = this._mapSize.height;
                    var zC = this._layer_zoomCenter;
                    var bounds = null;
                    var bboxMode = this._bboxMode;
                    switch (bboxMode) {
                        default:
                        case 'map':
                            break;
                        case 'layer':
                            this._navigating = true;
                            this._map.fitBounds(slMaps.BboxToBounds(this._layer_bbox), {"animate": false, "pan": {"noMoveStart": true}});
                            this._navigating = false;
                            break;
                        case 'paged':
                            this._navigating = true;
                            this._map.fitBounds(slMaps.BboxToBounds(this._paged_bbox), {"animate": false, "pan": {"noMoveStart": true}});
                            this._navigating = false;
                            break;
                        case 'unpaged':
                            this._navigating = true;
                            this._map.fitBounds(slMaps.BboxToBounds(this._unpaged_bbox), {"animate": false, "pan": {"noMoveStart": true}});
                            this._navigating = false;
                            break;
                    }
                    bounds = this._map.getBounds();
                    var bbox = slMaps.BoundsToBbox(bounds);
                    var layerSpecified = (opt_targetLayer !== undefined);
                    var displayCheck = (this.partialLoad === null) || (this.partialLoad === false);
                    if (displayCheck) {
                        if (!layerSpecified || (layerSpecified && (opt_targetLayer === this._unpaged))) {
                            LayerImage.GetImageURI(this.AllResultsImageReady.bind(this),
                                    this._layerId, bbox, width, height, 'web', '#ff0000;-1.8;0',
                                    '#00ff00;.25;0', this._unpagedIds.join(','), false, this._unpagedIds.join(','));
                        }
                    }
                    if (!layerSpecified || (layerSpecified && (opt_targetLayer === this._paged))) {
                        LayerImage.GetImageURI(this.PageResultsImageReady.bind(this),
                                this._layerId, bbox, width, height, 'web', '#ffff00;.5,3',
                                null, this._ids.join(','), false, this._unpagedIds.join(','));
                    }
                    if (!layerSpecified || (layerSpecified && (opt_targetLayer === this._selFeatureLyr))) {
                        var sels = (this._selections != null) ? this._selections.join(',') : '';
                        if (this._selections !== null) {
                            if (this._selections.length > 0) {
                                LayerImage.GetImageURI(this.SelImageReady.bind(this),
                                        this._layerId, bbox, width, height, 'web', '#ffff00:1,3',
                                        null, sels, false, '');
                            }
                        }
                    }
                    //this.staticMapContainer.style.width = width + 'px';
                    //this.staticMapContainer.style.height = height + 'px';

                },
                AllResultsImageReady: function (imgInfo) {
                    domClass.toggle(this.map, 'invis', false);
                    this.partialLoad = true;
                    this._unpaged.setBounds(slMaps.BboxToBounds(imgInfo.image.bbox));
                    this._unpaged.setUrl(imgInfo.image.content);
                    this._allReady = true;
                    if (this._allReady && this._pageReady)
                        this._navigating = false;
                },
                PageResultsImageReady: function (imgInfo) {
                    domClass.toggle(this.map, 'invis', false);
                    this._paged.setBounds(slMaps.BboxToBounds(imgInfo.image.bbox));
                    this._paged.setUrl(imgInfo.image.content);
                    this._pageReady = true;
                    if (this._allReady && this._pageReady)
                        this._navigating = false;
                },
                SelImageReady: function (imgInfo) {
                    domClass.toggle(this.map, 'invis', false);
                    this._selFeatureLyr.setBounds(slMaps.BboxToBounds(imgInfo.image.bbox));
                    this._selFeatureLyr.setUrl(imgInfo.image.content);
                    this._pageReady = true;
                    if (this._allReady && this._pageReady)
                        this._navigating = false;
                },
                UpdateBboxMode: function () {
                    if (this._allReady && this._pageReady) {
                        this._bboxMode = this._selectedBboxMode;
                    }
                },
                ResetImages: function (all) {

                    if (all === undefined) {
                        if (this.partialLoad === null) {
                            all = true;
                        } else {
                            all = !this.partialLoad;
                        }

                    }
                    this.partialLoad = !all;
                    if (all) {
                        if (!this._layerId == '*') {

                        }
                        this._unpaged.setUrl(slURL.GetEmptyImgURL());
                    }
                    this._paged.setUrl(slURL.GetEmptyImgURL());
                },
                HandleNavButton: function (event) {


                    switch (event.currentTarget) {
                        case this.wholeLayer_btn:
                            this._bboxMode = "layer";
                            break;
                        case this.allResults_btn:
                            this._bboxMode = "unpaged";
                            break;
                        case this.currentResults_btn:
                            this._bboxMode = "paged";
                            break;
                    }
                    this._selectedBboxMode = this._bboxMode;
                    this.UpdateLayerImages(true);
                }

            });
        });
