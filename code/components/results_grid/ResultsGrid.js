define(["dojo/_base/declare", "dojo/on", "dojo/topic", "dojo/dom-construct", "dojo/dom-attr",
    'dojo/dom-class', 'dojo/dom-style', 'dojo/_base/array', "dojo/query",
    'dgrid/Selection', 'dstore/Memory', 'dstore/Trackable',
    "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin",
    "sl_classes_open/Topical",
    "sl_modules_open/sl_api/Layers",
    "sl_modules_open/StateUtils",
    "dojo/text!./ResultsGrid.tpl.html"
], function (declare, on, topic, domCon, domAttr, domClass,
        domStyle, ArrayObject, query, Selection, Memory, Trackable, _WidgetBase, _TemplatedMixin,
        _WidgetsInTemplateMixin, Topical, Layers, StateUtils,template) {
    return declare('sl_components_open/results_grid/ResultsGrid', [Topical, _WidgetBase, _TemplatedMixin,
        _WidgetsInTemplateMixin], {
        templateString: template,
        baseClass: "ResultsGrid grid layout-inset",
        _gridNode: null,
        _gridObj: null,
        _layerId: null,
        _criteria: null,
        _mapLimit: null,
        _params: null,
        _count: null,
        _data: null,
        _ids: null,
        _unids: null,
        _rowCtr: null,
        _readyHandler: null,
        _searchableKeys: null,
        constructor: function (handler) {
            this._searchableKeys = [];
            this._readyHandler = handler;
        },
        postCreate: function () {

            this._gridNode = this.grid;
            on(this._gridNode, 'dgrid-select', this._HandleSelect.bind(this));
            this._readyHandler(this);
            return;
        },
        SetUp:function(layerId, criteria, mapLimit) {
            this._layerId = layerId;
            this._criteria = criteria;
            this._mapLimit = mapLimit;
            this.Search();
        },
        _Busy: function (message, percent) {
            topic.publish('results_grid/progress', {'layerId': this._layerId, 'state': message, 'percent': percent});
            //this.searchCriteria.Updating();
        },
        _UpdateGrid: function (response) {
            
            var atts = response.attributes;
            this._rowCtr = 0;
            // Make a TrackableMemory store
            TrackableMemory = declare([Memory, Trackable], {
                sort: function (sorted) {
                    sorted = [];
                    return this.inherited(arguments);
                }
            });

            // Build a list of visible and searchable keys
            var columns = [];
            var visibleKeys = []; //, searchableKeys = [];

            var hadGid = false;
            for (var i = 0, len = atts.length; i < len; i++) {
                if(atts[i].name == 'the_geom') continue;
                if (atts[i].name == 'gid') {
                    hadGid = true;
                    continue;
                }
                if (atts[i].visible) {
                    visibleKeys.push(atts[i]);
                    if (atts[i].searchable) {

                        this._searchableKeys.push(atts[i]);
                    }
                }
            }
            var idField = 'gid';
            if (!hadGid) {
                idField = atts[0].name;
            }

            // Make sure gid is the first visible key
            //if (visibleKeys.length > 0) {
            visibleKeys.shift({
                "display": "id",
                "name": "gid"
            });
            this._searchableKeys.shift(visibleKeys[0]);

            var count = this._data.length;
            
            this._gridNode.style.height = "Calc(2.75em * "+(count+1)+" + 12px)";
            this.title.innerHTML = response.layer_name+" : Found "+count+" results";
            //}
            // Get an instance of the trackable memory object with what we have so far.
            var memory = new TrackableMemory({data: this._data, idField: idField, idProperty: idField});
            columns.push({"field": 'gid', 'label': '', renderCell: this._RenderActionsCell.bind(this)});



            for (var i = 0, len = visibleKeys.length; i < len; i++) {
                switch (visibleKeys[i].requirement) {
                    case "url":
                        columns.push({
                            field: visibleKeys[i].name,
                            label: visibleKeys[i].display,
                            renderCell: this._RenderURLCells.bind(this)
                        });
                        break;
                    default:
                        columns.push({
                            field: visibleKeys[i].name,
                            label: visibleKeys[i].display,
                            renderCell: this._RenderCells.bind(this)
                        });
                }
            }

            this._gridObj = new MyGrid(
                    {
                        collection: memory,
                        columns: columns,
                        selectionMode: 'single',
                        minRowsPerPage: 1000,
                        maxRowsPerPage: 1000,
                        loadingMessage: 'Loading data...',
                        noDataMessage: 'No results found.',
                        bufferRows: count
                    }
            );

            this._gridObj.placeAt(this._gridNode);
            //this.searchCriteria.UpdateLayerImages(500,500);
            setTimeout(this._DelayedStart.bind(this), 500);

        },
        _DelayedStart: function () {
            this._gridObj.startup();
        },
        Search: function () {
            this._MakeSearchParams();
            Layers.SearchFeatures(this._params, this._HandleFeatures.bind(this));
        }
        ,
        _HandleFeatures: function (response) {
            if (response.hasOwnProperty('status')) {
                if (response.status !== 'ok') {
                    return;
                }
            }
            this._count = parseInt(response.paging.count);
            this._data = response.resultset.results;
            if(this._count === 0) {
                domCon.destroy(this.domNode);
                this.destroy();
                return;
            }
            this._ids = [];
            for (i = 0, l = this._data.length; i < l; i++) {
                this._ids.push(this._data[i].gid);
            }
            this._unids = response.resultset.hasOwnProperty('unpaged_ids') ? response.resultset.unpaged_ids : null;
            //this.searchCriteria.SetResultInfo(ids, unids, response.resultset.layer_bbox, response.resultset.paged_bbox, response.resultset.unpaged_bbox);

            domCon.empty(this._gridNode);
            this._UpdateGrid(response);
            
//                    /this.searchResults
            // this.searchCriteria.UpdateLayerImages();
        },
        _HandleSelect: function (event) {
            var sels = [];
            for (var item in this._gridObj.selection) {
                if (this.gridObj.selection[item] === true)
                    sels.push(item);
            }
            topic.publish('reslts_grid/selchange', {'layerId': this._layerId, 'features': sels});
            //this.parent.HandleFeature(event.rows[0].element.rowIndex + this.currStart);
            //domClass.toggle(event.rows[0].element, "dgrid-selected");
            //this.gridObj.selection[(event.rows[0].id)] = false;
        },
        _MakeSearchParams: function () {
            var memory = Layers.GetMemory();
            var params = {layer: this._layerId, 'with_attributes': 1};

            if (this._mapLimit !== false) {
                params['bbox'] = this._mapLimit;
            }
            // Loop throguh criteria and detect memory criteria;
            // If a memory criterion is detected establish the 
            // memory related parameters and then remove the criterion.
            for (var c = this._criteria.length - 1; c >= 0; c--) {
                var criterion = this._criteria[c];
                if (criterion.field === 'memory') {
                    if (!memory.HasMemoryLayer()) {
                        continue;
                    }
                    this._criteria.splice(c, 1);
                    var memoryInfo = memory.Restore();
                    params['memoryLayer'] = memoryInfo.layerId
                    params['intersectMode'] = criterion.compare;
                    params['gids'] = Layers.GetMemory().FeaturesToString();
                    
                    if (criterion.hasOwnProperty('buffer')) {
                        params['buffer'] = criterion.buffer;
                    }
                }
            }
            this.filters = JSON.stringify(this._criteria);
            params.filters = this.filters;
            this._params = params;
            return this._params;
        },
        _RenderURLCells: function (object, value, node, options) {
            domCon.create('a', {'href': value, 'innerHTML': value, 'title': value}, node);
        },
        _RenderCells: function (object, value, node, options) {
            domCon.create('b', {'innerHTML': value, 'title': value}, node);
        },
        _RenderActionsCell: function (object, value, node, options) {

            var z2f = domCon.create('button', {type: 'button', class: 'btn btn-sm btn-primary', 'name': value}, node);
            domCon.create('span', {'class': 'glyphicon glyphicon-record'}, z2f);
            on(z2f, 'click', this._HandleZoomToFeature.bind(this));

            var offset = this.currStart + this._rowCtr;

            var ibtn = domCon.create('button', {type: 'button', class: 'btn btn-sm btn-primary', 'name': 'info', 'data-value': value, 'data-index': offset}, node)
            domCon.create('span', {'class': 'glyphicon glyphicon-chevron-right'}, ibtn);
            on(ibtn, 'click', this._HandleInfoButton.bind(this));
            this._rowCtr += 1;
            domClass.toggle(node, 'layout-left', true);
            //domCon.create("button",{type:'button','class':'btn btn-sm'},node);
            //TODO: RenderActionCell
        },
        _HandleInfoButton: function (event) {
            this.parent.HandleFeature(parseInt(domAttr.get(event.currentTarget, 'data-index')));
        },
        _HandleZoomToFeature: function (event) {

            topic.publish('features/zoom', {'layerId': this.layerItem.id, 'feature': event.currentTarget.name});

        }

    });
});