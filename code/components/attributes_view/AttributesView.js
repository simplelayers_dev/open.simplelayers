define(['dojo/_base/declare', "dojo/on", "dojo/dom-construct", "dojo/dom-class",
    "dojo/dom-attr", "dijit/_WidgetBase", "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin", "dojo/dom", 'dstore/Memory', 'dstore/Trackable', 'dgrid/OnDemandGrid',
    'dgrid/extensions/DijitRegistry', 'dgrid/extensions/ColumnResizer', "sl_modules_open/sl_api/Layers",
    "dojo/text!./AttributesView.tpl.html"],
        function (declare, on, domCon, domClass, domAttr, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, dom, Memory,
                Trackable, Grid, Registry, ColumnResizer, Layers, template) {
            return declare('AttributesView', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                loadedItem: null,
                layerItem: null,
                parent: null,
                attGrid: null,
                templateString: template,
                baseClass: 'attributes_view',
                constructor: function () {
                    AttributesGrid = declare([Grid, Registry, ColumnResizer]);
                },
                postCreate: function () {
                    
                },
                Init: function (loadedItem, layerItem, parent) {
                    this.loadedItem = loadedItem;
                    this.layerItem = layerItem;
                    this.parent = parent;
                    domCon.empty(this.grid);
                    var keys = this.loadedItem.layerAttributes;
                    var columns = [{field: "z", label: "Display Order", renderCell: this.RenderOrderCells.bind(this)},
                        {field: "name", label: "Attribute", renderCell: this.RenderValueCells.bind(this)},
                        {field: "display", label: "Display Name", renderCell: this.RenderValueCells.bind(this)},
                        {field: "type", label: "Type", renderCell: this.RenderValueCells.bind(this)},
                        {field: "visible", label: "Visible", renderCell: this.RenderBoolCells.bind(this)},
                        {field: "searchable", label: "Searchable", renderCell: this.RenderBoolCells.bind(this)}];
                    TrackableMemory = declare([Memory, Trackable], {
                        /*sort: function (sorted) {
                         sorted = [];
                         return this.inherited(arguments);
                         }*/
                    });
                    var memory = new TrackableMemory({data: keys, idField: "z", idProperty: "z"});
                    
                    var obj = {
                        submit: "hidden",
                        widgets: [{
                                // visibleKeys goes in here
                            }]
                    };
                    var JSONInputData = {};
                    
                    for (var i = 0; i < memory.data.length; i++) {
                        var JSONWidgetData = {
                            name: memory.data[i].name,
                            label: memory.data[i].name,
                            value: memory.data[i].display,
                            disable: true
                        };
                        
                        var type;
                        switch (memory.data[i].type) {
                            case "url":
                                type = "urlInput";
                                JSONWidgetData.urlButton = "Open";
                                break;
                            default:
                                type = "textInput";
                        }
                        JSONWidgetData.componentType = type;

                        JSONInputData[memory.data[i].name] = memory.data[i].display;
                        obj.widgets[i] = JSONWidgetData;
                    }

                    var form = new DataEntry();
                    form.Start(obj);
                    form.placeAt(this.fieldSet);
                    
                    this.attGrid = new AttributesGrid(
                            {
                                collection: memory,
                                columns: columns,
                                minRowsPerPage: 100,
                                maxRowsPerPage: 100,
                                loadingMessage: 'Loading data...',
                                noDataMessage: 'No results found.',
                                bufferRows: 10
                            }
                    );
                    this.attGrid.on('dgrid-refresh-complete', this.ResizeGrid.bind(this));
                    this.attGrid.placeAt(this.grid);
                    this.attGrid.startup();
                    this.attGrid.set("sort", [{property:"z",descending:true}]);
                    
                },
                ResizeGrid: function () {
                    this.attGrid.resize();
                    
                    
                },
                RenderValueCells: function (object, value, node, options) {
                    domCon.create('b', {'innerHTML': value, 'title': value}, node);
                },
                RenderBoolCells: function (object, value, node, options) {
                    domCon.create('b', {'innerHTML': value, 'title': value}, node);
                },
                RenderOrderCells: function (object, value, node, options) {
                    domCon.create('b', {'innerHTML': value * -1, 'title': value * -1}, node);
                }
            });
        });
