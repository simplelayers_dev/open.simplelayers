/* global catalog */

define(['dojo/_base/declare',
    "dojo/on",
    "dojo/topic",
    "dojo/dom-construct",
    "sl_components_open/preview_manager/PreviewManager",
    "sl_components_open/info_view/InfoView",
    "sl_components_open/layer_metadata_view/LayerMetadataView",
    "sl_components_open/features_view/FeaturesView",
    "sl_components_open/feature_view/FeatureView",
    "sl_components_open/attributes_view/AttributesView",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/dom-class",
    "dojo/query",
    "sl_components_open/action_button/ActionButton",
    "sl_components_open/indicator/Indicator",
    "sl_modules_open/sl_api/Layers",
    "sl_modules_open/layerImage",
    "dojo/text!./LayerPreview.tpl.html"],
        function (declare, on, topic, domCon, PreviewManager, InfoView, LayerMetadataView, FeaturesView, FeatureView, AttributesView, domAttr, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, domClass, query, ActionButton, ProgressIndicator, Layers, LayerImage, template) {
            return declare('sl_components_open/layer_preview/LayerPreview', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                _fullView: null,
                layerItem: null,
                loadedItem: null,
                featuresLoaded: false,
                attributesLoaded: false,
                templateString: template,
                topicHandlers: null,
                baseClass: 'layer_preview flex-item flex-container col',
                _restoring: false,
                postCreate: function () {
                    this.topicHandlers = [];
                    this.previewManager.startup();
                    on(this.overview_a, 'click', this.HandleInfoButton.bind(this));
                    on(this.metadata_a, 'click', this.HandleMetadataButton.bind(this));
                    on(this.records_a, 'click', this.HandleFeaturesButton.bind(this));
                    on(this.attributes_a, 'click', this.HandleAttributesButton.bind(this));
                    on(this.csv_a, 'click', this.HandleCSV.bind(this));
                    on(this.kml_a, 'click', this.HandleKML.bind(this));
                    on(this.shp_a, 'click', this.HandleSHP.bind(this));
                    on(this.toggle_btn, 'click', this.HandleToggle.bind(this));
                    this._isFullView = false;
                    domClass.toggle(this.headerBar, 'sans-layer', true);
                    on(this.export_li, 'click', this.HandleExportLI.bind(this));


                },
                HandleMemoryRecall: function (event) {
                    var layer = event.layerData;
                    this.SetPreview(layer);

                },
                HandleExportLI: function (event) {

                    if (event.hasOwnProperty('preventDefault')) {
                        event.preventDefault();
                    }

                    var isExpanded = domClass.contains(this.export_menu, 'layout-block');
                    domClass.toggle(this.export_menu, 'layout-block', !isExpanded);
                },
                HandleToggle: function (event) {
                    event.preventDefault();
                    this.Toggle();

                },
                Toggle: function () {
                    domClass.toggle(this.domNode, 'full-view');
                    //this._isFullView = domClass.contains(this.domNode,'full-view');
                    topic.publish('layer_peview/full-view/changed', {target: this, 'isFullView': this._isFullView});

                },
                ResetNav: function () {
                    var navItems = query('.nav .navigable');
                    for (var i in navItems) {
                        var item = navItems[i];
                        domClass.toggle(item, 'active', i == 0);
                    }
                },
                SetPreview: function (layerItem, view, action) {
                    if (view !== undefined) {
                        switch (view) {
                            case 'features':
                                this._restoring = Layers.GetMemory().HasMemoryLayer();
                        }

                    } else {
                        this._restoring = false;
                    }
                    this.layerItem = layerItem;

                    domClass.toggle(this.records_li, 'hidden', !Layers.IsLayerTabular(layerItem.type));
                    this._allLayerMode = layerItem.data.type === "*";
                    domClass.toggle(this.export_li, 'hidden', !Layers.IsLayerTabular(layerItem.type));
                    domClass.toggle(this.attributes_li, 'hidden', !Layers.IsLayerTabular(layerItem.type));
                    domClass.toggle(this.metadata_li, 'hidden', false);
                    if (this._allLayerMode) {
                        domClass.toggle(this.overview_li, 'hidden', true);
                        domClass.toggle(this.records_li, 'hidden', false);
                        domClass.toggle(this.metadata_li, 'hidden', true);


                    }

                    this.name.innerHTML = this.layerItem.data["name"];
                    if (typeof this.layerItem['last_modified'] !== 'undefined') {
                        this.last_modified.innerHTML = "Last modified: " + this.layerItem["last_modified"].substring(0, 16);
                    }
                    domAttr.set(this.last_modified, "title", this.layerItem["last_modified"]);

                    this.featuresLoaded = this.attributesLoaded = false;
                    if (!this._allLayerMode) {
                        Layers.GetLoadedLayer(this.layerItem.id, this.HandleLoadedLayer.bind(this));
                        topic.publish('pane_content_updated', {target: this, container: this.previewManager});
                    } else {
                        this.HandleFeaturesButton();
                        //this.featuresView.Init(this.layerItem.data.data, this.layerItem, this, false);
                        //this.previewManager.GoToView("view_features");
                        topic.publish('pane_content_updated', {target: this, container: this.previewManager});
                    }

                    domClass.toggle(this.headerBar, 'sans-layer', false);


                },
                RestoreMemory: function () {
                    this.featuresView.Init(this.loadedItem, this.layerItem, this, false);
                    this.previewManager.GoToView("view_features");
                    this.featuresView.RestoreMemory();


                },
                HandleLoadedLayer: function (jsonData) {
                    this.loadedItem = jsonData;

                    this.metadataView.SetLayer(this.loadedItem.id);

                    this.infoView.Init(this.loadedItem, this.layerItem, this);
                    this.ResetNav();
                    this.HandleInfoButton();

                    domClass.remove(this.buttons, "hidden");
                    switch (jsonData["type_label"]) {
                        case "vector":
                        case "odbc":
                        case "relational":
                        case "relatable":
                            domClass.remove(this.records_a, "disabled");
                            domClass.remove(this.attributes_a, "disabled");
                            break;
                        default:
                            domClass.add(this.records_a, "disabled");
                            domClass.add(this.attributes_a, "disabled");
                    }

                    if (jsonData["permission"] < 8 || jsonData["export_options"].length <= 0) {
                        domClass.add(this.export_li, "disabled");
                    } else {
                        domClass.remove(this.export_li, "disabled");
                    }
                    if (this._restoring === true) {
                        this.RestoreMemory();
                    }
                },
                HandleReturnButton: function (event) {

                },
                HandleInfoButton: function (event) {
                    this.previewManager.GoToView("view_info");
                },

                HandleMetadataButton: function (event) {
                    this.previewManager.GoToView("view_metadata");
                },
                HandleCSV: function (event) {
                    domAttr.set(this.downloadFrame, "src", Layers.GetDownloadLink(this.layerItem.id, "csv"));
                },
                HandleKML: function (event) {
                    domAttr.set(this.downloadFrame, "src", Layers.GetDownloadLink(this.layerItem.id, "kml"));
                },
                HandleSHP: function (event) {
                    domAttr.set(this.downloadFrame, "src", Layers.GetDownloadLink(this.layerItem.id, "shp"));
                },
                HandleFeaturesButton: function (event) {
                    if (domClass.contains(this.overview_a, "disabled"))
                        return;
                    if (!this.featuresLoaded) {
                        if (this._allLayerMode) {
                            this.featuresView.Init(this.layerItem.data.data, this.layerItem, this, false);
                        } else {
                            this.featuresView.Init(this.loadedItem, this.layerItem, this);
                        }
                    }
                    this.featuresLoaded = true;
                    this.previewManager.GoToView("view_features");
                },
                ReturnToFeatures: function (event) {
                    this.previewManager.GoToView("view_features");
                },
                HandleFeature: function (event) {
                    this.featureView.Init(event, this.loadedItem, this.layerItem, this);
                    this.previewManager.GoToView("view_feature");
                },
                HandleAttributesButton: function (event) {
                    if (!this.attributesLoaded)
                        this.attributesView.Init(this.loadedItem, this.layerItem, this);
                    this.attributesLoaded = true;
                    this.previewManager.GoToView("view_attributes");
                },
                HandleClassificationButton: function (event) {

                }
            });
        });