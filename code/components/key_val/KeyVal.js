define([ "dojo/_base/declare", "dojo/on","dojo/dom-construct", "dojo/dom-attr",
		'dojo/dom-class', 'dojo/dom-style', 'dojo/_base/array',"dojo/query",
		'components/user_list/metadata_dialog/MetadataDialog',
		"dojo/text!./KeyVal.tpl.html" 
		 ], function(declare, on, domCon,domAttr,domClass, 
			domStyle, ArrayObject, query, MetaDialog, template) {
	return declare('components/KeyVal', [ ], {
		currentState:null,
		name:null,
		data:null,
		states:['view','edit','deleted','modified'],
		ui:null,
		textAreas:null,
		textInputs:null,
		_value:null,
		postCreate : function() {
			
			return;
		},
		SetValue:function(val) {
			
			for(var ta in this.textAreas) {
				this.textAreas[ta].text = val;
			}
			for(var ti in this.textInputs) {
				this.textInputs[ti].value = val;
			}
			
			this.value = val;
			
			return false;
		},
		GetValue:function() {
			return this.value;
		},
		SetData:function(name,data,state) {
			this.textAreas = [];
			this.textInputs = [];
			this.name = name;
			this.data = data;
			
			var tpl = ''+template;
			var regx = RegExp("__name__","g");
			tpl = tpl.replace(regx,name);
			
			var key = null;
			
			for(key in data) {
				regx = RegExp("__"+key+"__","g");
				tpl = tpl.replace(regx,data[key]);
			}
			this.ui = domCon.toDom(tpl);
			for(key in data) {
				var  nodes = query('.'+this.name+'_input',this.ui);
				var n;
				var nl;
				var type = data.type;
				for(n=0,nl=nodes.length;n<nl;n++) {
					
					if(type == 'textarea') {
						console.log(nodes[n]);
						domCon.destroy(nodes[n]);
					} else {
						nodes[n].value="";
						this.textInputs.push(nodes[n]);
						on(nodes[n],"keyup",this.UpdateValue.bind(this));
					}
				}
				var  nodes = query('.'+this.name+'_input_ta',this.ui);
				for(n=0,nl=nodes.length;n<nl;n++) {
					if(data[key] == 'text') {
						domCon.destroy(nodes[n]);												
					} else {
						console.log(nodes[n]);
						nodes[n].text="";
						this.textAreas.push(nodes[n]);
						on(nodes[n],"keyup",this.UpdateValue.bind(this));
					}
				}
				
			}
			this.SwitchState(state);
			
			return this.ui;
			
		
		},
		UpdateValue:function(event) {
			this.value = event.target.value;
			
		},
		SwitchState:function(state) {
			this.currentState = state;
			if(this.ui===null) return;
			var l=this.states.length;
			var dt = query('dt',this.ui).pop();
			var dd = query('dd',this.ui).pop();
			for(var i=0;i<l;i++) {
				var stateItem = this.states[i];
				var toggle=false;
				if(this.states[i]==state) {
					toggle=true;
				}
				domClass.toggle( dt, stateItem+'_state',toggle);
				domClass.toggle( dd, stateItem+'_state',toggle);
			}
		}
		});
});
