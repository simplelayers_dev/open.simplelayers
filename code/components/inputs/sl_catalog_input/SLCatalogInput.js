define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/dom",
    "components/sl_catalog/SLCatalog",
    "dojo/text!./SLCatalogInput.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                dom,
                SLCatalog,
                template)
        {
            return declare
                    (
                            'SLCatalogInput',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
                            {
                                data: null,
                                name: null,
                                button: null,
                                input: null,
                                templateString: template,
                                baseClass: 'SLCatalogStyle',
                                constructor: function (data)
                                {
                                    this.data = data;
                                },
                                postCreate: function ()
                                {
                                    var JSONText = '{}';
                                    var wrapper = dom.byId('wrapper');
                                    wrapper.innerHTML = "";
                                    var obj = JSON.parse(JSONText);
                                    var catalog = new SLCatalog(obj);
                                    catalog.placeAt(dom.byId('wrapper'));
                                }
                            }
                    );
        }
);