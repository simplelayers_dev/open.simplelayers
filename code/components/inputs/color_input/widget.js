define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-attr",
		 "dojo/dom-class",
		 "dojo/dom-style",
		 "dojox/widget/ColorPicker",
		 "dijit/ColorPalette",
		 "dijit/form/DropDownButton",
		 "dojo/fx/Toggler",
		 "sl_components_open/dialogs/SL_ColorDialog/widget",
		 "dijit/Dialog",
		 "dojo/dom-construct",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dojo/text!./templates/color_input.tpl.html"],
		function (declare, 
				  on,
				  domAttr,
				  domClass,
				  domStyle,
				  ColorPicker,
				  ColorPalette,
				  DropDownButton,
				  Toggler,
				  slColorDialog,
				  dijitDialog,
				  domCon,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
				  template) {
		    return declare('inputs/color_input', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    templateString: template,
                baseClass:'color_input',
				
				count: null,
				toggler: null,
				colorDialog: null,
				slDialog: null,
			    postCreate: function () {
					this.slDialog =new slColorDialog();
					this.colorDialog = new dijitDialog({
						title: "SL_ColorDialog",
						content: this.slDialog,
						style: "width: 360px"

					});
					this.slDialog.startup();
					on(this.colorPress, "click", this.ShowDialog.bind(this));
					
					on(this.slDialog.domNode, "color_selected", this.ColorSelected.bind(this));
					
					on(this.colorInput,"keyup",this.TextChange.bind(this));
			    },
				ColorSelected: function(event){
					this.colorDialog.hide();
					this.colorInput.value = event.color;
					this.SetColor(event.color);
				},
				SetColor: function (color){
					domStyle.set(this.colorPress,"backgroundColor",color);
					//document.getElementById("color_box").value = color;
				},
				TextChange: function(){
					this.SetColor(this.colorInput.value);
				},
				ShowDialog: function() {
					this.colorDialog.show();
					this.slDialog.SetValue(this.colorInput.value);
				}
				
				
			});
		}
	);
	
	//listener pattern  
