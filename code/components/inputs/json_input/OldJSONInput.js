define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-construct",
		 "dojo/dom-attr",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dojo/dom",
		 "components/data_entry/inputs/text_area/TextArea",
		 "components/data_entry/inputs/text/Text",
		 "components/data_entry/inputs/button/Button",
		 "components/data_entry/inputs/select/Select",
                 "components/data_entry/inputs/button_dropdown/Button_Dropdown",
		 "components/data_entry/DataEntry",
		 "components/data_entry/inputs/pw_password/PWPassword",
		 "components/url_input/URLInput",
                 "components/data_entry/inputs/search_input/SearchInput",
                 "components/data_entry/inputs/xpend_text/XPend/XPendedText",
                 "components/data_entry/inputs/location_button/LocationButton",
                 "components/data_entry/inputs/input_group/InputGroup",
		 "dojo/text!./OldJSONInput.tpl.html"],
		function (declare,
				  on,
				  domCon,
				  domAttr,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
				  dom,
				  textAreaWidget,
				  textInputWidget,
				  buttonWidget,
				  selectBasicWidget,
                                  buttonDropWidget,
                                  formWidget,
                                  PWPassword,
                                  URLInput,
                                  SearchInputWidget,
                                  XPendedTextWidget,
                                  LocationButton,
                                  InputGroup,
				  template) 
		{
		    return declare
		    (
		    	'JSONInput', 
		    	[_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
				{
					data: null,
					name: null,
					button: null,
					input: null,
				    templateString: template,
	                baseClass:'formStyle',
	                constructor: function (data)
	                {
	                	this.data = data;
	                },
			    	postCreate: function () 
			    	{
						var textAreaData =  {
							label: "Init Form from JSON",
                            placeholder: "Use JSON syntax",
                            value: '{\n\
"formName":"JSON-Generated Bootsnip Form",\n\
"widgets":[{\n\
    "componentType":"button",\n\
    "singlebutton":"I need to be depressed",\n\
    "label":"The button is sad"},\n\
{\n\
    "componentType": "selectBasic",\n\
    "name":"selectOption",\n\
    "label": "Choose something",\n\
    "visible": true,\n\
    "options": [\n\
        {"name":"some_option1", "label":"First option"},\n\
        {"name":"some_option2", "label":"Second option", "value":"option2"},\n\
        {"name":"some_option3", "label":"Third option", "value":"option3"}\n\
    ]\n\
},\n\
{\n\
    "componentType": "selectBasic",\n\
    "name":"testAvail",\n\
    "label": "Hidden Option",\n\
    "options": [\n\
        {"name":"opt1", "label":"First option"},\n\
        {"name":"opt2", "label":"Second option"},\n\
        {"name":"opt3", "label":"Third option"}\n\
    ],\n\
    "avail-when":"selectOption=some_option1"\n\
},\n\
{\n\
    "componentType":"buttonDrop",\n\
    "name":"buttonDropHidden",\n\
    "placeholder":"option2",\n\
    "label":"I am label",\n\
    "buttontext":"Options",\n\
    "options":"lala,option2,option3,yay,etc,none",\n\
    "avail-when":"selectOption=option2"\n\
},\n\
{\n\
    "componentType": "searchInput",\n\
    "name":"searchInput1",\n\
    "label": "searchInput1",\n\
    "placeholder": "search1",\n\
    "help": "searchInput1",\n\
    "avail-when": "buttonDropHidden=option2"\n\
},\n\
{\n\
    "componentType":"button",\n\
    "name":"showButton",\n\
    "singlebutton":"visible",\n\
    "label":"hiddenButton",\n\
    "avail-when":"searchInput1=search1"\n\
},\n\
{\n\
    "componentType":"urlInput",\n\
    "name":"urlHidden",\n\
    "label":"URL",\n\
    "placeholder":"visible",\n\
    "urlButton":"Test",\n\
    "visible" : true,\n\
    "avail-when":"showButton=visible"\n\
},\n\
{\n\
    "componentType": "textInput",\n\
    "name":"textInput1",\n\
    "label": "textInput1",\n\
    "placeholder": "textInput1",\n\
    "help": "textInput1",\n\
    "avail-when":"urlHidden=visible"\n\
},\n\
{\n\
    "componentType": "inputGroup",\n\
    "name": "inputGroupTest",\n\
    "avail-when":"selectOption=some_option1",\n\
    "items": [\n\
        {\n\
            "componentType": "xPendedText",\n\
            "x": "prepend",\n\
            "name": "InputGroup1",\n\
            "xpend": "InputGroup1",\n\
            "label": "InputGroup1",\n\
            "placeholder": "InputGroup1",\n\
            "help": "InputGroup1"\n\
        },\n\
        {\n\
            "componentType": "xPendedText",\n\
            "x": "append",\n\
            "name": "InputGroup2",\n\
            "xpend": "InputGroup2",\n\
            "label": "InputGroup2",\n\
            "placeholder": "show me",\n\
            "help": "InputGroup2"\n\
        },\n\
        {\n\
            "componentType": "inputGroup",\n\
            "name": "HiddenInputGroup",\n\
            "avail-when":"InputGroup2=show me",\n\
            "items": [\n\
                {\n\
                    "componentType": "xPendedText",\n\
                    "x": "prepend",\n\
                    "name": "HiddenInputGroup1",\n\
                    "xpend": "HiddenInputGroup1",\n\
                    "label": "HiddenInputGroup1",\n\
                    "placeholder": "HiddenInputGroup1",\n\
                    "help": "HiddenInputGroup1"\n\
                },\n\
                {\n\
                    "componentType": "xPendedText",\n\
                    "x": "append",\n\
                    "name": "HiddenInputGroup2",\n\
                    "xpend": "HiddenInputGroup2",\n\
                    "label": "HiddenInputGroup2",\n\
                    "placeholder": "HiddenInputGroup2",\n\
                    "help": "HiddenInputGroup2"\n\
                }\n\
            ]\n\
        }\n\
    ]\n\
},\n\
{\n\
    "componentType": "locationButton",\n\
    "name":"LocationButton",\n\
    "locateButton": "Find my location",\n\
    "label": "Location Button",\n\
    "latField": "latitude",\n\
    "lonField": "longitude",\n\
    "accuracyField" : "accuracy",\n\
    "visible" : true\n\
},\n\
{\n\
    "componentType": "xPendedText",\n\
    "x": "prepend",\n\
    "name": "prependHidden",\n\
    "xpend": "prependHidden",\n\
    "label": "prependHidden",\n\
    "placeholder": "prependPlaceholder",\n\
    "help": "prependHelp",\n\
    "avail-when": "LocationButton=Latitude Longitude Accuracy"\n\
},\n\
{\n\
    "componentType":"textInput",\n\
    "name": "latitude", \n\
    "label":"Latitude",\n\
    "placeholder":"Enter Latitude"\n\
},\n\
{\n\
    "componentType":"textInput",\n\
    "name": "longitude", \n\
    "label":"Longitude",\n\
    "placeholder":"Enter Longitude"\n\
},\n\
\n\{\n\
    "componentType":"textInput",\n\
    "name": "accuracy", \n\
    "label":"Accuracy",\n\
    "placeholder":"Enter Accuracy"\n\
},\n\
{\n\
    "componentType":"buttonDrop",\n\
    "placeholder":"I am holding a place.",\n\
    "label":"I am label",\n\
    "buttontext":"Options",\n\
    "options":"lala,option2,option3,yay,etc,none"\n\
},\n\
{\n\
    "componentType":"buttonDrop",\n\
    "placeholder":"I am holding a place.",\n\
    "label":"I am label",\n\
    "buttontext":"Options",\n\
    "options":"lala,option2,option3,yay,etc,none"\n\
},\n\
{\n\
    "componentType":"buttonDrop",\n\
    "placeholder":"I am holding a place.",\n\
    "label":"I am label",\n\
    "buttontext":"Options",\n\
    "options":"lala,option2,option3,yay,etc,none"\n\
},\n\
{\n\
    "componentType":"buttonDrop",\n\
    "placeholder":"I am holding a place.",\n\
    "label":"I am label",\n\
    "buttontext":"Options",\n\
    "options":"lala,option2,option3,yay,etc,none"\n\
},\n\
{\n\
    "componentType":"buttonDrop",\n\
    "placeholder":"I am holding a place.",\n\
    "label":"I am label","buttontext":"Options",\n\
    "options":"lala,option2,option3,yay,etc,none,veryloooooooooooooooooooooooooooooooooooooooongoption,looooooooooooooooooooooooooooooooongoption,moreoptions,moremore,moremoremore,moremoremoremore,moremoremoremore,moreeeeeee",\n\
    "rows":"7"\n\
},\n\
{\n\
    "componentType":"passwordInput",\n\
    "label":"Password",\n\
    "placeholder":"Enter Password",\n\
    "visible" : true \n\
},\n\
{\n\
    "componentType":"urlInput",\n\
    "label":"URL",\n\
    "placeholder":"Enter URL",\n\
    "urlButton":"Test",\n\
    "visible" : true \n\
},\n\
{\n\
    "componentType": "xPendedText",\n\
    "x": "append",\n\
    "xpend": "appendTest",\n\
    "label": "appendLabel",\n\
    "placeholder": "appendPlaceholder",\n\
    "help": "appendHelp"\n\
},\n\
{\n\
    "componentType": "xPendedText",\n\
    "x": "prepend",\n\
    "xpend": "prependTest",\n\
    "label": "prependLabel",\n\
    "placeholder": "prependPlaceholder",\n\
    "help": "prependHelp"\n\
},\n\
{\n\
    "componentType": "searchInput",\n\
    "label": "searchLabel",\n\
    "placeholder": "searchPlaceholder",\n\
    "help": "searchHelp"\n\
}]}'
											    		

				    	};
						var buttonData = 
						{
							singlebutton: "Load JSON"
				    	};
			    		input = new textAreaWidget(textAreaData);
			    		input.placeAt(this.widgetAttachPoint);
                                        input.textarea.rows = 28;
			    		on(input, 'click', this.HandleTextFocus.bind(this));
			    		button = new buttonWidget(buttonData);
			    		button.placeAt(this.widgetAttachPoint);
			    		on(button, 'click', this.HandleActionButton.bind(this));
                                        
                                        this.HandleActionButton(button); // To
																			// autopress
																			// button
																			// on
																			// load
			    	},
			    	HandleActionButton: function (event) 
			    	{
			    		var JSONText = input.textAreaText();
			    		if (this.isJsonString(JSONText))
			    		{
			   		 		var wrapper = dom.byId('wrapper');
				    		wrapper.innerHTML = "";
				    		var obj = JSON.parse(JSONText);
							var form = new formWidget(obj);
							form.placeAt(dom.byId('wrapper'));
			    		}
			    		else
			    		{
			    			alert("Syntax error in JSON input");
			    		}
				   	},
			    	getTextAreaText: function (textSource)
			    	{
			    		return textSource.innerHTML;
			    	},
			    	HandleTextFocus: function ()
			    	{
			    		if (input.textarea.innerHTML == input.data["placeholder"])
			    		{
			    			input.textarea.innerHTML = "";
			    		}
			    	},
					isJsonString: function(str) 
					{
						try 
						{
							JSON.parse(str);
						} 
						catch (e) 
						{
							return false;
						}
						return true;
					}
			    }
			);
		}
	);



