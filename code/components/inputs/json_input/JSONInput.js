define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/dom",
    "sl_components_open/data_entry/DataEntry",
    "sl_components_open/data_entry/inputs/text_area/TextArea",
    "sl_components_open/data_entry/inputs/button/Button",
    "sl_components_open/data_entry/inputs/text/Text",
    "sl_components_open/data_entry/inputs/select/Select",
    "sl_components_open/data_entry/inputs/button_dropdown/Button_Dropdown",
    "sl_components_open/data_entry/inputs/pw_password/PWPassword",
    "sl_components_open/url_input/URLInput",
    "sl_components_open/data_entry/inputs/search_input/SearchInput",
    "sl_components_open/data_entry/inputs/xpend_text/XPend/XPendedText",
    "sl_components_open/data_entry/inputs/location_button/LocationButton",
    "sl_components_open/data_entry/inputs/input_group/InputGroup",
    "sl_components_open/data_entry/inputs/checkbox/Checkbox",
    "sl_components_open/data_entry/inputs/state_widget/StateWidget",
    "sl_components_open/data_entry/inputs/radio_group/RadioGroup",
    "dojo/text!./JSONInput.tpl.html"],
        function (declare,
                on,
                domCon,
                domAttr,
                _WidgetBase,
                _TemplatedMixin,
                _WidgetsInTemplateMixin,
                dom,
                formWidget,
                textAreaWidget,
                buttonWidget,
                textInputWidget,
                selectBasicWidget,
                buttonDropWidget,
                PWPassword,
                URLInput,
                SearchInput,
                XPendedText,
                LocationButton,
                InputGroup,
                Checkbox,
                StateWidget,
                RadioGroup,
                template)
        {
            return declare
                    (
                            'JSONInput',
                            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
                            {
                                data: null,
                                existingData: null,
                                name: null,
                                button: null,
                                JSONInputText: null,
                                existingDataText: null,
                                formOutputText: null,
                                form: null,
                                templateString: template,
                                baseClass: 'json_input flex-container flex-item',
                                constructor: function (data, existingData)
                                {
                                    this.data = data;
                                    this.existingData = existingData;
                                },
                                postCreate: function ()
                                {
                                    var JSONInputData = this.data;
                                    this.JSONInputText = new textAreaWidget(JSONInputData);
                                    this.JSONInputText.placeAt(this.widgetAttachPoint);
                                    this.JSONInputText.textarea.rows = 1;

                                    if(typeof this.existingData === "undefined") {
                                        this.existingData = {};
                                    }
                                    this.existingDataText = new textAreaWidget(this.existingData);
                                    this.existingDataText.textarea.rows = 1;
                                    this.existingDataText.placeAt(dom.byId('existingData'));
                                    
                                    this.formOutputText = new textAreaWidget({
                                        label: "JSON Form Output"
                                    });
                                    this.formOutputText.textarea.rows = 1;
                                    this.formOutputText.placeAt(dom.byId('formOutput'));
                                },
                                getTextAreaText: function (textSource)
                                {
                                    return textSource.innerHTML;
                                },
                                HandleTextFocus: function ()
                                {
                                    if (this.JSONInputText.textarea.innerHTML === this.JSONInputText.data["placeholder"])
                                    {
                                        this.JSONInputText.textarea.innerHTML = "";
                                    }
                                },
                                isJsonString: function (str)
                                {
                                    try
                                    {
                                        JSON.parse(str);
                                    } catch (e)
                                    {
                                        return false;
                                    }
                                    return true;
                                }
                            }
                    );
        }
);



