/* global L */
define(["dojo/_base/declare",
    "dojo/_base/lang", 
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/on",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "sl_classes_open/StateManager",
    "dojo/text!./MapPanel.ctrl.html"], function (declare, lang, domCon,domClass, on,query,_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, StateManager,template) {
    return declare('sl_components_open/controls/MapPanel', [StateManager,_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        baseClass:'MapPanel',
        _map: null,
        _control: null,
        _controlClass: null,
        constructor: function () {
           
            this._controlClass = L.Control.extend({
                // @section
                // @aka Control.Zoom options
                options: {
                    position: 'topleft'
                },
                onAdd: function (map) {
                    var controlName = 'leaflet-control-sl-control-panel';
                    var container = L.DomUtil.create('div', controlName + ' leaflet-control');
                    L.DomEvent.disableClickPropagation(container);
                    this.container = container;
                    return this.container;
                },
                onRemove: function (map) {
                    //map.off('zoomend zoomlevelschange', this._updateDisabled, this);
                },
                SetOptions: function (opts) {
                    this.options = opts;
                },
                MergeOptions: function (opts) {
                    lang.mixin(this.options, opts);
                }


            })
        },
        postCreate: function() {
            this.SetPrefix('sl-map-panel-ctl');
            this.AddState('open');
            this.AddState('closed');
            this.SetMachine(this.domNode);
            this.SetToggleStates('open','closed');
            this.SetState('open');
            on(this.toggle_btn,'click',this.HandleToggleState.bind(this));
            this.HandleToggleState({},true);
            
        },
        MQLogoFix:function() {
            var nodes = query('.leaflet-bottom.leaflet-left');
            if(nodes.length === 0) {
                setTimeout(this.MQLogoFix.bind(this),500);
                return;
            }
            var node = nodes[0];
            domClass.toggle(node,'hidden',this.GetCurrentState()=='open');
        },
        HandleToggleState:function(event,updateOnly) {
            if(updateOnly !== true)this.ToggleState(event);
            this.MQLogoFix();            
        },
        AddToMap: function (map, options) {
            this._control = new this._controlClass();
            this._control.MergeOptions(options);
            this._map = map;
            this._map.addControl(this._control);
            this.placeAt(this._control.container);
            var controlParent = this._control.container.parentNode;
            domClass.add(controlParent, 'main-panel-control');
        },
        AddToPanel:function(component) {
           domCon.place(component,this.panelContent);
        },
        AddToTitle:function(component) {
            domCon.place(component,this.titleContent);
        },
        SetInstructions:function(ins) {
            if([null,undefined,""].indexOf(ins)<0) {
                this.instruction_txt.innerHTML = ins;
                domClass.toggle(this.instructionPane,'hidden',false);
            } else {
                domCon.empty(this.instruction_txt);
                domClass.toggle(this.instructionPane,'hidden',true);
            }
        },
        SetInfoHeading:function(heading) {
            if([null,undefined,""].indexOf(heading)<0) {
                this.heading.innerHTML = heading;
            } else {
                this.heading.innerHTML = "";
            }
        }
    });
});

