var ControlPanel = Control.extend({
	// @section
	// @aka Control.Zoom options
	options: {
		position: 'topleft',

	},

	onAdd: function (map) {
		var controlName = 'leaflet-control-sl-control-panel',
		    container = create$1('div', controlName+' leaflet-bar'),
		    options = this.options;
                this._updateDisabled();
		//map.on('zoomend zoomlevelschange', this._updateDisabled, this);

		return container;
	},

	onRemove: function (map) {
		//map.off('zoomend zoomlevelschange', this._updateDisabled, this);
	},

	disable: function () {
		this._disabled = true;
		this._updateDisabled();
		return this;
	},

	enable: function () {
		this._disabled = false;
		this._updateDisabled();
		return this;
	}	
});