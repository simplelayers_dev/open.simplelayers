define(['dstore/Memory', 'dgrid/Selection', 'dstore/Trackable', 'dgrid/OnDemandList', 'dojo/_base/declare',
    'dgrid/extensions/DijitRegistry', "dojo/on", "dojo/dom-geometry", "dojo/dom-style", "dojo/dom-construct", "dojo/dom-class", "dijit/Dialog",
    "dojo/dom-attr", "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin", "sl_components_open/record_counter/RecordCounter",
    "sl_components_open/criteria_item/CriteriaItem", "dojo/dom", "sl_modules_open/sl_api/Layers", "dojo/text!./SearchDialog.tpl.html"],
        function (Memory, Selection, Trackable, List, declare, Registry, on, domGeo, domStyle, domCon, domClass, Dialog, domAttr, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, RecordCounter, CriteriaItem, dom, Layers, template) {
            return declare('SearchDialog', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                critlist: null,
                criteriaItemList: [],
                parent: null,
                templateString: template,
                baseClass: 'search_dialog',
                constructor: function (parent) {
                    this.parent = parent;
                },
                postCreate: function () {
                    on(this.addCriteriaButton, 'click', this.HandleAddCriteriaButton.bind(this));
                    on(this.cancelButton, 'click', this.HandleCancelButton.bind(this));
                    on(this.searchButton, 'click', this.HandleSearchButton.bind(this));
                    this.ListCriteria();
                },
                HandleAddCriteriaButton: function () {
                    var item = new CriteriaItem((this.criteriaItemList.length === 0 ? true : false), this.parent.searchableKeys);
                    item.on("destroy", this.DestroyItem.bind(this));
                    console.log("e");
                    this.criteriaItemList.push(item);
                    this.UpdateList();
                },
                DestroyItem: function (event) {
                    for (var i = this.criteriaItemList.length - 1; i >= 0; i--) {
                        if (this.criteriaItemList[i] === event.item) {
                            this.criteriaItemList.splice(i, 1);
                        }
                    }
                    if (this.criteriaItemList.length > 0) {
                        this.criteriaItemList[0].IsFirst();
                    }

                    this.UpdateList();
                },
                HandleCancelButton: function () {
                    this.dialog.hide();
                },
                UpdateList: function () {
                    this.critlist.refresh();

                },
                SelectHandler: function () {

                },
                HandleSearchButton: function () {
                    var data = [];

                    for (var i = 0, l = this.criteriaItemList.length; i < l; i++) {
                        data.push(this.criteriaItemList[i].GetData());
                    }

                    this.dialog.hide();
                    this.parent.MultiCriteriaSearch(parseInt(this.recordCounter.value), data);
                },
                ListCriteria: function () {
                    domCon.destroy(this.critlist);
                    var TrackableMemory = declare([Memory, Trackable]);
                    var memory = new TrackableMemory({data: this.criteriaItemList});
                    //MyCritList = declare([List, Registry, Selection]);
                    MyCritList = declare([List, Registry]);

                    this.critlist = new MyCritList({
                        collection: memory,
                        renderRow: this.RenderRow.bind(this),
                        //selectionMode: 'single'
                    }, 'critlist').placeAt(this.criteria_attach_point);
                    this.critlist.on('dgrid-select', this.SelectHandler.bind(this));
                    this.critlist.startup();
                },
                RenderRow: function (data, options) {
                    return data.domNode;
                },
                Refresh: function () {
                    this.criteriaItemList.splice(0, this.criteriaItemList.length);
                    this.recordCounter.SetValue(25);
                    this.critlist.refresh();
                },
                Show: function () {
                    var coords = domGeo.position(this.parent.domNode);

                    this.dialog.show();
                    domStyle.set(this.dialog.domNode, {left: (coords.x + 50) + "px", top: coords.y + "px", bottom: coords.y + "px", right: 50 + "px"});
                    if (this.parent.loadedItem["search_tip"]) {
                        this.searchTip.innerHTML = "Search Tip: " + this.parent.loadedItem["search_tip"];
                    } else {
                        this.searchTip.innerHTML = "";
                    }
                }
            });
        });
