define(['dojo/_base/declare',
    "dojo/on",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/dom",
    "dojo/query",
    "sl_components_open/ws_widget/WSWidget",
    "sl_components_open/legend/Legend",
    "sl_modules_open/sl_api/Layers",
    
    "dojo/text!./InfoView.tpl.html"],
        function (declare, on, domCon, domClass, domAttr, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, dom, query,WSWidget, Legend,Layers, template) {
            return declare('InfoView', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                loadedItem: null,
                layerItem: null,
                parent: null,
                info_items: [],
                content_items:[],
                templateString: template,
                baseClass: 'info_view flex-item flex-container',
                postCreate: function () {
                    //on(this.opts, 'change', this.HandleSelect.bind(this)); //bbox
                    //this.info_items = query('.info_widget',this.domNode);
                    this.info_items = [this.info_preview, this.info_legend, this.info_description, this.info_tags, this.info_webservice, this.info_maps, this.info_sublayers];
                    this.content_items = [this.preview_image,this.legend,this.description,this.tags,this.webservice,this.maps,this.sublayers];
                    window.addEventListener("resize", this.UpdateThumbnail.bind(this));
                    on(this.preview_image,'load',this.UpdateThumbnail.bind(this));

                },
                UpdateThumbnail:function() {
                        this.preview_image.style.height = (this.preview_image.offsetWidth * 0.8)+"px";
                },
                Init: function (loadedItem, layerItem, parent) { //gets the loaded layer not view
                    this.Reset();
                    this.loadedItem = loadedItem;
                    this.layerItem = layerItem;
                    this.parent = parent;

                    this.parent.progress_indicator.Show('Loading');

                    //console.log(layerItem);
                    //console.log(loadedItem);
                    domAttr.set(this.preview_image, "src", Layers.GetThumbnailURL(this.loadedItem.id));
                    
                    switch (this.loadedItem["type_label"].toLowerCase()) {
                        case "vector":
                        case "relational":
                            this.ShowIfIn([this.info_preview, this.info_legend, this.info_description, this.info_tags, this.info_webservices]);//, this.info_maps]);
                            break;
                        case "raster":
                            this.ShowIfIn([this.info_preview, this.info_description, this.info_tags, this.info_webservices]);//, this.info_maps]);
                            break;
                        case "relatable":
                            this.ShowIfIn([this.info_description, this.info_tags]);
                            break;
                        case "collection":
                            this.ShowIfIn([this.info_preview, this.info_description, this.info_tags, this.info_maps, this.info_sublayers]);
                            break;
                        case 'wms':
                            this.ShowIfIn([this.info_preview,this.info_description,this.info_tags]);
                            break;
                        default:
                            this.ShowIfIn([this.info_preview, this.info_legend, this.info_description, this.info_tags, this.info_webservice, this.info_maps, this.info_sublayers]);
                            break;
                            
                    }

                    if (this.loadedItem["description"] != null) {
                        this.description.innerHTML = this.loadedItem["description"];
                        this.info_description.Show();
                    } else {
                        this.info_description.Hide();
                    }

                    if (this.loadedItem["tags"] != null) {
                        this.tags.innerHTML = "";
                        var tags = this.loadedItem["tags"].split(",");
                        for (var i = 0, l = tags.length; i < l; i++) {
                            if (tags[i] === "")
                                continue;
                            this.tags.innerHTML += "#" + tags[i] + " ";
                        }
                        this.info_tags.Show();                        
                    } else {
                       this.info_tags.Hide();                        
                    }

                    this.legend.ShowLayerLegend(loadedItem);

                    //this.infoBbox.value = ""; //bbox stuff
                    /*this.opts.selectedIndex = 0;
                     if (this.loadedItem.hasOwnProperty("bbox") && this.loadedItem["bbox"] !== null) {
                     //for (var i = 0, l = this.loadedItem['bbox'].length; i < l; i++) {
                     //    this.infoBbox.value += this.loadedItem['bbox'][i] + ", ";
                     //}
                     //this.infoBbox.value = this.infoBbox.value.substr(0,this.infoBbox.value.length-2);
                     this.infoBbox.innerHTML = this.loadedItem['bbox'][0] + ", " + this.loadedItem['bbox'][1] + "\n" + this.loadedItem['bbox'][2] + ", " + this.loadedItem['bbox'][3];
                     this.WLon.innerHTML = this.loadedItem['bbox'][0];
                     this.NLat.innerHTML = this.loadedItem['bbox'][1];
                     this.ELon.innerHTML = this.loadedItem['bbox'][2];
                     this.SLat.innerHTML = this.loadedItem['bbox'][3];
                     }
                     
                     if (this.loadedItem['type_label'] !== "relatable" && this.loadedItem['type_label'] !== "collection") {
                     domAttr.set(this.image, "src", Layers.GetThumbnailURL(this.loadedItem.id));
                     domClass.remove(this.image, "hidden");
                     } else {
                     domClass.add(this.image, "hidden");
                     }
                     
                     this.HandleSelect("0");*/
                    this.UpdateThumbnail();
                    this.parent.progress_indicator.Hide();
                },
                ShowIfIn: function (nodeSet) {
                    for( var ii in this.info_items) {
                        var item = this.info_items[ii];
                        item.Toggle(nodeSet.indexOf(item)> -1);
                        
                    }                   
                },
                Reset:function() {
                    for( var ii in this.content_items) {
                        var item = this.content_items[ii];
                        item.innerHTML = "";
                    }
                },
                /*ShowIfIn: function (domNode) {
                 for(var i = 0, l = domNode.length; i < l; i++) {
                 domClass.remove(domNode[i], "hidden")
                 }
                 },*/
                Hide: function (domNode) {
                    domClass.add(domNode, "hidden")
                },
                Show: function (domNode) {
                    domClass.remove(domNode, "hidden");
                }
                /*,
                 HandleSelect: function (event) { //bbox event
                 if (event !== "0") {
                 event = event.target.value;
                 }
                 switch (event) {
                 case "0":
                 domClass.remove(this.infoBbox, "hidden");
                 domClass.add(this.details, "hidden");
                 break;
                 case "1":
                 domClass.add(this.infoBbox, "hidden");
                 domClass.remove(this.details, "hidden");
                 break;
                 }
                 }*/
            });
        });
