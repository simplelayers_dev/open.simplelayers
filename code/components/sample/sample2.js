define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-construct",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dojo/text!./templates/sample2.tpl.html"],
		function (declare,
				  on,
				  domCon,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
				  template) {
		    return declare('sample/sample_widget', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    data: null,
			    templateString: template,
                baseClass: 'sample_widget',
			    postCreate: function () {
			        console.log(template);
			        this.heading.innerHTML = "this is a widget within a widget";
			        on(this.action_btn, 'click', this.HandleActionButton.bind(this));
			    },
			    HandleActionButton: function (event) {
			        on.emit(this.domNode, 'action', { bubbles: true, cancelable: true });
			    }

			});
		}
	);
