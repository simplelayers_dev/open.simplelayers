define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-construct",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dojo/text!./broosWidget.tpl.html"],
		function (declare,
				  on,
				  domCon,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
				  template) 
		{
		    return declare
		    (
		    	'blarg', 
		    	[_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
				{
				    templateString: template,
	                baseClass:'broosStyle',
			    	postCreate: function () 
			    	{
			    		this.label.innerHTML = "Broos Form";
			    		//on(this.action_btn, 'click', this.HandleActionButton.bind(this));
			    	},
			    	HandleActionButton: function (event) 
			    	{
			        	alert("broosAlert");
			    	}
				}
			);
		}
	);



