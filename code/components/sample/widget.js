define(
		[ "dojo/_base/declare", "dojo/on", "dojo/dom-construct",
				"dijit/_WidgetBase", "dijit/_TemplatedMixin",
				"dijit/_WidgetsInTemplateMixin",
				'sl_components_open/sample/sample2',
				"dojo/text!./templates/sample.tpl.html" ],
		function(declare, on, domCon, _WidgetBase, _TemplatedMixin,
				_WidgetsInTemplateMixin, sample2, template) {
		    return declare('sample/sample_widget2', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    data: null,
			    startHeading: null,
			    templateString: template,
                baseClass:'sample_widget',
	 
			    HandleAction: function (event) {
			        console.log('in handle action');
			        alert('sample HandleAction function; sample2 action button clicked');
			    },
			    SetData: function (data) {
			        this.data = data;
			        this.UpdateDisplay();
			    },
			    UpdateDisplay: function () {
			        this.heading.innerHTML = "Sample Widget-data set";
			        this.label.innerHTML = this.data.label;

			    }
			});
		}
	);
