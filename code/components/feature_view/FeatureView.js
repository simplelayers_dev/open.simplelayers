define(['dojo/_base/declare', "dojo/on", "dojo/dom-construct", "dojo/dom-class",
    "dojo/dom-attr", "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_WidgetsInTemplateMixin",
    "dojo/dom", "sl_modules_open/sl_api/Layers", "dojo/text!./FeatureView.tpl.html", "sl_components_open/data_entry/DataEntry"],
        function (declare, on, domCon, domClass, domAttr, _WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin, dom, Layers, template, DataEntry) {
            return declare('FeatureView', [_WidgetBase, _TemplatedMixin,
                _WidgetsInTemplateMixin], {
                loadedItem: null,
                layerItem: null,
                parent: null,
                curr: 0,
                count: 0,
                templateString: template,
                baseClass: 'feature_view',
                postCreate: function () {
                    on(this.left, 'click', this.PageLeft.bind(this));
                    on(this.right, 'click', this.PageRight.bind(this));

                    on(this.return, 'click', this.ReturnButton.bind(this));
                    on(this.record_start, 'change', this.GoToRecord.bind(this));
                },
                Init: function (curr, loadedItem, layerItem, parent) {
                   
                    this.loadedItem = loadedItem;
                    this.layerItem = layerItem;
                    this.parent = parent;
                    this.curr = curr;

                    this.parent.progress_indicator.Show('Loading');

                    Layers.SearchFeatures({layer: this.layerItem.id, first: this.curr, limit: 1}, this.HandleFeature.bind(this));

                },
                HandleFeature: function (result) {
                    domCon.empty(this.fieldSet);
                    if (this.curr == 0) {
                        domClass.add(this.left, "disabled");
                        domAttr.set(this.left, "disabled", "disabled");

                    } else {
                        domClass.remove(this.left, "disabled");
                        domAttr.remove(this.left, "disabled");

                    }

                    if (this.curr + 1 == this.count) {
                        domClass.add(this.right, "disabled");
                        domAttr.set(this.right, "disabled", "disabled");

                    } else {
                        domClass.remove(this.right, "disabled");
                        domAttr.remove(this.right, "disabled");
                    }


                    this.count = result.paging.count;
                    this.record_start.value = (1 + this.curr);
                    this.record_text.innerHTML = " of " + this.count;

                    var visibleKeys = [], searchableKeys = [];
                    var atts = this.loadedItem.layerAttributes;
                    hadId = false;
                    for (var i = 0, len = atts.length; i < len; i++) {
                        if (atts[i].name === 'gid')
                            hadId = true;
                        if (atts[i] == 'the_geom')
                            continue;

                        if (atts[i].visible) {
                            visibleKeys.push(atts[i]);
                            if (atts[i].searchable) {
                                searchableKeys.push(atts[i]);
                            }
                        }
                    }
                    if (!hadId) {
                        visibleKeys.unshift({display: "gid", "name": "gid"});
                    }
                    var feature = result.resultset.results[0];

                    var obj = {
                        submit: "hidden",
                        widgets: [{
                                // visibleKeys goes in here
                            }]
                    };
                    var JSONInputData = {};

                    for (var i = 0; i < visibleKeys.length; i++) {
                        var JSONWidgetData = {
                            name: visibleKeys[i].name,
                            label: visibleKeys[i].name,
                            value: feature[visibleKeys[i].name],
                            disable: true
                        };

                        var type;
                        switch (visibleKeys[i].requirement) {
                            case "url":
                                type = "urlInput";
                                JSONWidgetData.urlButton = "Open";
                                break;
                            default:
                                type = "textInput";
                        }
                        JSONWidgetData.componentType = type;

                        if (visibleKeys[i].display) {
                            JSONWidgetData.label = visibleKeys[i].display;
                        }

                        JSONInputData[visibleKeys[i].name] = feature[visibleKeys[i].name];
                        obj.widgets[i] = JSONWidgetData;
                    }

                    var form = new DataEntry();
                    form.Start(obj);
                    form.placeAt(this.fieldSet);

                    // replaced with Data Entry form
//                    for (var i = 0, l = visibleKeys.length; i < l; i++) {
//                        var row = '<div class="row"><div class="col-xs-4"><label>' + visibleKeys[i].display + '</label></div><div class="col-xs-8 text-left">';
//                        if (visibleKeys[i].requirement === "url") {
//                            row += '<a href="' + feature[visibleKeys[i].name] + '">' + feature[visibleKeys[i].name] + '</a></div></div>';
//                        } else if(visibleKeys[i].requirement === "boolean"){
//                            row += (feature[visibleKeys[i].name] ? "yes" : "no") + '</div></div>';
//                        } else {
//                            row += feature[visibleKeys[i].name] + '</div></div>';
//                        }
//                        domCon.place(row, this.field_list);
//                    }

                    this.parent.progress_indicator.Hide();
                },
                PageLeft: function (event) {
                    this.curr--;
                    domClass.remove(this.record_start, "invalid");
                    this.parent.progress_indicator.Show('Loading');
                    Layers.SearchFeatures({layer: this.layerItem.id, first: this.curr, limit: 1, count: this.count}, this.HandleFeature.bind(this));
                },
                PageRight: function (event) {
                    this.curr++;
                    domClass.remove(this.record_start, "invalid");
                    this.parent.progress_indicator.Show('Loading');
                    Layers.SearchFeatures({layer: this.layerItem.id, first: this.curr, limit: 1, count: this.count}, this.HandleFeature.bind(this));
                },
                ReturnButton: function (event) {
                    this.parent.ReturnToFeatures(event);
                },
                GoToRecord: function (event) {
                    if (/^[0-9]+$/.test(this.record_start.value) && parseInt(this.record_start.value) > 0 && parseInt(this.record_start.value) < this.count) {
                        domClass.remove(this.record_start, "remove");
                        this.parent.progress_indicator.Show('Loading');
                        this.curr = parseInt(this.record_start.value) - 1;
                        Layers.SearchFeatures({layer: this.layerItem.id, first: this.curr, limit: 1, count: this.count}, this.HandleFeature.bind(this));
                    } else {
                        //wrong input
                        domClass.add(this.record_start, "invalid");
                    }
                }
            });
        });
