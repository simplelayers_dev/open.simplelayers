define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/query!css3",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "digit/_WidgetsInTemplateMixin",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/_base/lang",
    "dojo/data/ObjectStore",
    "dstore/Memory",
    "dstore/Trackable",
    "dstore/Tree",
    "dgrid/OnDemandGrid",
    "dgrid/extensions/DijitRegistry",
    "components/layer_list/layer_item/LayerItem",
    "sl_modules_open/Layers",
    "dojo/text!./LayerList.tpl.html"
],
        function (declare,
                on,
                domAttr,
                domCon,
                query,
                _WidgetBase,
                _TemplatedMixin,
                _widgetsInTemplateMixin,
                domClass,
                domStyle,
                lang,
                ObjectStore,
                Memory,
                Trackable,
                Tree,
                Grid,
                DijitRegistry,
                LayerItem,
                Layers,
                template
                ) {
            return declare('components/layer_list/LayerList', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                templateString: template,
                currentState: null,
                isOn: null,
                baseClass: 'layer_listing',
                grid: null,
                store: null,
                started: false,
                columns: null,
                newItems: null,
                postCreate: function () {

                },
                LayersLoaded: function (event) {
                    var data = [{id: "", name: "All Layers", type: '*', type_label: ""}];
                    console.log(data);
                    this.Start(data.concat(event.results));
                },
                Start: function (data) {
                    if (this.started) {
                        this.store.setData(data);
                        return;
                    }
                    var column = {
                        field: "id",
                        label: 'Layer',
                        renderCell: this.FormatCell.bind(this)
                    };
                    var TrackedTreeMemoryStore = declare([Memory, Trackable, DijitRegistry]);

                    this.store = new TrackedTreeMemoryStore({
                        idProperty: "id",
                        data: data
                    });

                    var grid = new (declare([Grid]))({
                        collection: this.store,
                        columns: column
                    }, this.layerGrid);

                    this.grid = grid;
                    this.grid.startup();
                    this.store.setData(data);

                    this.started = true;
                    var probNodes = query(".dgrid-scroller", this.layerGrid);
                    domAttr.remove(probNodes.pop(), "style");
                },
                AddLayer: function () {
                    this.newItems++;
                    var id = -this.newItems;
                    var newLayer = {
                        "id": id,
                        "name": "", //whatever the new layer needs
                    }
                },
                Display: function () {
                    //Layers.ListLayers(this.LayersLoaded.bind(this));
                },
                FormatCell: function (obj, value, node, options) {
                    var item = new LayerItem();
                    domCon.empty(node);
                    item.ShowData(obj, "edit");
                    item.placeAt(node);
                },
                SaveChanges: function () {
                    var changes = [];
                    for (var i = 0, l = this.store.data.length; i < l; i++) {
                        var item = this.store.data[i];
                        if (item.isDeleted) {
                            changes.push({
                                id: item.id,
                                isDeleted: true
                            });
                        }
                    }
                },
                ChangesSaved: function (results) {
                    this.Refresh();
                },
                Refresh: function () {
                    try {
                        var TrackedTreeMemoryStore = declare([Memory, Trackable, DijitRegistry]);
                        this.store = new TrackedTreeMemoryStore({
                            idProperty: "id",
                            data: []
                        });
                        this.grid.set("collection", this.store);
                        this.grid.refresh();
                        this.started = false;
                        this.Display();
                    } finally {

                    }
                }
            });
        });