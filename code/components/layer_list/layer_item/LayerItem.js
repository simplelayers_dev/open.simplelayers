define(["dojo/_base/declare",
    "dojo/on",
    "dojo/query",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/topic",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/dom-class",
    "dojo/dom-style",
    "sl_classes_open/StateManager",
    "sl_modules_open/layerImage",
    "sl_modules_open/sl_api/Layers",
    "dojo/text!./LayerItem.tpl.html"
], function (declare,
        on,
        query,
        domAttr,
        domCon,
        topic,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        domClass,
        domStyle,
        StateManager,
        LayerImage,
        Layers,
        template

        ) {
    return declare("LayerItem", [StateManager, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        id: null,
        parent: null,
        templateString: template,
        baseClass: "layer_item flex-item flex-container col",
        data: null,
        topicHandlers: null,
        constructor: function (data, parent) {
            this.data = data;
            this.parent = parent;
            this.topicHandlers = [];
        },
        postCreate: function () {
            this.SetMachine(this.domNode);
            this.SetPrefix('memory')
            this.AddState('layer');
            this.AddState('non-layer');
            this.SetToggleStates('layer', 'non-layer');
            this.id = this.data["id"];
            this.ToggleState(false);

            var type = LayerImage.getIconClass(this.data["type"], this.data["geom"]);
            domClass.toggle(this.allLayer_icon,'hidden',this.data['type']!=='*');
            domClass.toggle(this.layer_icon,'hidden',this.data['type']==='*');
            domClass.toggle(this.layer_icon, type, true);
            domAttr.set(this.layer_icon, "title", type);
            this.name.innerHTML = this.data["name"];
            domAttr.set(this.name, 'title', this.data['name']);
            this.description.innerHTML = this.data["description"] != "" ? this.data["description"] : "";//No description available.";//.length <= 120 ? this.data["description"] : this.data["description"].substring(0, 120) + "...";
            domAttr.set(this.description, "title", this.data["description"]);
            var tags = this.data["tags"].split(",");
            for (var i = 0, l = tags.length; i < l; i++) {
                if (tags[i] === "")
                    continue;
                var tag = domCon.toDom("<a class='tag'>#" + tags[i] + "</a>");
                on(tag, "click", this.TagClick.bind(this));
                domCon.place(tag, this.tags);
            }
            this.topicHandlers.push(topic.subscribe('geocalculator/updated', this.HandleMemoryUpdates.bind(this)));
            

            on(this.memoryRecall_btn, 'click', this.HandleMemoryButton.bind(this));

        },
        HandleMemoryButton: function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            setTimeout(this.NotifyRecallListeners.bind(this), 500);
        },
        NotifyRecallListeners: function () {
            topic.publish('layeritem/memory/recall', {layerId: this.id, layerData: this.data});
        },
        UpdateMemory: function (calc) {
            this.ToggleState(calc.IsLayer(this.id));
        },
        TagClick: function (event) {
            this.parent.filter.value = event.target.innerHTML;
            this.parent.FilterLayers();
        },
        HandleMemoryUpdates: function (event) {
            this.UpdateMemory(event.calculator);
        },
        destroy: function () {
            for (var i in this.topicHandlers) {
                this.topicHandlers[i].remove();
            }
            return this.inherited(arguments);

        }
    });
});