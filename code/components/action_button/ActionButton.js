define(["dojo/_base/declare",
        "dojo/on",
        "dojo/dom-attr",
        "dijit/_WidgetBase", 
        "dijit/_TemplatedMixin", 
        "dijit/_WidgetsInTemplateMixin",
          'dojo/dom-class',
        'dojo/dom-style',
        "dojo/text!./ActionButton.tpl.html"
        ],
    function(declare,
    		on,
    		domAttr,
    		_WidgetBase, 
    		_TemplatedMixin,
    		_WidgetsInTemplateMixin,
    		domClass,
    		domStyle,
    		template){
        return declare('components/action_button/ActionButton',[_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        	templateString:template,
        	handler:null,
        	context:null,
        	postCreate:function() {
        		var icon=domAttr.get(this.domNode,'data-sl_open-icon');
        		domClass.add(this.iconImage,icon);
        		on(this.domNode,'click',this.HandleClick.bind(this));
        	},
        	SetHandler:function(handler,context) {
        		this.handler = handler;
        		this.context = context;
        	},
        	HandleClick:function(event) {
        		//domClass.toggle(this.domNode,'pressed',false);
        		if(this.handler != null) this.handler.call(this.context,event);
        	},
        	
        	
        
        }
        
        );
});
