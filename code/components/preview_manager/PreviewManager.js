define(["dojo/_base/declare",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./PreviewManager.tpl.html"],
        function (declare, on, dom, domCon, domClass, domAttr, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
            return declare('PreviewManager', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
                views: {},
                _currentView:null,
                templateString: template,
                baseClass: 'preview_manager flex-item flex-container',
                startup: function () {
                    var children = this.domNode.childNodes;
                    for (var i = 0, l = children.length; i < l; i++) {
                        var viewName = domAttr.get(children[i], "data-preview");
                        if (viewName) {
                            this.views[viewName] = children[i];
                            domClass.add(children[i], "hidden");
                        }
                    }
                },
                GoToView: function (viewName) {
                    if(this._currentView === viewName) return;
                    for (var view in this.views) {
                        if (view === viewName) {
                            domClass.toggle(this.views[view], "hidden",false);
                            this._currentView = this.views[view];
                            continue;
                        }
                        domClass.toggle(this.views[view], "hidden",true);
                        
                    }
                },
                GetCurrentView:function(){
                    return this._currentView;
                }
                
            });
        });
