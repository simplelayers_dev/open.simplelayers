define(["dojo/_base/declare",
		 "dojo/on",
		 "dojo/dom-construct",
		 "dijit/form/Button",
		 "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_WidgetsInTemplateMixin",
         'sl_modules_open/WAPI',
		 "dojo/text!./templates/ui.tpl.html"],
		function (declare,
				  on,
				  domCon,
				  button,
				  _WidgetBase,
				  _TemplatedMixin,
				  _WidgetsInTemplateMixin,
                  wapi,
				  template) {
		    return declare('components/button_maker', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
			    data: null,
			    baseClass:'button_maker',
			    templateString: template,
			    postCreate: function () {
			        this.sample_button.set('label','Button Maker');
			        on(this.sample_button, 'click', this.HandleClick.bind(this));
			    },
			    HandleClick:function(event) {
			    	alert('by George Vine');
			    }
			    
			});
		}
	);
